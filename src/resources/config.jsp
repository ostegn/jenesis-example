/*****************************************************<%
response.setContentType("text/javascript");
%>**                                                  **
** Jenesis Configuration Javascript                 **
** By Thiago Pereira Ricciardi                      **
**                                                  **
******************************************************/

jenesisConfig = {};

(function ()
{

var cfg = jenesisConfig;

cfg.getBaseURL = function () {
	return "${baseurl}";
};

cfg.getHomePage = function () {
	return "${homepage}";
};

cfg.getLoginPage = function () {
	return "${loginpage}";
};

cfg.getImageBaseURL = function () {
	return "${imagebase}";
};

cfg.getDateFormatString = function () {
	return "${dateformat}";
};

cfg.getTimeFormatString = function () {
	return "${timeformat}";
};

cfg.getDateTimeFormatString = function () {
	return "${datetimeformat}";
};

cfg.getJSONDateTimeFormatString = function () {
	return "${json.datetimeformat}";
};

cfg.getLoginAction = function () {
	return "${sec.login}";
};

cfg.getLogoutAction = function () {
	return "${sec.logout}";
};

cfg.getUsernameField = function () {
	return "${sec.userfield}";
};

cfg.getPasswordField = function () {
	return "${sec.passfield}";
};

cfg.getAppName = function () {
	return "${app.name}";
};

cfg.getAppVersion = function () {
	return "${app.version}";
};

cfg.getAppBuild = function () {
	return "${app.build}";
};

cfg.getAppRevision = function () {
	return "${app.revision}";
};

cfg.getCSRFHeaderName = function () {
	return "${_csrf.headerName}";
};

cfg.getCSRFParameterName = function () {
	return "${_csrf.parameterName}";
};

cfg.getCSRFToken = function () {
	return "${_csrf.token}";
};

})();
