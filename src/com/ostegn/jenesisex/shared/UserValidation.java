package com.ostegn.jenesisex.shared;

import java.util.Date;

import com.ostegn.jenesis.shared.base.SimpleFunctions;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

public class UserValidation {
	
	public static final Integer MIN_AGE = 18;
	public static final Integer MIN_PASS_LENGTH = 5;
	public static final Integer MIN_NAME_LENGTH = 2;
	public static final Integer MIN_USERNAME_LENGTH = 3;
	
	public static final String _SEPARATOR_ = ",";
	public static final String EMAIL = "email";
	public static final String PASSWORD = "password";
	public static final String NAMEF = "namef";
	public static final String NAMEL = "namel";
	public static final String BIRTHDAY = "birthday";
	public static final String BIRTHDAY_YOUNG = "birthday_young";
	public static final String USERNAME = "username";
	
	public static String validate(User user, UserDetails user_details) {
		String ret = "";
		
		ret += validate(user);
		ret += validate(user_details);
		
		return ret;
	}
	
	public static String validate(User user) {
		String ret = "";
		
		if ((user != null) ? !validateEmail(user.getEmail()) : true)
		{
			if (ret.length() > 0) ret += _SEPARATOR_;
			ret += EMAIL;
		}
		
		if ((user != null) ? !validatePassword(user.getPassword()) : true)
		{
			if (ret.length() > 0) ret += _SEPARATOR_;
			ret += PASSWORD;
		}
		
		if ((user != null) ? !SimpleFunctions.validateMinLength(user.getUsername(), MIN_USERNAME_LENGTH) : true) {
			if (ret.length() > 0) ret += _SEPARATOR_;
			ret += USERNAME;
		}
		
		return ret;
	}
	
	public static Boolean validateEmail(String email) {
		return SimpleFunctions.validateEmail(email);
	}
	
	public static Boolean validatePassword(String password) {
		return SimpleFunctions.validateMinLength(password, MIN_PASS_LENGTH);
	}
	
	public static String validate(UserDetails user_details) {
		String ret = "";
		
		if ((user_details != null) ? !validateNames(user_details.getNamef()) : true )
		{
			if (ret.length() > 0) ret += _SEPARATOR_;
			ret += NAMEF;
		}
		if ((user_details != null) ? !validateNames(user_details.getNamel()) : true )
		{
			if (ret.length() > 0) ret += _SEPARATOR_;
			ret += NAMEL;
		}
		
		if ( (user_details != null) ? (user_details.getBirthday() != null) : false ) {
			Date min_age = SimpleFunctions.addYears(new Date(), -MIN_AGE);
			Date birthday = user_details.getBirthday();
			if (min_age.before(birthday)) {
				if (ret.length() > 0) ret += _SEPARATOR_;
				ret += BIRTHDAY_YOUNG;
			}
		}
		else {
			if (ret.length() > 0) ret += _SEPARATOR_;
			ret += BIRTHDAY;
		}
		
		return ret;
	}
	
	public static Boolean validateNames(String name) {
		return SimpleFunctions.validateMinLength(name, MIN_NAME_LENGTH);
	}

}
