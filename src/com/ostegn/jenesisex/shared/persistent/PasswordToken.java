package com.ostegn.jenesisex.shared.persistent;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ostegn.jenesis.shared.base.PersistentObject;
import com.ostegn.jenesis.shared.base.SimpleConstants;
import com.ostegn.jenesisex.shared.base.IUserDependant;


@SuppressWarnings("serial")
@Entity
@Table(name="password_token")
public class PasswordToken extends PersistentObject implements IUserDependant {
	
	/** Defines the expiration time of a token */
	public static final long EXPIRATION_MSEC = SimpleConstants.ONE_DAY_MSEC;
	
	@Id @Column(columnDefinition="bigint unsigned", nullable=false) private BigInteger iduser;
	@Column(columnDefinition="char", length=64, nullable=false) private String token;
	@Column(nullable=false) private Date lastedit;
	
	/**
	 * Verifies if a token is expired.
	 * @return True if it's expired. False if it's valid.
	 */
	public Boolean isExpired() {
		Boolean ret = true;
		if (lastedit != null) {
			Date d = getExpirationDate();
			if (d != null) {
				ret = d.before(new Date());
			}
		}
		return ret;
	}
	
	/**
	 * Gets the expiration date of this token.
	 * @return null if it's not set yet.
	 */
	public Date getExpirationDate() {
		Date ret = null;
		if (lastedit != null) {
			ret = new Date(lastedit.getTime() + EXPIRATION_MSEC);
		}
		return ret;
	}
	
	@Override
	public void doBeforeSave() {
		setLastedit(new Date());
	}
	
	@Override
	public Serializable getPrimaryKey() {
		return iduser;
	}
	
	@Override
	public BigInteger getIduser() {
		return iduser;
	}
	@Override
	public void setIduser(BigInteger iduser) {
		this.iduser = iduser;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Date getLastedit() {
		return lastedit;
	}
	public void setLastedit(Date lastedit) {
		this.lastedit = lastedit;
	}

	@Override
	public String toString() {
		return "PasswordToken [iduser=" + iduser + ", token=" + token + ", lastedit=" + lastedit + "]";
	}

}
