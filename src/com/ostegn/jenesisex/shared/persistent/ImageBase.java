package com.ostegn.jenesisex.shared.persistent;


import org.springframework.data.mongodb.core.mapping.Document;

import com.ostegn.jenesisex.shared.base.IImageData;

@SuppressWarnings("serial")
@Document(collection="img_original")
public class ImageBase extends SimpleImage implements IImageData {
	
	private byte[] data;
	
	public ImageBase() {
		super();
	}
	public ImageBase(ImageSize size) {
		super(size);
	}
	
	@Override
	public byte[] getData() {
		return data;
	}
	@Override
	public void setData(byte[] data) {
		this.data = data;
	}

}
