package com.ostegn.jenesisex.shared.persistent;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import com.ostegn.jenesis.shared.base.PersistentObject;
import com.ostegn.jenesisex.shared.base.IHasDescription;
import com.ostegn.jenesisex.shared.base.IHasName;
import com.ostegn.jenesisex.shared.base.IImageDependant;
import com.ostegn.jenesisex.shared.base.IUserDependant;


@SuppressWarnings("serial")
@Entity
@Table(name="user_details")
public class UserDetails extends PersistentObject implements IUserDependant, IImageDependant, IHasName, IHasDescription {
	
	public enum Gender {MALE, FEMALE}
	
	@Id @Column(columnDefinition="bigint unsigned", nullable=false) private BigInteger iduser;
	@Column(length=50, nullable=false) private String namef;
	@Column(length=50, nullable=false) private String namel;
	private Date birthday;
	@Column(columnDefinition="tinyint", nullable=false) @Enumerated(EnumType.ORDINAL) private Gender gender;
	@Version @Column(columnDefinition="datetime", nullable=false) private Timestamp lastedit;
	@Column(columnDefinition="decimal unsigned", precision=30) private BigInteger idimage;
	
	@Override
	public Serializable getPrimaryKey() {
		return iduser;
	}
	
	@Override
	public BigInteger getIduser() {
		return iduser;
	}
	@Override
	public void setIduser(BigInteger iduser) {
		this.iduser = iduser;
	}
	@Override
	public String getNamef() {
		return namef;
	}
	@Override
	public void setNamef(String namef) {
		this.namef = namef;
	}
	@Override
	public String getNamel() {
		return namel;
	}
	@Override
	public void setNamel(String namel) {
		this.namel = namel;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Timestamp getLastedit() {
		return lastedit;
	}
	public void setLastedit(Timestamp lastedit) {
		this.lastedit = lastedit;
	}
	@Override
	public BigInteger getIdimage() {
		return idimage;
	}
	@Override
	public void setIdimage(BigInteger idimage) {
		this.idimage = idimage;
	}

	@Override
	public String getDescr() {
		if ((getNamef() == null) && (getNamel() == null)) return null;
		String ret = "";
		if (getNamef() != null) ret += getNamef();
		if (getNamel() != null) {
			if (ret != null) ret += " ";
			ret += getNamel();
		}
		return ret;
	}
	/** Does nothing */
	@Override
	public void setDescr(String descr) {
		//NOP
	}

	@Override
	public String toString() {
		return "UserDetails [iduser=" + iduser + ", namef=" + namef + ", namel=" + namel + ", birthday=" + birthday
				+ ", gender=" + gender + ", lastedit=" + lastedit + ", idimage=" + idimage + "]";
	}
	
}
