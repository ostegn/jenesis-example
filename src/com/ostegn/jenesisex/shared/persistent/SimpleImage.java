package com.ostegn.jenesisex.shared.persistent;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.ostegn.jenesis.shared.base.PersistentObject;
import com.ostegn.jenesisex.shared.base.IHasDescription;
import com.ostegn.jenesisex.shared.base.IImageDependant;
import com.ostegn.jenesisex.shared.base.IUserDependant;
import com.ostegn.jenesisex.shared.base.IImageData.ImageSize;

@SuppressWarnings("serial")
@Document(collection="img_original")
public class SimpleImage extends PersistentObject implements IUserDependant, IImageDependant, IHasDescription {
	
	@Transient private ImageSize size;
	
	@Id private BigInteger idimage;
	private String caption;
	private String originalName;
	private String contentType;
	private String extension;
	private Integer width;
	private Integer height;
	@Indexed private BigInteger iduser;
	private Date created = new Date();
	
	public SimpleImage() {
		this.size = ImageSize.DEFAULT;
	}
	public SimpleImage(ImageSize size) {
		this.size = size;
	}
	
	public String getFileName() {
		String ret = null;
		if (idimage != null) {
			ret = new String();
			ret += idimage;
			if ( (extension != null) ? (extension.length() > 0) : false ) {
				ret += "." + extension;
			}
		}
		return ret;
	}
	
	public String getRelativeURL() {
		String ret = getFileName();
		if (ret != null) {
			if (getSize() != null) {
				ret += getSize().getId() + "/" + ret;
			}
		}
		return ret;
	}
	
	@Override
	public String getDescr() {
		return getCaption();
	}
	@Override
	public void setDescr(String descr) {
		setCaption(descr);
	}
	
	@Override
	public Serializable getPrimaryKey() {
		return idimage;
	}
	
	public ImageSize getSize() {
		return size;
	}
	public void setSize(ImageSize size) {
		this.size = size;
	}
	
	@Override
	public BigInteger getIdimage() {
		return idimage;
	}
	@Override
	public void setIdimage(BigInteger idimage) {
		this.idimage = idimage;
	}
	public String getCaption() {
		return caption;
	}
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getOriginalName() {
		return originalName;
	}
	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Integer getHeight() {
		return height;
	}
	public void setHeight(Integer height) {
		this.height = height;
	}
	@Override
	public BigInteger getIduser() {
		return iduser;
	}
	@Override
	public void setIduser(BigInteger iduser) {
		this.iduser = iduser;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	@Override
	public String toString() {
		return "SimpleImage [size=" + size + ", idimage=" + idimage + ", caption=" + caption + ", originalName="
				+ originalName + ", contentType=" + contentType + ", extension=" + extension + ", width=" + width
				+ ", height=" + height + ", iduser=" + iduser + ", created=" + created + "]";
	}

}
