package com.ostegn.jenesisex.shared.persistent;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

import com.ostegn.jenesis.shared.base.AuthLevel;
import com.ostegn.jenesis.shared.base.ICredential;
import com.ostegn.jenesis.shared.base.PersistentObject;
import com.ostegn.jenesisex.shared.base.IUserDependant;

@Entity
@SuppressWarnings("serial")
public class User extends PersistentObject implements IUserDependant, ICredential {
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) @Column(columnDefinition="bigint unsigned", nullable=false) private BigInteger iduser;
	@Column(length=254, nullable=false, unique=true) private String email;
	@Column(length=30, nullable=false, unique=true) private String username;
	@Column(columnDefinition="char", length=64, nullable=false) private String password;
	@Column(columnDefinition="datetime", nullable=false) private Date created;
	@Version @Column(columnDefinition="datetime", nullable=false) private Timestamp lastedit;
	@Column(columnDefinition="int unsigned") private Long logincount;
	@Column(columnDefinition="datetime") private Date lastlogin;
	@Column(columnDefinition="datetime") private Date banneduntil;
	@Column(columnDefinition="smallint unsigned") private Integer authlevel;
	
	@Override
	public void onSignIn() {
		setLastlogin(new Date());
		setLogincount(getLogincount()+1);
	}
	@Override
	public void onSignOut() {
		//NOP
	}
	
	@Override
	public Serializable getPrimaryKey() {
		return iduser;
	}
	
	@Override
	public BigInteger getIduser() {
		return iduser;
	}
	@Override
	public void setIduser(BigInteger iduser) {
		this.iduser = iduser;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Timestamp getLastedit() {
		return lastedit;
	}
	public void setLastedit(Timestamp lastedit) {
		this.lastedit = lastedit;
	}
	public Long getLogincount() {
		return logincount;
	}
	public void setLogincount(Long logincount) {
		this.logincount = logincount;
	}
	public Date getLastlogin() {
		return lastlogin;
	}
	public void setLastlogin(Date lastlogin) {
		this.lastlogin = lastlogin;
	}
	public Date getBanneduntil() {
		return banneduntil;
	}
	public void setBanneduntil(Date banneduntil) {
		this.banneduntil = banneduntil;
	}
	public Integer getAuthlevel() {
		return authlevel;
	}
	public void setAuthlevel(Integer authlevel) {
		this.authlevel = authlevel;
	}

	@Override
	public Collection<String> getJAuthorities() {
		return AuthLevel.getRoles(getAuthlevel());
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return ( (getBanneduntil() != null) ? getBanneduntil().before(new Date()) : true );
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return (getAuthlevel() > 0);
	}
	
}
