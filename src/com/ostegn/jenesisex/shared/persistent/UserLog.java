package com.ostegn.jenesisex.shared.persistent;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.ostegn.jenesis.shared.base.GeneratedValue;
import com.ostegn.jenesis.shared.base.ICompositePK;
import com.ostegn.jenesis.shared.base.PersistentObject;
import com.ostegn.jenesisex.shared.base.IUserDependant;


@SuppressWarnings("serial")
@Entity
@Table(name="user_log")
@IdClass(UserLogPK.class)
public class UserLog extends PersistentObject implements IUserDependant {
	
	@Id @Column(columnDefinition="bigint unsigned", nullable=false) private BigInteger iduser;
	@Id @GeneratedValue @Column(columnDefinition="bigint unsigned", nullable=false) private BigInteger iduser_log;
	@Column(nullable=false) private Date timestamp = new Date();
	@Column(length=255, nullable=false) private String data;
	
	@Override
	public Serializable getPrimaryKey() {
		UserLogPK ret = new UserLogPK();
		ret.setIduser(iduser);
		ret.setIduser_log(iduser_log);
		return ret;
	}
	
	@Override
	public BigInteger getIduser() {
		return iduser;
	}
	@Override
	public void setIduser(BigInteger iduser) {
		this.iduser = iduser;
	}
	public BigInteger getIduser_log() {
		return iduser_log;
	}
	public void setIduser_log(BigInteger iduser_log) {
		this.iduser_log = iduser_log;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "UserLog [iduser=" + iduser + ", iduser_log=" + iduser_log + ", timestamp=" + timestamp + ", data="
				+ data + "]";
	}
}

@SuppressWarnings("serial")
class UserLogPK implements ICompositePK {
	
	private BigInteger iduser;
	private BigInteger iduser_log;
	
	public BigInteger getIduser() {
		return iduser;
	}
	public void setIduser(BigInteger iduser) {
		this.iduser = iduser;
	}
	public BigInteger getIduser_log() {
		return iduser_log;
	}
	public void setIduser_log(BigInteger iduser_log) {
		this.iduser_log = iduser_log;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iduser == null) ? 0 : iduser.hashCode());
		result = prime * result
				+ ((iduser_log == null) ? 0 : iduser_log.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserLogPK other = (UserLogPK) obj;
		if (iduser == null) {
			if (other.iduser != null)
				return false;
		} else if (!iduser.equals(other.iduser))
			return false;
		if (iduser_log == null) {
			if (other.iduser_log != null)
				return false;
		} else if (!iduser_log.equals(other.iduser_log))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UserLogPK [iduser=" + iduser + ", iduser_log=" + iduser_log + "]";
	}
	
}
	
		



