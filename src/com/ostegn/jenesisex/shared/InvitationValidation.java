package com.ostegn.jenesisex.shared;



public class InvitationValidation {
		
	public static String _SEPARATOR_ = ",";
	public static String EMAIL = "email";
	public static String SUBJECT = "subject";
	public static String MESSAGE = "mensage";
		
	public static String validate(String emails, String subject, String message) {
	
		String ret = "";
		
		ret += emailValidate(emails);
		ret += subjectValidation(subject);
		ret += messageValidation(message);
		
		return ret;
		
		}
		
	public static String emailValidate(String emails) {
		String ret = "";
		
		
		String div = "[,;]+";
		String[] a = emails.split(div);
		
		for (String email : a) {
			email=email.trim();
			if (!email.matches("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$")){
					if (ret.length() > 0) ret += _SEPARATOR_;
					ret += EMAIL;
				}
		}
		return ret;
		}
	
	public static String subjectValidation (String subject){
		String ret = "";
		if (subject.isEmpty()){
			ret += _SEPARATOR_;
			ret += SUBJECT;
		}
		return ret;
	}
	
	public static String messageValidation (String message){
		String ret="";
		if (message.isEmpty()){
			ret += _SEPARATOR_;
			ret += MESSAGE; 
		}
		return ret;
	}
	
	
	}