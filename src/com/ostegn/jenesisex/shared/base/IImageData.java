package com.ostegn.jenesisex.shared.base;

import com.ostegn.jenesis.shared.base.SimpleConstants;

/**
 * 
 * Defines the interface of image data.
 * 
 * @author Thiago Ricciardi
 *
 */
public interface IImageData {
	
	/** The maximum size of an image to be stored on the server (in bytes) */
	static final long MAX_SIZE_BYTES = 5 * SimpleConstants.ONE_MEBIBYTE_BYTE; //5 MiB
	
	/**
	 * 
	 * The sizes stored on the image server database.
	 * 
	 * @author Thiago Ricciardi
	 *
	 */
	public enum ImageSize {
		
		/** The original uploaded size */
		ORIGINAL("original"),
		/** This size has no data (does not implement {@link IImageData}) */
		NODATA("nodata", 0, 0),
		/** 64x64 maintain aspect ratio */
		_64X64("64x64", 64, 64),
		/** 256x256 maintain aspect ratio */
		_256X256("256x256", 256, 256),
		/** 768x768 maintain aspect ratio */
		_768X768("768x768", 768, 768);
		
		/** The default size (redirects to one of the enumerated sizes)<br>{@code ImageSize.ORIGINAL} */
		public static final ImageSize DEFAULT = ORIGINAL;
		
		private String id;
		private Integer maxWidth;
		private Integer maxHeight;
		private Boolean crop;
		
		ImageSize(String id) {
			setAll(id, null, null, null);
		}
		ImageSize(String id, Integer maxWidth, Integer maxHeight) {
			setAll(id, maxWidth, maxHeight, null);
		}
		ImageSize(String id, Integer maxWidth, Integer maxHeight, Boolean crop) {
			setAll(id, maxWidth, maxHeight, crop);
		}
		
		private void setAll(String id, Integer maxWidth, Integer maxHeight, Boolean crop) {
			this.id = id;
			this.maxWidth = (maxWidth != null) ? maxWidth : -1;
			this.maxHeight = (maxHeight != null) ? maxHeight : -1;
			this.crop = (crop != null) ? crop : false;
		}
		
		/** Gets the id of the size */
		public String getId() {
			return id;
		}
		/** Gets the maximum width */
		public Integer getMaxWidth() {
			return maxWidth;
		}
		/** Gets the maximum height */
		public Integer getMaxHeight() {
			return maxHeight;
		}
		/** Gets the crop property (if the image is cropped or not) */
		public Boolean getCrop() {
			return crop;
		}
		/** Gets the aspect ratio of the size */
		public Float getAspectRatio() {
			return (getMaxWidth().floatValue() / getMaxHeight().floatValue());
		}
		
	}
	
	/**
	 * 
	 * The image types that can be stored on the image server database.
	 * 
	 * @author Thiago Ricciardi
	 *
	 */
	public enum ImageType {
		
		/** The Graphics Interchange Format (GIF) format specification */
		//GIF("gif", "image/gif", "gif"),
		/** The Joint Photographic Experts Group (JPEG) format specification */
		JPEG("jpg", "image/jpeg", "jpeg"),
		/** The Portable Network Graphics format specification */
		PNG("png", "image/png", "png");
		
		private String extension;
		private String contentType;
		private String formatName;
		
		private ImageType(String extension, String contentType, String formatName) {
			this.extension = extension;
			this.contentType = contentType;
			this.formatName = formatName;
		}
		
		/** Gets the extension of the format */
		public String getExtension() {
			return extension;
		}
		/** Gets the MIME content type of the format */
		public String getContentType() {
			return contentType;
		}
		/** Gets the format name */
		public String getFormatName() {
			return formatName;
		}

	}
	
	/** Gets the image data */
	byte[] getData();
	/** Sets the image data */
	void setData(byte[] data);
	
	/** Gets the image MIME content type */
	String getContentType();
	/** Sets the image MIME content type */
	void setContentType(String contentType);
	
	/** Gets the image extension */
	String getExtension();
	/** Sets the image extension */
	void setExtension(String extension);
	
	/** Gets the image width */
	Integer getWidth();
	/** Sets the image width */
	void setWidth(Integer width);
	
	/** Gets the image height */
	Integer getHeight();
	/** Sets the image height */
	void setHeight(Integer height);
	
}
