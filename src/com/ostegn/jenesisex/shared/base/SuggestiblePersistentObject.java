package com.ostegn.jenesisex.shared.base;

import java.math.BigInteger;

import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.ostegn.jenesis.shared.base.PersistentObject;
import com.ostegn.jenesisex.client.base.CssStyleName;
import com.ostegn.jenesisex.client.ui.base.Image;
import com.ostegn.jenesisex.shared.base.IImageData.ImageSize;

@SuppressWarnings("serial")
public abstract class SuggestiblePersistentObject extends PersistentObject implements Suggestion {
	
	protected ImageSize getImageSize() {
		return ImageSize._64X64;
	}

	@Override
	public String getDisplayString() {
		String ret = "";
		if (this instanceof IImageDependant) {
			ImageSize is = getImageSize();
			HorizontalPanel hp = new HorizontalPanel();
			BigInteger idimage = ((IImageDependant) this).getIdimage();
			Image i = new Image();
			i.setSize(is);
			if (idimage != null) {
				i.setIdimage(idimage);
			}
			else {
				if (this instanceof IUserDependant) {
					//i.setStaticImage(StaticImage.ANAKIN);
				}
				i.setHeight(is.getMaxHeight().toString() + "px");
			}
			FlowPanel imgPanel = new FlowPanel();
			imgPanel.add(i);
			imgPanel.setSize(is.getMaxWidth().toString() + "px", is.getMaxHeight().toString() + "px");
			imgPanel.addStyleName(CssStyleName.LAYOUT_NOOVERFLOW_HIDE);
			imgPanel.addStyleName(CssStyleName.LAYOUT_PADDED);
			imgPanel.getElement().setAttribute("align", "center");
			imgPanel.getElement().setAttribute("style", imgPanel.getElement().getAttribute("style") + " display: table-cell; vertical-align: middle;");
			hp.add(imgPanel);
			Label l = new Label(getReplacementString());
			l.setStyleName(CssStyleName.LAYOUT_PADDED);
			hp.add(l);
			hp.setCellVerticalAlignment(l, HasVerticalAlignment.ALIGN_MIDDLE);
			ret += hp.toString();
			//ret += getReplacementString();
		}
		else {
			ret += getReplacementString();
		}
		return ret;
	}

	@Override
	public String getReplacementString() {
		String ret = "";
		if (this instanceof IHasDescription) {
			ret += ((IHasDescription) this).getDescr();
		}
		else if (this instanceof IHasName) {
			ret += ((IHasName) this).getNamef() + " " + ((IHasName) this).getNamel();
		}
		return ret;
	}

}
