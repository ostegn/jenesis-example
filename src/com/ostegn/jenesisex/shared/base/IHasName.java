package com.ostegn.jenesisex.shared.base;

import com.ostegn.jenesis.shared.base.IPersistentObject;


/**
 * 
 * Defines a persistent object that has a name field.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentObject
 * 
 */
public interface IHasName extends IPersistentObject {
	
	/** Defines the table field of the first name */
	static final String FIRSTNAME_FIELD = "namef";
	/** Defines the table field of the last name */
	static final String LASTNAME_FIELD = "namel";
	
	/** Gets the First Name */
	String getNamef();
	/** Sets the First Name */
	void setNamef(String namef);
	/** Gets the Last Name */
	String getNamel();
	/** Sets the Last Name */
	void setNamel(String namel);

}
