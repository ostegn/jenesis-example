package com.ostegn.jenesisex.shared.base;

import java.math.BigInteger;

import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.shared.persistent.SimpleImage;


/**
 * 
 * Defines that a persistent object depends on {@link SimpleImage}.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentObject
 * 
 */
public interface IImageDependant extends IPersistentObject {
	
	/** Defines the table field of the image id */
	static final String IDIMAGE_FIELD = "idimage";
	
	/** Gets the image id */
	BigInteger getIdimage();
	/** Sets the image id */
	void setIdimage(BigInteger idimage);

}
