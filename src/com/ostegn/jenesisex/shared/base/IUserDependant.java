package com.ostegn.jenesisex.shared.base;

import java.math.BigInteger;

import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.shared.persistent.User;


/**
 * 
 * Defines that a persistent object depends on {@link User}.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentObject
 * 
 */
public interface IUserDependant extends IPersistentObject {
	
	/** Defines the table field of the user id */
	static final String IDUSER_FIELD = "iduser";
	
	/** Set this value to the iduser field if you do not want the framework to automatic fill it for you */
	static final BigInteger IDUSER_NOFILL = BigInteger.ONE.negate();
	
	/** Gets the user id */
	BigInteger getIduser();
	/** Sets the user id */
	void setIduser(BigInteger iduser);

}
