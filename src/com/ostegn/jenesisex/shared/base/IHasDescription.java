package com.ostegn.jenesisex.shared.base;

import java.util.Comparator;

import com.ostegn.jenesis.shared.base.IPersistentObject;


/**
 * 
 * Defines a persistent object that has a description field.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentObject
 */
public interface IHasDescription extends IPersistentObject {
	
	/** Defines the table field of the description */
	static final String DESCR_FIELD = "descr";
	
	/** Defines the main comparator for objects that implements this interface */
	static final Comparator<IHasDescription> COMPARATOR = new Comparator<IHasDescription>() {
		@Override
		public int compare(IHasDescription o1, IHasDescription o2) {
			String s1 = (o1 != null) ? ((o1.getDescr() != null) ? o1.getDescr() : "") : "";
			String s2 = (o2 != null) ? ((o2.getDescr() != null) ? o2.getDescr() : "") : "";
			return s1.compareToIgnoreCase(s2);
		}
	};
	
	/** Gets the description */
	String getDescr();
	/** Sets the description */
	void setDescr(String descr);
	
}
