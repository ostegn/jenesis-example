package com.ostegn.jenesisex.server;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ostegn.jenesis.server.base.EmailSender;
import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.server.security.AuthHelper;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesis.shared.base.SimpleFunctions;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IUserService;
import com.ostegn.jenesisex.client.services.IUserServiceAsync;
import com.ostegn.jenesisex.server.base.EmailHandler;
import com.ostegn.jenesisex.shared.UserValidation;
import com.ostegn.jenesisex.shared.persistent.PasswordToken;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

/**
 * 
 * Used to realize all user's transactions since we can't use PersistentService because of security reasons.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IUserService
 * @see IUserServiceAsync
 *
 */
@Service(Services.Paths.UserService)
@Secured("ROLE_ANONYMOUS")
public class UserService implements IUserService {
	
	@Autowired private PersistentOperations ph;
	@Autowired private EmailHandler eh;
	@Autowired protected PasswordEncoder passwordEncoder;

	@Override
	public Boolean userExists(User user) {
		ph.evict(user);
		User u = (User) ph.get(user);
		Boolean ret = (u != null);
		if (!ret) {
			User uemail = new User();
			uemail.setEmail(user.getEmail());
			User uname = new User();
			uname.setUsername(user.getUsername());
			List<User> lu = new ArrayList<User>();
			lu.add(uemail);
			lu.add(uname);
			List<User> l = ph.getList(lu);
			ret = l.size() > 0;
		}
		return ret;
	}
	
	@Override
	public Boolean emailExists(String email) {
		User u = new User();
		u.setEmail(email);
		List<User> l = ph.getList(u);
		return l.size() > 0;
	}
	
	@Override
	public Boolean usernameExists(String username) {
		User u = new User();
		u.setUsername(username);
		List<User> l = ph.getList(u);
		return l.size() > 0;
	}

	@Override
	public User signUp(User user, UserDetails user_details) throws JenesisException {
		String validation = UserValidation.validate(user, user_details);
		if (validation.length() > 0) {
			throw new JenesisException(validation, "VALIDATION");
		}
		user.setLogincount(0L);
		user.setAuthlevel(1);
		user.setCreated(new Date());
		try {
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user = ph.save(user);
		}
		catch (Exception e) {
			if (e.getMessage().contains("ConstraintViolationException")) {
				throw new JenesisException("User exists.", "EXISTS");
			}
			else {
				// TODO Colocar Log
				throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
			}
		}
		user_details.setIduser(user.getIduser());
		try {
			ph.save(user_details);
		}
		catch (Exception e) {
			// TODO Colocar Log
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		}
		return user;
	}
	
	@Override
	@Secured("ROLE_USER")
	public User changePass(String currentPass, String newPass) throws JenesisException {
		User user = getUser();
		try {
			if (currentPass == null) throw new JenesisException("No password.", "PASSWORD");
			if (newPass == null) throw new JenesisException("No password.", "PASSWORD");
			user = (User) ph.get(user);
			if (!passwordEncoder.encode(currentPass).equalsIgnoreCase(user.getPassword())) throw new JenesisException("Wrong password.", "PASSWORD");
			user.setPassword(passwordEncoder.encode(newPass));
			user = (User) ph.save(user);
		} catch (DataAccessException e) {
			//TODO Colocar Log
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		}
		return user;
	}
	
	@Override
	public void changePass(String token, String hash, String newPass) throws JenesisException {
		if (checkPassToken(token, hash)) {
			PasswordToken p = new PasswordToken();
			p.setToken(passwordEncoder.encode(token));
			List<? extends IPersistentObject> l = ph.getList(p);
			if (l.size() > 0) p = (PasswordToken) l.get(0);
			User u = new User();
			u.setIduser(p.getIduser());
			u = (User) ph.get(u);
			if (u != null) {
				u.setPassword(passwordEncoder.encode(newPass));
				u = (User) ph.save(u);
				ph.del(p);
			}
		}
	}
	
	@Override
	public Boolean checkPassToken(String token, String hash) throws JenesisException {
		final JenesisException invalidToken = new JenesisException("Invalid token.", "INVALID");
		PasswordToken p = new PasswordToken();
		p.setToken(passwordEncoder.encode(token));
		List<? extends IPersistentObject> l = ph.getList(p);
		if (l.size() > 0) p = (PasswordToken) l.get(0);
		else throw invalidToken;
		if (p.isExpired()) throw new JenesisException("Expired token.", "EXPIRED");
		User u = new User();
		u.setIduser(p.getIduser());
		u = (User) ph.get(u);
		if (u == null) throw invalidToken;
		if (!passwordEncoder.encode(u.getEmail()).equalsIgnoreCase(hash)) throw invalidToken;
		return true;
	}
	
	@Override
	public void forgotPass(String email) throws JenesisException {
		User u = new User();
		u.setEmail(email);
		List<? extends IPersistentObject> l = ph.getList(u);
		if (l.size() > 0) u = (User) l.get(0);
		else throw new JenesisException("E-mail not found.", "NOT_FOUND");
		UserDetails ud = new UserDetails();
		ud.setIduser(u.getIduser());
		ud = (UserDetails) ph.get(ud);
		PasswordToken p = new PasswordToken();
		p.setIduser(u.getIduser());
		p = (PasswordToken) ph.get(p);
		if (p == null) {
			p = new PasswordToken();
			p.setIduser(u.getIduser());
		}
		String token = SimpleFunctions.newStringToken(64);
		p.setToken(passwordEncoder.encode(token));
		p = (PasswordToken) ph.save(p);
		try {
			String hash = passwordEncoder.encode(u.getEmail());
			//TODO Do a better mail sending
			eh.send(EmailSender.createAddress(u.getEmail(), ud.getNamef() + " " + ud.getNamel()).toUnicodeString(), "Reset password request",
					"Hello " + ud.getNamef() + ",\n\n" +
					"A password reset has been requested for your account.\n" +
					"If you have requested a password reset please click at the link below or copy and paste into your favorite browser:\n\n" +
					String.format("%1$s?t=%2$s&h=%3$s", "${baseurl}/${loginpage}" , token, hash) + "\n\n" +
					"If you did not requested a password reset please ignore this message.\n\n" +
					String.format("This request expires on %1$td-%1$tm-%1$tY %1$tH:%1$tM:%1$tS, after that this request will be invalidated.\n\n", p.getExpirationDate()) +
					""
			);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		}
	}
	
	@Override
	@Secured("ROLE_USER")
	public User getUser() {
		return ph.get((User) AuthHelper.getCredential());
	}
	
	@Override
	@Secured("ROLE_USER")
	public void contactSupport(String message) throws JenesisException {
		User u = getUser();
		UserDetails ud = new UserDetails();
		ud.setIduser(u.getIduser());
		ud = (UserDetails) ph.get(ud);
		try {
			eh.send(EmailSender.createAddress(u.getEmail(), ud.getNamef() + " " + ud.getNamel()).toUnicodeString(),
					EmailSender.createAddress("support@jenesis.ostegn.com", "Jenesis Support").toUnicodeString(), "Site problems",
					"The user " + ud.getNamef() + " " + ud.getNamel() + " has sent the following message:\n\n" +
					message + "\n\n" +
					"Yours,\nJenesis Team."
			);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
		}
	}
	
}
