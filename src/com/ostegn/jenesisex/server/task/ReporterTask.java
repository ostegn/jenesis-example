package com.ostegn.jenesisex.server.task;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimerTask;

import javax.mail.MessagingException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.server.base.ServerFunctions;
import com.ostegn.jenesis.server.task.TaskBase;
import com.ostegn.jenesis.shared.base.SimpleConstants;
import com.ostegn.jenesis.shared.base.SimpleFunctions;
import com.ostegn.jenesisex.server.base.Config;
import com.ostegn.jenesisex.server.base.Config.Property;
import com.ostegn.jenesisex.server.base.EmailHandler;
import com.ostegn.jenesisex.shared.persistent.User;


public class ReporterTask extends TaskBase {
	
	private final ReporterTimer task = new ReporterTimer();
	
	@Autowired private PersistentOperations po;
	@Autowired private EmailHandler eh;
	
	@Autowired private Config cfg;

	@Override
	public TimerTask getTask() {
		return task;
	}

	@Override
	public Date getStartupTime() throws ServletException {
		String[] time = cfg.getDefaultValue(Property.TASK_REPORTER_TIME).split(":");
		Calendar startupCalendar = ServerFunctions.setTime(Calendar.getInstance(),
															Integer.parseInt(time[0]),
															(time.length > 1) ? Integer.parseInt(time[1]) : 0,
															(time.length > 2) ? Integer.parseInt(time[2]) : 0, 0);
		try {
			time = cfg.getValue(Property.TASK_REPORTER_TIME).split(":");
			startupCalendar = ServerFunctions.setTime(Calendar.getInstance(),
					Integer.parseInt(time[0]),
					(time.length > 1) ? Integer.parseInt(time[1]) : 0,
					(time.length > 2) ? Integer.parseInt(time[2]) : 0, 0);
		} catch (Exception e) {
			throw new ServletException("Error loading " + Property.TASK_REPORTER_TIME.key + " property, please review properties file.");
		}
		
		return startupCalendar.getTime();
	}

	@Override
	public Long getIntervalMilliseconds() {
		return SimpleConstants.ONE_DAY_MSEC;
	}
	
	class ReporterTimer extends TimerTask {
		
		@Override
		public void run() {
			
			Calendar now = Calendar.getInstance();
			
			String body = "";
			body += "Total de cadastros: ";
			Long usersCount = po.countAll(User.class);
			if (usersCount == null) usersCount = 0L;
			body += usersCount.toString();
			body += "\n";
			
			String[] daysParser = cfg.getValue(Property.TASK_REPORTER_DAYS).split("[,;]+");
			Integer[] days = new Integer[daysParser.length];
			for (int i = 0; i < daysParser.length; i++) {
				days[i] = SimpleFunctions.getIntegerValue(daysParser[i]);
			}
			
			for (Integer day : days) {
				body += "Total de usuários que logaram nos últimos " + day.toString() + " dias: ";
				Calendar date = ServerFunctions.getTimeMin(now);
				date.add(Calendar.DAY_OF_YEAR, -day);
				CriteriaBuilder cb = po.getCriteriaBuilder();
				CriteriaQuery<Long> q = cb.createQuery(Long.class);
				Root<User> r = q.from(User.class);
				q.select(cb.count(r));
				q.where(cb.greaterThanOrEqualTo(r.<Date>get("lastlogin"), date.getTime()));
				Long count = po.createQuery(q).getSingleResult();
				if (count == null) count = 0L;
				body += count.toString();
				if (usersCount > 0) body += String.format(" (%1$.2f%%)", (count.doubleValue()/usersCount.doubleValue() * 100));
				body += "\n";
			}
			
			//TODO usuarios que deletaram as contas (AINDA NÃO DÁ PRA APAGAR CONTA)
			
			String[] emails = cfg.getValue(Property.TASK_REPORTER_EMAILS).split("[,;]+");
			for (String email : emails) {
				email = email.trim();
				if (SimpleFunctions.validateEmail(email)) {
					try {
						eh.send(email, "Relatório diário", body);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		}
		
	}
	
}
