package com.ostegn.jenesisex.server.base;

import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import com.ostegn.jenesis.server.base.JenesisWebConfig;
import com.ostegn.jenesis.server.security.AuthHandler;
import com.ostegn.jenesis.shared.base.AuthLevel;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.server.task.ReporterTask;


@Configuration
@PropertySources({@PropertySource("classpath:resources/my.properties")})
@ComponentScan({"com.ostegn.jenesisex.server.base","com.ostegn.jenesisex.server"})
public class WebConfig extends JenesisWebConfig {
	
	@Override
	protected String[] packagesToScan() {
		String[] packagesToScan = {
			"com.ostegn.jenesisex.shared.persistent"
		};
		return packagesToScan;
	}
	
	@Autowired protected AuthHandler authHandler;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		String loginPage = env.getProperty("loginpage");
		String secLogin = env.getProperty("sec.login");
		String secUserField = env.getProperty("sec.userfield");
		String secPassField = env.getProperty("sec.passfield");
		String secLogout = env.getProperty("sec.logout");
		http.authorizeRequests()
			.antMatchers(HttpMethod.GET, 
				"/favicon.ico",
				"/*.css",
				"/*.js",
				"/img/**",
				"/theme/**"
				).permitAll()
			.antMatchers(HttpMethod.GET,
				String.format("/%1$s", loginPage),
				String.format("/%1$s/**", loginPage.substring(0, loginPage.indexOf("."))),
				"/config.jsp"
				).hasAuthority(AuthLevel.ANONYMOUS.getRole())
			.antMatchers(HttpMethod.POST, Services.Paths.IUserService.replace("..", "")).hasAuthority(AuthLevel.ANONYMOUS.getRole())
			.antMatchers(HttpMethod.POST, String.format("%1$s**", Services.Paths.PATH.replace("..", ""))).hasAuthority(AuthLevel.USER.getRole())
			.antMatchers(HttpMethod.GET, "/**").hasAuthority(AuthLevel.USER.getRole())
			.and().anonymous().authorities(AuthLevel.ANONYMOUS.getRole())
			.and().formLogin()
				.loginPage(String.format("/%1$s", loginPage))
				.loginProcessingUrl(String.format("/%1$s", secLogin))
				.usernameParameter(secUserField)
				.passwordParameter(secPassField)
				.successHandler(authHandler)
				.failureHandler(authHandler)
			.and().logout()
				.logoutUrl(String.format("/%1$s", secLogout))
				.logoutSuccessHandler(authHandler)
			.and().sessionManagement()
				.invalidSessionUrl("/")
				.sessionFixation().newSession()
			.and().headers().frameOptions().sameOrigin()
			;
	}
	
	@Bean(initMethod="init")
	public Config config() throws IOException {
		Properties properties = new Properties();
		for (Iterator<org.springframework.core.env.PropertySource<?>> it = ((AbstractEnvironment) env).getPropertySources().iterator(); it.hasNext();) {
			org.springframework.core.env.PropertySource<?> propertySource = it.next();
			if (propertySource instanceof MapPropertySource) {
				properties.putAll(((MapPropertySource) propertySource).getSource());
			}
		}
		return new Config(properties);
	}
	
	@Bean(initMethod="startup", destroyMethod="shutdown")
	@Lazy(false)
	public ReporterTask reporterService() {
		return new ReporterTask();
	}
	
}
