package com.ostegn.jenesisex.server.base;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Properties;

import javax.servlet.ServletContext;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;

import com.ostegn.jenesis.server.base.FileHandler;
import com.ostegn.jenesis.shared.base.TagReplacer;

public class Config {
	
	public enum Property {
		
		RESOURCES_PATH("resources.path", "classpath:"),
		
		BASEURL("baseurl", "http://localhost:8080/"),
		HOMEPAGE("homepage", "home.html"),
		LOGINPAGE("loginpage", "main.html"),
		IMAGEBASE("imagebase", "http://localhost:8080/images"),
		DATEFORMAT("dateformat", ((SimpleDateFormat) DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())).toLocalizedPattern()),
		TIMEFORMAT("timeformat", ((SimpleDateFormat) DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())).toLocalizedPattern()),
		DATETIMEFORMAT("datetimeformat", ((SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault())).toLocalizedPattern()),
		JSONDATETIMEFORMAT("json.datetimeformat", "yyyyMMddHHmmssSSSZ"),
		
		SEC_LOGIN("sec.login", "login"),
		SEC_LOGOUT("sec.logout", "logout"),
		SEC_USERFIELD("sec.userfield", "username"),
		SEC_PASSFIELD("sec.passfield", "password"),
		SEC_EMAILCONF("sec.emailconf", "false"),
		
		MAIL_SERVER("mail.server", "localhost"),
		MAIL_PORT("mail.port", "25"),
		MAIL_USERNAME("mail.username"),
		MAIL_PASSWORD("mail.password"),
		MAIL_FROM("mail.from"),
		MAIL_SUBJECT("mail.subject", "${subject}"),
		MAIL_TEMPLATE("mail.template"),
		
		PERSIST_PACKAGE("persist.package"),
		
		IMAGE_DBPREFIX("image.dbprefix", "img_"),
		
		MONGO_HOST("mongo.host", "localhost"),
		MONGO_PORT("mongo.port", "27017"),
		MONGO_DB("mongo.db"),
		MONGO_USER("mongo.user"),
		MONGO_PASSWORD("mongo.password"),
		MONGO_URI("mongo.uri", "mongodb://${mongo.user}:${mongo.password}@${mongo.host}:${mongo.port}/${mongo.db}"),
		
		SQL_HOST("sql.host", "localhost"),
		SQL_PORT("sql.port", "3306"),
		SQL_SCHEMA("sql.schema"),
		SQL_USER("sql.user"),
		SQL_PASSWORD("sql.password"),
		SQL_URL("sql.url", "jdbc:mysql://${sql.host}:${sql.port}/${sql.schema}?useSSL=false"),
		SQL_DRIVER("sql.driver"),
		SQL_DIALECT("sql.dialect"),
		SQL_POOL_MAXACTIVE("sql.pool.max_active"),
		SQL_POOL_MAXIDLE("sql.pool.max_idle"),
		SQL_POOL_MAXWAIT("sql.pool.max_wait"),
		
		TASK_REPORTER_TIME("task.reporter.time", "5:00"),
		TASK_REPORTER_DAYS("task.reporter.days"),
		TASK_REPORTER_EMAILS("task.reporter.emails"),
		
		APP_NAME("app.name", "Jenesis"),
		APP_VERSION("app.version"),
		APP_BUILD("app.build"),
		APP_BUILD_COMMIT("app.build.commit"),
		APP_BUILD_BRANCH("app.build.branch"),
		APP_BUILD_TIME("app.build.time"),
		APP_BUILD_USER("app.build.user"),
		APP_BUILD_HOST("app.build.host"),
		APP_BUILD_OS("app.build.os"),
		APP_REVISION("app.revision")
		
		;
		
		public final String key;
		public final String defaultValue;
		
		Property(String key) {
			this.key = key;
			this.defaultValue = null;
		}
		
		Property(String key, String defaultValue) {
			this.key = key;
			this.defaultValue = defaultValue;
		}
		
	}
	
	@Autowired private ApplicationContext applicationContext;
	@Autowired(required=false) private ServletContext servletContext;
	
	private Properties properties;
	
	public Config(Properties properties) {
		this.properties = properties;
	}
	
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}
	
	public ServletContext getServletContext() {
		return servletContext;
	}
	
	public String getValue(Property property) {
		return properties.getProperty(property.key, property.defaultValue);
	}
	
	public String getDefaultValue(Property property) {
		return property.defaultValue;
	}
	
	public void init() throws IOException {
		replaceProperties();
		createGWTConfig();
	}
	
	private void replaceProperties() {
		HashMap<String, String> keyValue = new HashMap<String, String>();
		TagReplacer tg = new TagReplacer("${", "}");
		for (Property p : Property.values()) {
			if (getValue(p) != null) {
				keyValue.put(p.key, getValue(p));
			}
		}
		for (Property p : Property.values()) {
			if (getValue(p) != null) {
				properties.setProperty(p.key, tg.replace(keyValue, getValue(p)));
			}
		}
	}

	/*@SuppressWarnings("unchecked")
	public void findImageClasses() {
		Reflections refl = new Reflections(getValue(Property.PERSIST_PACKAGE));
		Set<Class<?>> s = refl.getTypesAnnotatedWith(Image.class);
		imageClasses = new ArrayList<Class<? extends ImageBase>>();
		for (Class<?> c : s) {
			imageClasses.add((Class<? extends ImageBase>) c);
		}
	}*/
	
	public void createGWTConfig() throws IOException {
		//load resource/config.js, replace the properties tags and save in root directory.
		if (getApplicationContext() != null) {
			Resource cfgTempl = getApplicationContext().getResource(getValue(Property.RESOURCES_PATH) + "/config.jsp");
			String gwtCfg = new FileHandler(cfgTempl.getFile()).readString();
			HashMap<String, String> keyValue = new HashMap<String, String>();
			for (Property p : Property.values()) {
				if (getValue(p) != null) {
					keyValue.put(p.key, getValue(p));
				}
			}
			gwtCfg = new TagReplacer().replace(keyValue, gwtCfg);
			if (getServletContext() != null) {
				File gwtCfgFile = new File(getServletContext().getRealPath("config.jsp"));
				new FileHandler(gwtCfgFile).write(gwtCfg);
			}
		}
	}
	
}
