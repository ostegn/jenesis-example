package com.ostegn.jenesisex.server.base;

import java.io.IOException;

import javax.mail.MessagingException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;

import com.ostegn.jenesis.server.base.EmailSender;
import com.ostegn.jenesis.server.base.FileHandler;
import com.ostegn.jenesis.shared.base.SimpleFunctions;
import com.ostegn.jenesis.shared.base.TagReplacer;
import com.ostegn.jenesisex.server.base.Config.Property;

/**
 * 
 * This class handles all the e-mail send processes.<br>
 * It automatically adds a subject template and a body template according to the configurations.
 * 
 * @author Thiago Ricciardi
 *
 */
@Repository
public class EmailHandler {
	
	//TODO log
	
	/** Subject template tag to be replaced by the real subject */
	public static final String TAG_SUBJECT = "subject";
	/** Body template tag to be replaced by the real body */
	public static final String TAG_BODY = "body";
	/** Extension files acceptable as html content */
	public static final String[] HTML_EXT = new String [] {"htm", "html"};
	
	@Autowired private Config config;
	
	/**
	 * Send the e-mail according to the properties configurations.
	 * @param dest E-mail destination.
	 * @param subject E-mail subject (a template will be applied)
	 * @param body E-mail body (a template will be applied)
	 * @throws IOException Problems loading template files.
	 * @throws MessagingException Problems sending the e-mail.
	 */
	public void send(String dest, String subject, String body) throws IOException, MessagingException {
		send(config.getValue(Property.MAIL_FROM), dest, subject, body, null);
	}
	/**
	 * Send the e-mail according to the properties configurations.
	 * @param reply_to The reply-to e-mail.
	 * @param dest E-mail destination.
	 * @param subject E-mail subject (a template will be applied)
	 * @param body E-mail body (a template will be applied)
	 * @throws IOException Problems loading template files.
	 * @throws MessagingException Problems sending the e-mail.
	 */
	public void send(String reply_to, String dest, String subject, String body) throws IOException, MessagingException {
		send(config.getValue(Property.MAIL_FROM), dest, subject, body, reply_to);
	}
	
	/**
	 * Send the e-mail according to the properties configurations.
	 * @param sender E-mail sender.
	 * @param dest E-mail destination.
	 * @param subject E-mail subject (a template will be applied)
	 * @param body E-mail body (a template will be applied)
	 * @throws IOException Problems loading template files.
	 * @throws MessagingException Problems sending the e-mail.
	 */
	@Deprecated public void sendAs(String sender, String dest, String subject, String body) throws IOException, MessagingException {
		send(sender, dest, subject, body, null);
	}
	
	private void send(String sender, String dest, String subject, String body, String reply_to) throws IOException, MessagingException {
		
		//Loading values
		String contentType = EmailSender.Types.PLAIN;
		String subjectTempl = config.getValue(Property.MAIL_SUBJECT);
		if (subjectTempl == null) subjectTempl = TAG_SUBJECT;
		String bodyTempl = TAG_BODY;
		String templPath = config.getValue(Property.MAIL_TEMPLATE);
		if (templPath != null) {
			Resource templFile = config.getApplicationContext().getResource(templPath);
			if (templFile.exists()) {
				String templFileName = templFile.getFilename();
				if (templFileName.lastIndexOf(".") > 0) {
					String templFileExt = templFileName.substring(templFileName.lastIndexOf(".")+1);
					for (String ext : HTML_EXT) {
						if (templFileExt.equalsIgnoreCase(ext)) {
							contentType = EmailSender.Types.HTML;
							break;
						}
					}
				}
				FileHandler loader = new FileHandler(templFile.getFile());
				bodyTempl = loader.readString();
			}
		}
		
		//Template translations
		TagReplacer replacer = new TagReplacer();
		for (Property p : Config.Property.values()) {
			body = replacer.replace(p.key, config.getValue(p), body);
		}
		String _subject = replacer.replace(TAG_SUBJECT, subject, subjectTempl);
		String _body = replacer.replace(TAG_BODY, body, bodyTempl);
		
		//Configure e-mail sender
		EmailSender es = new EmailSender(
				config.getValue(Property.MAIL_SERVER),
				SimpleFunctions.getIntegerValue((config.getValue(Property.MAIL_PORT))),
				config.getValue(Property.MAIL_USERNAME),
				config.getValue(Property.MAIL_PASSWORD),
				contentType);
		if ( (reply_to != null) ? (!reply_to.isEmpty()) : false ) es.replyto = EmailSender.createAddress(reply_to);
		//Send e-mail
		es.send(sender, dest, _subject, _body);
		
	}
	
}
