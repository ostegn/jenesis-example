package com.ostegn.jenesisex.server;


import java.util.List;

import org.springframework.context.annotation.Configuration;

import com.ostegn.jenesis.server.security.JenesisAuth;
import com.ostegn.jenesis.shared.base.ICredential;
import com.ostegn.jenesisex.shared.persistent.User;

/**
 * 
 * This is an example of how to use the credential system on Jenesis Framework.
 * 
 * @author Thiago Ricciardi
 *
 */
@Configuration
public class AuthService extends JenesisAuth {

	@Override
	public ICredential getCredentialByID(String id) {
		ICredential ret = null;
		User u = new User();
		u.setUsername(id);
		List<User> l = persistentHandler.getList(u);
		if (!(l.size() > 0)) {
			u = new User();
			u.setEmail(id);
			l = persistentHandler.getList(u);
		}
		if (l.size() > 0) {
			ret = l.get(0);
		}
		return ret;
	}

}
