package com.ostegn.jenesisex.server;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.server.security.AuthHelper;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IImageService;
import com.ostegn.jenesisex.server.base.Config;
import com.ostegn.jenesisex.server.base.Config.Property;
import com.ostegn.jenesisex.shared.base.IImageData;
import com.ostegn.jenesisex.shared.base.IImageData.ImageSize;
import com.ostegn.jenesisex.shared.base.IImageData.ImageType;
import com.ostegn.jenesisex.shared.persistent.ImageBase;
import com.ostegn.jenesisex.shared.persistent.SimpleImage;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

@Service(Services.Paths.ImageService)
@Secured("ROLE_USER")
public class ImageService implements IImageService {
	
	@Autowired private PersistentOperations po;
	@Autowired private Config config;
	
	private BigInteger getIdUser() {
		return ((User) AuthHelper.getCredential()).getIduser();
	}
	
	private String getCollection() {
		return getCollection((ImageSize) null);
	}
	private String getCollection(SimpleImage i) {
		return getCollection(i.getSize());
	}
	private String getCollection(ImageSize size) {
		String ret = new String();
		if (size == null) size = ImageSize.NODATA;
		ret += config.getValue(Property.IMAGE_DBPREFIX);
		ret += size.getId();
		return ret;
	}
	
	private Boolean hasWritePermission(SimpleImage i) {
		//TODO Create the permission set
		return true;
	}
	
	public String getBaseURL() {
		return config.getValue(Property.IMAGEBASE);
	}
	
	public String getAbsoluteURL(ImageBase i) {
		String ret = i.getRelativeURL();
		if (ret != null) {
			ret = getBaseURL() + "/" + ret;
		}
		return ret;
	}
	
	public SimpleImage get(SimpleImage i) {
		return po.getMongoOperations().findById(i.getPrimaryKey(), i.getClass(), getCollection(i));
	}
	
	public SimpleImage save(ImageBase i) throws JenesisException {
		if (i.getSize() != ImageSize.DEFAULT) throw new JenesisException("Saves are only for DEFAULT size", "WRONGSIZE");
		//Image owner
		i.setIduser(getIdUser());
		//Verify permission
		if (!hasWritePermission(i)) throw new JenesisException("You do not have permission to save this image.", "NOPERMISSION");
		//Save
		BigInteger idimage = null;
		for (ImageBase image : createAllSizes(i)) {
			if (idimage != null) image.setIdimage(idimage);
			po.getMongoOperations().save(image, getCollection(image));
			if (idimage == null) idimage = image.getIdimage();
			if (image.getSize() == ImageSize.DEFAULT) i = image;
		}
		return i;
	}
	
	public void del(SimpleImage i) {
		i = get(i);
		if (hasWritePermission(i)) {
			//Delete image
			BigInteger idimage = i.getIdimage();
			for (ImageSize size : ImageSize.values()) {
				po.getMongoOperations().remove(new BasicQuery("{ \"_id\" : { \"$oid\" : \"" + idimage.toString(16) + "\"}}"), getCollection(size));
			}
			//Check user image
			UserDetails u = new UserDetails();
			u.setIdimage(idimage);
			for (UserDetails user : po.getList(u)) {
				user.setIdimage(null);
				po.save(user);
			}
		}
	}
	
	public List<SimpleImage> getUserImages() {
		return getUserImages(getIdUser());
	}
	public List<SimpleImage> getUserImages(BigInteger iduser) {
		List<SimpleImage> ret = new ArrayList<SimpleImage>();
		ret = po.getMongoOperations().find(new Query(Criteria.where("iduser").is(iduser)), SimpleImage.class, getCollection());
		return ret;
	}
	
	public void rebuildImageSizes() throws JenesisException {
		List<SimpleImage> l = po.getMongoOperations().findAll(SimpleImage.class, getCollection(ImageSize.DEFAULT));
		for (SimpleImage i : l) {
			for (ImageSize size : ImageSize.values()) {
				SimpleImage img = po.getMongoOperations().findById(i.getPrimaryKey(), i.getClass(), getCollection(size));
				if (img == null) {
					img = po.getMongoOperations().findById(i.getPrimaryKey(), ImageBase.class, getCollection(ImageSize.DEFAULT));
					po.getMongoOperations().save(createSize((ImageBase) img, size), getCollection(size));
				}
			}
		}
	}
	
	private List<ImageBase> createAllSizes(ImageBase i) throws JenesisException {
		List<ImageBase> ret = new ArrayList<ImageBase>();
		if (i.getSize() == ImageSize.DEFAULT) {
			for (ImageSize size : ImageSize.values()) {
				ret.add(createSize(i, size));
			}
		}
		return ret;
	}
	
	private ImageBase createSize(ImageBase i, ImageSize size) throws JenesisException {
		ImageBase ret = null;
		if (i.getData() != null && i.getSize() == ImageSize.DEFAULT) {
			if (i.getData().length > IImageData.MAX_SIZE_BYTES) throw new JenesisException("Image size too big.", "TOOBIG");
			try {
				BufferedInputStream bis = new BufferedInputStream(new ByteArrayInputStream(i.getData()));
				Tika tika = new Tika();
				String contentType = tika.detect(bis);
				bis.mark(0);
				bis.reset();
				BufferedImage bi = ImageIO.read(bis);
				bis.close();
				ImageType it = null;
				for (ImageType type : ImageType.values()) {
					if (type.getContentType().equalsIgnoreCase(contentType)) {
						it = type;
						break;
					}
				}
				if (it == null) throw new JenesisException("Image not supported.", "NOTSUPPORTED");
				
				ret = new ImageBase(size);
				
				int w = size.getMaxWidth();
				int h = size.getMaxHeight();
				
				float imgRatio = ((float) bi.getWidth())/((float) bi.getHeight());
				
				//Data
				if ( (size.getCrop()) ? !(imgRatio > size.getAspectRatio()) : (imgRatio > size.getAspectRatio()) ) {
					h = -1;
				}
				else {
					w = -1;
				}
				if (w > 0 || h > 0) {
					Image thumb = bi.getScaledInstance(w, h, Image.SCALE_SMOOTH);
					bi = new BufferedImage(thumb.getWidth(null), thumb.getHeight(null), bi.getType());
					bi.getGraphics().drawImage(thumb, 0, 0, null);
					if (size.getCrop()) {
						bi = bi.getSubimage((bi.getWidth() - size.getMaxWidth()) >> 1, (bi.getHeight() - size.getMaxHeight()) >> 1, size.getMaxWidth(), size.getMaxHeight());
					}
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					ImageIO.write(bi, it.getFormatName(), new BufferedOutputStream(bos));
					ret.setData(bos.toByteArray());
				}
				else if (w == -1 && h == -1) {
					ret.setData(i.getData());
				}
				
				//Data dependent fields
				ret.setContentType(it.getContentType());
				ret.setExtension(it.getExtension());
				ret.setWidth(bi.getWidth());
				ret.setHeight(bi.getHeight());
				
				//Other fields
				ret.setCaption(i.getCaption());
				ret.setOriginalName(i.getOriginalName());
				ret.setIduser(i.getIduser());
				//ret.setCreated(i.getCreated());
				
				ret.setIdimage(i.getIdimage());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new JenesisException(e.getMessage(), JenesisException.UNEXPECTED);
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new JenesisException("Image not supported.", "NOTSUPPORTED");
			}
		}
		return ret;
		
	}
	
}
