package com.ostegn.jenesisex.server;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.server.security.AuthHelper;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IPersistentService;
import com.ostegn.jenesisex.shared.base.IUserDependant;
import com.ostegn.jenesisex.shared.persistent.SimpleImage;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserLog;

/**
 * Used to get and save all database objects.<br>
 * For security reasons, this class cannot be used to persist the following classes:<br><br>
 * 
 * {@code User}<br>
 * {@code SimpleImage}<br>
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentService
 * @see IPersistentObject
 * @see PersistentOperations
 */
@Service(Services.Paths.PersistentService)
@Secured("ROLE_USER")
public class PersistentService implements IPersistentService {
	
	/** Creates a new PersistentService. */
	protected PersistentService() {}
	/**
	 * Creates a new PersistentService.
	 * @param po The PersistentOperation implementation used to do the transactions.
	 */
	public PersistentService(PersistentOperations po) {
		this.po = po;
	}
	
	@Autowired
	private PersistentOperations po;
	
	public Date getDate() {
		return new Date();
	}
	
	public IPersistentObject get(IPersistentObject p) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.get(p);
	}
	
	public IPersistentObject save(IPersistentObject p) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.save(p);
	}
	
	public IPersistentObject del(IPersistentObject p) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.del(p);
	}
	
	public IPersistentObject insert(IPersistentObject p) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.insert(p);
	}
	
	public IPersistentObject update(IPersistentObject p) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.update(p);
	}
	
	public List<IPersistentObject> getAll(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		return po.getAll(p);
	}
	
	public Long countAll(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		return po.countAll(p);
	}
	
	public List<IPersistentObject> getAllLatest(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getAllLatest(p);
	}
	
	public IPersistentObject getLatest(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getLatest(p);
	}
	public List<IPersistentObject> getLatest(IPersistentObject p, int firstRow, int maxRows) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getLatest(p, firstRow, maxRows);
	}
	
	public List<IPersistentObject> getAllLatest(List<IPersistentObject> l) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getAllLatest(l);
	}
	public IPersistentObject getLatest(List<IPersistentObject> l) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getLatest(l);
	}
	public List<IPersistentObject> getLatest(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getLatest(l, firstRow, maxRows);
	}
	
	public List<IPersistentObject> getAllFirst(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getAllFirst(p);
	}
	
	public IPersistentObject getFirst(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getFirst(p);
	}
	public List<IPersistentObject> getFirst(IPersistentObject p, int firstRow, int maxRows) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getFirst(p, firstRow, maxRows);
	}
	
	public List<IPersistentObject> getAllFirst(List<IPersistentObject> l) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getAllFirst(l);
	}
	public IPersistentObject getFirst(List<IPersistentObject> l) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getFirst(l);
	}
	public List<IPersistentObject> getFirst(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getFirst(l, firstRow, maxRows);
	}
	
	public List<IPersistentObject> getList(IPersistentObject p) throws JenesisException
	{
		return getList(p, 0, 0);
	}
	public List<IPersistentObject> getList(IPersistentObject p, int firstRow, int maxRows) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getList(p, firstRow, maxRows);
	}
	
	public List<IPersistentObject> getList(List<IPersistentObject> l) throws JenesisException {
		return getList(l, 0, 0);
	}
	public List<IPersistentObject> getList(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException
	{
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getList(l);
	}
	
	public Long countList(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.countList(p);
	}
	public Long countList(List<IPersistentObject> l) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.countList(l);
	}
	
	public List<IPersistentObject> getLike(IPersistentObject p) throws JenesisException
	{
		return getLike(p, 0, 0);
	}
	public List<IPersistentObject> getLike(IPersistentObject p, int firstRow, int maxRows) throws JenesisException
	{
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.getLike(p);
	}
	
	public List<IPersistentObject> getLike(List<IPersistentObject> l) throws JenesisException {
		return getLike(l, 0, 0);
	}
	public List<IPersistentObject> getLike(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.getLike(l, firstRow, maxRows);
	}
	
	public Long countLike(IPersistentObject p) throws JenesisException {
		doSecurityCheck(p);
		p = doBeforePersist(p);
		return po.countLike(p);
	}
	public Long countLike(List<IPersistentObject> l) throws JenesisException {
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.countLike(l);
	}
	
	public List<IPersistentObject> saveList(List<IPersistentObject> l) throws JenesisException
	{
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.saveList(l);
	}
	
	public List<IPersistentObject> delList(List<IPersistentObject> l) throws JenesisException
	{
		doSecurityCheck(l);
		l = doBeforePersist(l);
		return po.delList(l);
	}
	
	/* ################ Private Methods ################ */
	
	//Security check, cannot use this class to do anything related to some classes.
	private final void doSecurityCheck(List<IPersistentObject> l) throws JenesisException {
		if (l != null) {
			for (IPersistentObject p : l) {
				doSecurityCheck(p);
			}
		}
	}
	private final void doSecurityCheck(IPersistentObject p) throws JenesisException {
		doSecurityCheck((p != null) ? p.getClass() : null);
	}
	private final void doSecurityCheck(Class<? extends IPersistentObject> c) throws JenesisException {
		Boolean ok = (c != null);
		
		if (c != null) {
			ok &= !c.equals(User.class);
			ok &= !c.equals(UserLog.class);
			ok &= !c.equals(SimpleImage.class);
		}
		
		if (!ok) throw new JenesisException(String.format("Security violation. The class %1$s cannot be persisted using this method." , c), JenesisException.GENERIC);
	}
	
	private List<IPersistentObject> doBeforePersist(List<IPersistentObject> l) throws JenesisException {
		if (l != null) {
			for (IPersistentObject p : l) {
				p = doBeforePersist(p);
			}
		}
		return l;
	}
	private IPersistentObject doBeforePersist(IPersistentObject p) throws JenesisException {
		
		if (p instanceof IUserDependant) {
			p = fillUserDependant((IUserDependant) p);
		}
		
		return p;
	}
	
	private IPersistentObject fillUserDependant(IUserDependant p) {
		if (p.getIduser() == null) {
			User u = (User) AuthHelper.getCredential();
			if (u != null) p.setIduser(u.getIduser());
		}
		else if (IUserDependant.IDUSER_NOFILL.equals(p.getIduser())) {
			p.setIduser(null);
		}
		return p;
	}
	
}
