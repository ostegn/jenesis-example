package com.ostegn.jenesisex.server.i18n;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.ostegn.jenesisex.server.base.Config;

@Component
public class Locales {
	
	@PostConstruct
	private void init() {
		loadGwtLocaleConstants();
	}
	
	private static final Locale BRAZIL = new Locale("pt", "br");
	
	private static final String GWT_EN = "en";
	private static final String GWT_PTBR = "pt_BR";
	
	private transient Log logger = LogFactory.getLog(getClass());
	
	private final Map<String, Map<String, String>> loadedLocales = new HashMap<String, Map<String,String>>();
	
	@Autowired private Config config;
	
	private enum Table {
		
		DEFAULT(BRAZIL, GWT_PTBR),
		PT_BR(BRAZIL, GWT_PTBR),
		EN_US(Locale.US, GWT_EN),
		EN_UK(Locale.UK, GWT_EN)
		;
		
		private Locale jreLocale;
		private String gwtLocale;
		
		Table(Locale jreLocale, String gwtLocale) {
			this.jreLocale = jreLocale;
			this.gwtLocale = gwtLocale;
		}
		
		public Locale getJreLocale() {
			return jreLocale;
		}
		public String getGwtLocale() {
			return gwtLocale;
		}
		
	}
	
	public static String getDefaultGwtLocale() {
		return Table.DEFAULT.getGwtLocale();
	}
	
	public static String getGwtLocale(Locale jreLocale) {
		String ret = null;
		if (jreLocale != null) {
			for (Table locale : Table.values()) {
				if (locale.getJreLocale().equals(jreLocale)) {
					ret = locale.getGwtLocale();
					break;
				}
			}
		}
		return ret;
	}
	
	public static String getGwtLocale(HttpServletRequest request) {
		String ret = null;
		Enumeration<Locale> locales = request.getLocales();
		while (locales.hasMoreElements() && ret == null) {
			Locale l = locales.nextElement();
			ret = getGwtLocale(l);
		}
		if (ret == null) ret = getDefaultGwtLocale();
		return ret;
	}
	
	public Map<String, String> getGwtLocaleConstants() {
		return getGwtLocaleConstants((Locale) null);
	}
	
	public Map<String, String> getGwtLocaleConstants(Locale jreLocale) {
		return loadedLocales.get((jreLocale != null) ? getGwtLocale(jreLocale) : "");
	}
	
	public Map<String, String> getGwtLocaleConstants(HttpServletRequest request) {
		return loadedLocales.get((request != null) ? getGwtLocale(request) : "");
	}
	
	public void loadGwtLocaleConstants() {
		loadGwtLocaleConstants(null);
		for (Table locale : Table.values()) {
			loadGwtLocaleConstants(locale.getJreLocale());
		}
	}
	
	public void loadGwtLocaleConstants(Locale jreLocale) {
		Map<String, String> loc = new HashMap<String, String>();
		String gwtLocale = getGwtLocale(jreLocale);
		if (gwtLocale == null) gwtLocale = "";
			
		String path = getClass().getName();
		path = path.substring(0, path.lastIndexOf("."));
		path = path.replace("server", "client");
		path = path.replace(".", "/");
		path = "classpath:" + path + "/IConstants";
		if (jreLocale != null) {
			path += "_" + gwtLocale;
		}
		path += ".properties";
		
		Properties prop = new Properties();
		Resource res = config.getApplicationContext().getResource(path);
		if (res.exists()) {
			try {
				prop.load(new InputStreamReader(res.getInputStream(), Charset.forName("UTF-8")));
			} catch (IOException e) {
				if (logger.isWarnEnabled()) logger.warn(e.getLocalizedMessage(), e.getCause());
				e.printStackTrace();
				loadedLocales.put(gwtLocale, loc);
			}
		}
		for (String key : prop.stringPropertyNames()) {
			loc.put(key, prop.getProperty(key));
		}
		
		loadedLocales.put(gwtLocale, loc);
	}
	
}
