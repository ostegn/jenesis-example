package com.ostegn.jenesisex.server;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigInteger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.context.support.HttpRequestHandlerServlet;

import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesis.shared.base.SimpleConstants;
import com.ostegn.jenesis.shared.base.SimpleFunctions;
import com.ostegn.jenesisex.client.services.IImageService;
import com.ostegn.jenesisex.shared.base.IImageData.ImageSize;
import com.ostegn.jenesisex.shared.persistent.ImageBase;

@SuppressWarnings("serial")
@Component
public class ImageServlet extends HttpRequestHandlerServlet implements HttpRequestHandler {
	
	@Autowired private IImageService is;
	
	@Override
	public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if ("GET".equalsIgnoreCase(request.getMethod())) {
			handleImageDownload(request, response);
		}
		else {
			handleImageUpload(request, response);
		}
	}
	
	private void handleImageDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		int err = HttpServletResponse.SC_FORBIDDEN;
		String error = request.getContextPath() + request.getServletPath();
		
		String path = request.getPathInfo();
		
		/*if (request.getDateHeader("If-Modified-Since") == -1L) {
			response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
			return;
		}*/
		
		if (path != null) {
			File img = new File(path);
			
			String idimage = SimpleFunctions.getFileName(img.getName());
			String ext = SimpleFunctions.getFileExtension(img.getName());
			
			/*
			String idimage = img.getName();
			String ext = "";
			if (idimage.contains(".")) {
				idimage = img.getName().substring(0, img.getName().lastIndexOf("."));
				ext = img.getName().substring(img.getName().lastIndexOf(".")+1);
			}
			*/
			
			if (img.getParentFile() != null) {
				
				err = HttpServletResponse.SC_NOT_FOUND;
				error = request.getContextPath() + request.getServletPath() + request.getPathInfo();
			
				String type = img.getParentFile().getName();
				
				ImageSize size = null;
				for (ImageSize imgSize : ImageSize.values()) {
					if (type.equalsIgnoreCase(imgSize.getId())) {
						size = imgSize;
						break;
					}
				}
				if (size != null) {
					ImageBase image = new ImageBase(size);
					if (idimage.startsWith("0x")) {
						idimage = idimage.substring(2);
						image.setIdimage(new BigInteger(idimage, 16));
					}
					else {
						image.setIdimage(new BigInteger(idimage));
					}
					image = (ImageBase) is.get(image);
					if (image != null) {
						if ( (ext != "") ? !ext.equalsIgnoreCase(image.getExtension()) : false ) {
							response.sendRedirect(image.getFileName());
						}
						else {
							response.setContentType(image.getContentType());
							response.setContentLength(image.getData().length);
							response.setDateHeader("Last-Modified", image.getCreated().getTime());
							response.setDateHeader("Expires", System.currentTimeMillis() + (3*SimpleConstants.ONE_MONTH_MSEC));
							response.setHeader("Cache-Control", "max-age=" + (3*SimpleConstants.ONE_MONTH_SEC));
							BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
							bos.write(image.getData());
							bos.close();
						}
						error = null;
					}
				}
				
			}
		}
		
		if (error != null) {
			response.sendError(err, error);
		}
	}
	
	private void handleImageUpload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//request.getParameterNames()
		if (ServletFileUpload.isMultipartContent(request)) {
			response.setContentType("text/html");
			PrintWriter out = response.getWriter();
			try {
				
				ImageBase ib = new ImageBase();
				
				ServletFileUpload sfu = new ServletFileUpload();
				FileItemIterator fii = sfu.getItemIterator(request);
				while (fii.hasNext()) {
					FileItemStream fis = fii.next();
					if (!fis.isFormField()) {
						if ("imageFile".equalsIgnoreCase(fis.getFieldName())) {
							BufferedInputStream bis = new BufferedInputStream(fis.openStream());
							ByteArrayOutputStream baos = new ByteArrayOutputStream();
							BufferedOutputStream bos = new BufferedOutputStream(baos);
							int b = bis.read();
							while (b != -1) {
								bos.write(b);
								b = bis.read();
							}
							bos.close();
							bis.close();
							ib.setOriginalName(fis.getName());
							ib.setData(baos.toByteArray());
						}
					}
					else {
						String field = fis.getFieldName();
						String value = readString(fis.openStream());
						if ( (value != null) ? (value.length() > 0) : false ) {
							if ("caption".equalsIgnoreCase(field)) {
								ib.setCaption(value);
							}
						}
					}
				}
				
				ib = (ImageBase) is.save(ib);
				out.println("<image-upload result=\"SUCCESS\" idimage=\"" + ib.getIdimage() + "\"/>");
			} catch (FileUploadException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new ServletException(e);
			} catch (JenesisException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				out.println("<image-upload result=\"FAILURE\" type=\"" + e.getType() + "\" message=\"" + e.getMessage() + "\"/>");
			} finally {
				out.flush();
			}
		}
	}
	
	private String readString(InputStream inputStream) {
		String ret = null;
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			int b = inputStream.read();
			while (b != -1) {
				baos.write(b);
				b = inputStream.read();
			}
			ret = new String(baos.toByteArray());
			baos.close();
			inputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ret;
	}
	
	@Override
	public String getServletInfo() {
		return "Jenesis Images Server at your service.";
	}
	
}
