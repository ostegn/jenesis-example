package com.ostegn.jenesisex.client.i18n;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Constants;


public interface IConstants extends Constants {
	
	static final IConstants instance = GWT.create(IConstants.class);
	
	Map<String, String> signUpPanel();
	Map<String, String> signInPanel();
	Map<String, String> passwordManager();
	Map<String, String> signOut();
	
	Map<String, String> imageUpload();
	
}
