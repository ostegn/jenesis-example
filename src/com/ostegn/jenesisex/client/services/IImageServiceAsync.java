package com.ostegn.jenesisex.client.services;

import java.math.BigInteger;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.ostegn.jenesisex.shared.persistent.ImageBase;
import com.ostegn.jenesisex.shared.persistent.SimpleImage;

public interface IImageServiceAsync {

	void getBaseURL(AsyncCallback<String> callback);

	void getAbsoluteURL(ImageBase i, AsyncCallback<String> callback);

	void get(SimpleImage i, AsyncCallback<SimpleImage> callback);

	void save(ImageBase i, AsyncCallback<SimpleImage> callback);

	void del(SimpleImage i, AsyncCallback<Void> callback);

	void getUserImages(AsyncCallback<List<SimpleImage>> callback);

	void getUserImages(BigInteger iduser, AsyncCallback<List<SimpleImage>> callback);

	void rebuildImageSizes(AsyncCallback<Void> callback);

}
