package com.ostegn.jenesisex.client.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.Services;

/**
 * 
 * This class manages all {@link IPersistentServiceAsync} methods to do not make unnecessary calls on the server.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentServiceAsync
 * @see IPersistentService
 * @see IPersistentObject
 * 
 */
public class PersistentManager implements IPersistentServiceAsync {
	
	private static PersistentManager me;
	
	private static IPersistentServiceAsync service;
	
	private final HashMap<String, HashMap<Serializable, IPersistentObject>> singleMap = new HashMap<String, HashMap<Serializable,IPersistentObject>>();
	private final HashMap<String, List<IPersistentObject>> allMap = new HashMap<String, List<IPersistentObject>>();
	
	private final HashMap<String, HashMap<Serializable, List<AsyncCallback<IPersistentObject>>>> singleWait = new HashMap<String, HashMap<Serializable,List<AsyncCallback<IPersistentObject>>>>();
	
	protected PersistentManager() {}
	
	private Boolean hasSingle(IPersistentObject p) {
		Boolean ret = false;
		if (p != null) {
			HashMap<Serializable, IPersistentObject> child = singleMap.get(p.getClass().getName());
			if (child != null) ret = child.containsKey(p.getPrimaryKey());
		}
		return ret;
	}
	private void setSingle(IPersistentObject p) {
		if (p != null) {
			HashMap<Serializable, IPersistentObject> child = singleMap.get(p.getClass().getName());
			if (child == null) child = new HashMap<Serializable, IPersistentObject>();
			child.put(p.getPrimaryKey(), p);
			singleMap.put(p.getClass().getName(), child);
		}
	}
	private IPersistentObject getSingle(IPersistentObject p) {
		IPersistentObject ret = null;
		if (p != null) {
			HashMap<Serializable, IPersistentObject> child = singleMap.get(p.getClass().getName());
			if (child != null) ret = child.get(p.getPrimaryKey());
		}
		return ret;
	}
	private void setList(List<IPersistentObject> l) {
		for (IPersistentObject p : l) {
			setSingle(p);
		}
	}
	private Boolean isWaiting(IPersistentObject p) {
		Boolean ret = false;
		if (p != null) {
			HashMap<Serializable, List<AsyncCallback<IPersistentObject>>> child = singleWait.get(p.getClass().getName());
			if (child != null) ret = child.containsKey(p.getPrimaryKey());
		}
		return ret;
	}
	private List<AsyncCallback<IPersistentObject>> getWaitingCallbacks(IPersistentObject p) {
		List<AsyncCallback<IPersistentObject>> ret = null;
		if (p != null) {
			HashMap<Serializable, List<AsyncCallback<IPersistentObject>>> child = singleWait.get(p.getClass().getName());
			if (child != null) ret = child.get(p.getPrimaryKey());
		}
		return ret;
	}
	private void addWaitingCallback(IPersistentObject p, AsyncCallback<IPersistentObject> callback) {
		if (p != null) {
			HashMap<Serializable, List<AsyncCallback<IPersistentObject>>> child = singleWait.get(p.getClass().getName());
			if (child == null) child = new HashMap<Serializable, List<AsyncCallback<IPersistentObject>>>();
			List<AsyncCallback<IPersistentObject>> list = child.get(p.getPrimaryKey());
			if (list == null) list = new ArrayList<AsyncCallback<IPersistentObject>>();
			if (!list.contains(callback)) list.add(callback);
			child.put(p.getPrimaryKey(), list);
			singleWait.put(p.getClass().getName(), child);
		}
	}
	private void removeWaitingCallbacks(IPersistentObject p) {
		List<AsyncCallback<IPersistentObject>> list = null;
		if (p != null) {
			HashMap<Serializable, List<AsyncCallback<IPersistentObject>>> child = singleWait.get(p.getClass().getName());
			if (child != null) list = child.get(p.getPrimaryKey());
			list.removeAll(list);
			child.put(p.getPrimaryKey(), list);
			singleWait.put(p.getClass().getName(), child);
		}
	}
	
	/**
	 * Gets the instance of the persistent manager.<br>
	 * This method exists to make sure only one instance is used.
	 */
	public static PersistentManager getInstance() {
		if (me == null) me = new PersistentManager();
		return me;
	}
	
	private static IPersistentServiceAsync getService() {
		if (service == null) {
			service = GWT.create(IPersistentService.class);
			((ServiceDefTarget) service).setRpcRequestBuilder(Services.getRpcRequestBuilder());
		}
		return service;
	}
	
	/**
	 * Clears data cache.<br>
	 * Use this to make sure the next method will call the server to get the results.
	 */
	public void clearData() {
		singleMap.clear();
		allMap.clear();
	}
	
	@Override
	public void getDate(AsyncCallback<Date> callback) {
		getService().getDate(callback);
	}
	
	@Override
	public void del(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		getService().del(p, callback);
	}

	@Override
	public void delList(final List<IPersistentObject> l, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().delList(l, callback);
	}
	
	@Override
	public void get(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		if (hasSingle(p)) {
			callback.onSuccess(getSingle(p));
		}
		else {
			Boolean isWaiting = isWaiting(p);
			addWaitingCallback(p, callback);
			if (!isWaiting) {
				getService().get(p, new AsyncCallback<IPersistentObject>() {
					@Override
					public void onFailure(Throwable caught) {
						List<AsyncCallback<IPersistentObject>> lcb = new ArrayList<AsyncCallback<IPersistentObject>>();
						lcb.addAll(getWaitingCallbacks(p));
						removeWaitingCallbacks(p);
						for (AsyncCallback<IPersistentObject> cb : lcb) {
							cb.onFailure(caught);
						}
					}
					@Override
					public void onSuccess(IPersistentObject result) {
						setSingle(result);
						List<AsyncCallback<IPersistentObject>> lcb = new ArrayList<AsyncCallback<IPersistentObject>>();
						lcb.addAll(getWaitingCallbacks(p));
						removeWaitingCallbacks(p);
						for (AsyncCallback<IPersistentObject> cb : lcb) {
							cb.onSuccess(result);
						}
					}
				});
			}
		}
	}
	
	@Override
	public void getAll(final IPersistentObject p, final AsyncCallback<List<IPersistentObject>> callback) {
		if (allMap.containsKey(p.getClass().getName())) {
			callback.onSuccess(allMap.get(p.getClass().getName()));
		}
		else {
			getService().getAll(p, new AsyncCallback<List<IPersistentObject>>() {
				@Override
				public void onFailure(Throwable caught) {
					callback.onFailure(caught);
				}
				@Override
				public void onSuccess(List<IPersistentObject> result) {
					allMap.put(p.getClass().getName(), result);
					setList(result);
					callback.onSuccess(result);
				}
			});
		}
	}

	@Override
	public void countAll(final IPersistentObject p, final AsyncCallback<Long> callback) {
		if (allMap.containsKey(p.getClass().getName())) {
			callback.onSuccess((long) allMap.get(p.getClass().getName()).size());
		}
		else {
			getService().countAll(p, callback);
		}
	}
	
	@Override
	public void getAllLatest(final IPersistentObject p, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getAllLatest(p, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getAllLatest(final List<IPersistentObject> l, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getAllLatest(l, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void getLatest(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		getService().getLatest(p, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getLatest(final List<IPersistentObject> l, final AsyncCallback<IPersistentObject> callback) {
		getService().getLatest(l, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void getLatest(final IPersistentObject p, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getLatest(p, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getLatest(final List<IPersistentObject> l, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getLatest(l, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getAllFirst(final IPersistentObject p, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getAllFirst(p, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getAllFirst(final List<IPersistentObject> l, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getAllFirst(l, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void getFirst(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		getService().getFirst(p, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getFirst(final List<IPersistentObject> l, final AsyncCallback<IPersistentObject> callback) {
		getService().getFirst(l, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void getFirst(final IPersistentObject p, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getFirst(p, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getFirst(final List<IPersistentObject> l, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getFirst(l, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getList(final IPersistentObject p, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getList(p, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getList(final List<IPersistentObject> l, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getList(l, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void getList(final IPersistentObject p, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getList(p, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getList(final List<IPersistentObject> l, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getList(l, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void countList(final IPersistentObject p, final AsyncCallback<Long> callback) {
		getService().countList(p, callback);
	}
	
	@Override
	public void countList(final List<IPersistentObject> l, final AsyncCallback<Long> callback) {
		getService().countList(l, callback);
	}
	
	@Override
	public void getLike(final IPersistentObject p, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getLike(p, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getLike(final List<IPersistentObject> l, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getLike(l, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void getLike(final IPersistentObject p, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getLike(p, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void getLike(final List<IPersistentObject> l, final int firstRow, final int maxRows, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().getLike(l, firstRow, maxRows, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}
	
	@Override
	public void countLike(final IPersistentObject p, final AsyncCallback<Long> callback) {
		getService().countLike(p, callback);
	}
	
	@Override
	public void countLike(final List<IPersistentObject> l, final AsyncCallback<Long> callback) {
		getService().countLike(l, callback);
	}

	@Override
	public void insert(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		getService().insert(p, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void save(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		getService().save(p, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void saveList(final List<IPersistentObject> l, final AsyncCallback<List<IPersistentObject>> callback) {
		getService().saveList(l, new AsyncCallback<List<IPersistentObject>>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				setList(result);
				callback.onSuccess(result);
			}
		});
	}

	@Override
	public void update(final IPersistentObject p, final AsyncCallback<IPersistentObject> callback) {
		getService().update(p, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
			@Override
			public void onSuccess(IPersistentObject result) {
				setSingle(result);
				callback.onSuccess(result);
			}
		});
	}

}
