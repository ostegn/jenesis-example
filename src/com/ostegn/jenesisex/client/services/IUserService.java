package com.ostegn.jenesisex.client.services;


import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

/**
 * 
 * This class defines all needed user transactions, since we can't use PersistentService because of security reasons. 
 * 
 * @author Thiago Ricciardi
 * 
 * @see IUserServiceAsync
 * @see UserService
 */
@RemoteServiceRelativePath(Services.Paths.IUserService)
public interface IUserService extends RemoteService {
	
	/**
	 * Check if this user exists.<br>
	 * It uses the ID, the username and the e-mail to do this check.
	 * @param user The User to check for existence.
	 * @return True if the user exists.
	 */
	Boolean userExists(User user);
	
	/**
	 * Register the user in the network.
	 * @param user The User to be registered.
	 * @param user_details The User's UserDetails.
	 * @return The registered user.
	 * @throws JenesisException For any problems registering the user.
	 */
	User signUp(User user, UserDetails user_details) throws JenesisException;
	
	/**
	 * Changes the password of the logged user.
	 * @param currentPass The current password for security check.
	 * @param newPass The new password.
	 * @return The updated user.
	 * @throws JenesisException For any problems changing the user's password.
	 */
	User changePass(String currentPass, String newPass) throws JenesisException;
	
	/**
	 * Changes the password using a token.<br>
	 * Ends the process of a reset password procedure.
	 * @param token The token generated in {@code forgotPass} method.
	 * @param hash The hash that is used to verify the user.
	 * @param newPass The new password.
	 * @throws JenesisException For any problems with the token or problems changing the user's password.
	 */
	void changePass(String token, String hash, String newPass) throws JenesisException;
	
	/**
	 * Generates a token and send an e-mail to the user.<br>
	 * Starts the process of a reset password procedure.
	 * @param email The user's e-mail.
	 * @throws JenesisException For any problems generating the token or sending the e-mail.
	 */
	void forgotPass(String email) throws JenesisException;
	
	/**
	 * Checks if the password reset token is active.
	 * @param token The token to be checked.
	 * @param hash The hash for user validation.
	 * @return <b>true</b> if the token is valid and active. It throws an error otherwise.
	 * @throws JenesisException If the token is not valid or for any problems encountered in the process.
	 */
	Boolean checkPassToken(String token, String hash) throws JenesisException;
	
	/**
	 * Checks if the e-mail exists in the database.
	 * @param email The e-mail to check.
	 * @return True if the e-mail exists.
	 */
	Boolean emailExists(String email);
	
	/**
	 * Checks if the username exists in the database.
	 * @param username The username to check.
	 * @return True if the username exists.
	 */
	Boolean usernameExists(String username);
	
	/**
	 * Gets the logged user.
	 * @return The {@link User} object representing the logged user.
	 */
	User getUser();
	
	/**
	 * Sends an e-mail to the support team.
	 * @param message The message to send.
	 * @throws JenesisException For any problems sending the e-mail.
	 */
	void contactSupport(String message) throws JenesisException;
	
}
