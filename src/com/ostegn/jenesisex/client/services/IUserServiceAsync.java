package com.ostegn.jenesisex.client.services;


import com.google.gwt.user.client.rpc.AsyncCallback;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

/**
 * 
 * Used to realize all transactions of the user since we can't use PersistentService because of security reasons.<br>
 * Remember that this is an Async Service, so you will have to create <b>callback functions</b>.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IUserService
 * @see UserService
 */
public interface IUserServiceAsync {
	
	/**
	 * Register the user in the network.
	 * @param user The User to be registered.
	 * @param user_details The User's UserDetails.
	 * @param callback The callback function of return.
	 */
	void signUp(User user, UserDetails user_details, AsyncCallback<User> callback);
	
	/**
	 * Check if this user exists.<br>
	 * It uses the ID, the username and the e-mail to do this check.
	 * @param user The User to check for existence.
	 * @param callback The callback function of return.
	 */
	void userExists(User user, AsyncCallback<Boolean> callback);
	
	/**
	 * Changes the password of the logged user.
	 * @param currentPass The current password for security check.
	 * @param newPass The new password.
	 * @param callback The callback function of return.
	 */
	void changePass(String currentPass, String newPass, AsyncCallback<User> callback);
	
	/**
	 * Changes the password using a token.<br>
	 * Ends the process of a reset password procedure.
	 * @param token The token generated in {@code forgotPass} method.
	 * @param hash The hash that is used to verify the user.
	 * @param newPass The new password.
	 * @param callback The callback function of return (void).
	 */
	void changePass(String token, String hash, String newPass, AsyncCallback<Void> callback);
	
	/**
	 * Generates a token and send an e-mail to the user.<br>
	 * Starts the process of a reset password procedure.
	 * @param email The user's e-mail.
	 * @param callback The callback function of return (void).
	 */
	void forgotPass(String email, AsyncCallback<Void> callback);
	
	/**
	 * Checks if the password reset token is active.
	 * @param token The token to be checked.
	 * @param hash The hash for user validation.
	 * @param callback The callback function of return.
	 */
	void checkPassToken(String token, String hash, AsyncCallback<Boolean> callback);
	
	/**
	 * Checks if the e-mail exists in the database.
	 * @param email The e-mail to check.
	 * @param callback The callback function of return.
	 */
	void emailExists(String email, AsyncCallback<Boolean> callback);
	
	/**
	 * Checks if the username exists in the database.
	 * @param username The username to check.
	 * @param callback The callback function of return.
	 */
	void usernameExists(String username, AsyncCallback<Boolean> callback);
	
	/**
	 * Gets the logged user.
	 * @param callback The callback function of return.
	 */
	void getUser(AsyncCallback<User> callback);
	
	/**
	 * Sends an e-mail to the support team.
	 * @param message The message to send.
	 * @param callback The callback function of return.
	 */
	void contactSupport(String message, AsyncCallback<Void> callback);

}
