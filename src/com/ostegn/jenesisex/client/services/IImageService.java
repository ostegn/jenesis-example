package com.ostegn.jenesisex.client.services;

import java.math.BigInteger;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.shared.persistent.ImageBase;
import com.ostegn.jenesisex.shared.persistent.SimpleImage;


@RemoteServiceRelativePath(Services.Paths.IImageService)
public interface IImageService extends RemoteService {
	
	String getBaseURL();
	
	String getAbsoluteURL(ImageBase i);
	
	SimpleImage get(SimpleImage i);
	
	SimpleImage save(ImageBase i) throws JenesisException;
	
	void del(SimpleImage i);
	
	List<SimpleImage> getUserImages();
	
	List<SimpleImage> getUserImages(BigInteger iduser);
	
	void rebuildImageSizes() throws JenesisException;
	
}
