package com.ostegn.jenesisex.client.services;

import java.util.Date;
import java.util.List;



import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.server.PersistentService;

/**
 * 
 * This class defines the interface of a persistent service used to manupulate the objects of a database.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentServiceAsync
 * @see PersistentService
 * 
 */
@RemoteServiceRelativePath(Services.Paths.IPersistentService)
public interface IPersistentService extends RemoteService {
	
	/**
	 * Gets the current date/time from the server.
	 * @return The current date/time.
	 */
	Date getDate();
	
	/**
	 * Gets a persistent object.
	 * @param p The persistent object to get.
	 * @return The persistent object filled.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject get(IPersistentObject p) throws JenesisException;
	
	/**
	 * Saves (insert or update) a persistent object.
	 * @param p The persistent object to be saved.
	 * @return The persistent object with all changes made during save process.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject save(IPersistentObject p) throws JenesisException;
	
	/**
	 * Deletes a persistent object.
	 * @param p The persistent object to be deleted.
	 * @return The object after delete procedure.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject del(IPersistentObject p) throws JenesisException;
	
	/**
	 * Forces the insert of a new persistent object into database.
	 * @param p The persistent object to be inserted.
	 * @return The persistent object with all changes made during insert process.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject insert(IPersistentObject p) throws JenesisException;
	
	/**
	 * Forces the update of an already existing persistent object.
	 * @param p The persistent object to be updated.
	 * @return The persistent object with all changes made during update process.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject update(IPersistentObject p) throws JenesisException;
	
	
	/**
	 * Dumps the complete list of persisted objects from the database (all the table).<br>
	 * Be careful, this method can take some time to load from big tables.<br>
	 * <br>
	 * This method is equivalent to <b>getList(new YourPersistentObj())</b>
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @return The list of persistent objects fetched.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getAll(IPersistentObject p) throws JenesisException;
	
	/**
	 * Counts all table rows.
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @return The number of all objects persisted.
	 * @throws JenesisException In case of any error.
	 */
	Long countAll(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets the list of persisted objects from the database given the object of example (if all parameters are null, gets the whole table) ordered from newest to oldest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param p The persistent object of example.
	 * @return The list of persistent objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getAllLatest(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets the list of persisted objects from the database given the list of objects of example (if all parameters are null, gets the whole table) ordered from newest to oldest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param l The list of persistent objects of example.
	 * @return The list of persistent objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getAllLatest(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets the latest persisted object from the database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the latest of the whole table).
	 * @return The latest persisted object that match the example.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject getLatest(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets the latest persisted object from the database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the latest of the whole table).
	 * @return The latest persisted object that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject getLatest(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets the limited list of the latest persisted objects from database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the latest persisted objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getLatest(IPersistentObject p, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Gets the limited list of the latest persisted objects from database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the latest persisted objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getLatest(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Gets the list of persisted objects from the database given the object of example (if all parameters are null, gets the whole table) ordered from oldest to newest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param p The persistent object of example.
	 * @return The list of persistent objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getAllFirst(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets the list of persisted objects from the database given the list of objects of example (if all parameters are null, gets the whole table) ordered from oldest to newest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param l The list of persistent objects of example.
	 * @return The list of persistent objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getAllFirst(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets the first persisted object from the database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the first of the whole table).
	 * @return The first persisted object that match the example.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject getFirst(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets the first persisted object from the database according to the versioning system given the list of objects of example.
	 * @param l The persistent object of example (if all parameters are null gets the first of the whole table).
	 * @return The first persisted object that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	IPersistentObject getFirst(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets the limited list of the first persisted objects from database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the first persisted objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getFirst(IPersistentObject p, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Gets the limited list of the first persisted objects from database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @return The list containing the first persisted objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getFirst(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Gets a list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @return A list of persisted objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getList(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets a list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return A list of persisted objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getList(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets a limited list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getList(IPersistentObject p, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Gets a limited list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getList(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Counts a list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @return The number of persisted objects that matches the example.
	 * @throws JenesisException In case of any error.
	 */
	Long countList(IPersistentObject p) throws JenesisException;
	
	/**
	 * Counts a list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return The number of persisted objects that matches the examples.
	 * @throws JenesisException In case of any error.
	 */
	Long countList(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets a list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @return A list of persisted objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getLike(IPersistentObject p) throws JenesisException;
	
	/**
	 * Gets a list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return A list of persisted objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getLike(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Gets a limited list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match the example.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getLike(IPersistentObject p, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Gets a limited list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @return A list of persisted objects that match the examples.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> getLike(List<IPersistentObject> l, int firstRow, int maxRows) throws JenesisException;
	
	/**
	 * Counts a list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @return The number of persisted objects that matches like the example.
	 * @throws JenesisException In case of any error.
	 */
	Long countLike(IPersistentObject p) throws JenesisException;
	
	/**
	 * Counts a list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @return The number of persisted objects that matches like the examples.
	 * @throws JenesisException In case of any error.
	 */
	Long countLike(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Saves (insert or update) an entire list of persistent objects.
	 * @param l The persistent object list with all objects to be saved.
	 * @return The persistent object list with all changes made during save process.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> saveList(List<IPersistentObject> l) throws JenesisException;
	
	/**
	 * Deletes an entire list of objects.
	 * @param l The persistent object list with all objects to be deleted.
	 * @return The persistent object list after delete procedure.
	 * @throws JenesisException In case of any error.
	 */
	List<IPersistentObject> delList(List<IPersistentObject> l) throws JenesisException;
	
}
