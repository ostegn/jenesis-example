package com.ostegn.jenesisex.client.services;

import java.util.Date;
import java.util.List;



import com.google.gwt.user.client.rpc.AsyncCallback;
import com.ostegn.jenesis.shared.base.IPersistentObject;

/**
 * Used to get and save all database objects.<br>
 * For security reasons, this class cannot be used to persist the following classes:<br><br>
 * 
 * {@code User}<br><br>
 * 
 * Remember that this is an Asynchronous Service, so you will have to create <b>callback functions</b>.
 * 
 * @author Thiago Ricciardi
 * 
 * @see IPersistentService
 * @see PersistentService
 * @see IPersistentObject
 */
public interface IPersistentServiceAsync {
	
	/**
	 * Gets the current date/time from the server.
	 * @param callback To be executed after current time is fetched.
	 */
	void getDate(AsyncCallback<Date> callback);
	
	/**
	 * Deletes a persistent object.
	 * @param p The persistent object to be deleted.
	 * @param callback To be executed after deleted.
	 */
	void del(IPersistentObject p, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Deletes an entire list of objects.
	 * @param l The persistent object list with all objects to be deleted.
	 * @param callback To be executed after deleted.
	 */
	void delList(List<IPersistentObject> l, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a persistent object.
	 * @param p The persistent object to get.
	 * @param callback To be executed after get.
	 */
	void get(IPersistentObject p, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Dumps the complete list of persisted objects from the database (all the table).<br>
	 * Be careful, this method can take some time to load from big tables.
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @param callback To be executed after get.
	 */
	void getAll(IPersistentObject p, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Counts all table rows.
	 * @param p The persistent object (cannot be null, but it's just for class assignment).
	 * @param callback The number of all objects persisted.
	 */
	void countAll(IPersistentObject p, AsyncCallback<Long> callback);
	
	/**
	 * Gets the list of persisted objects from the database given the object of example (if all parameters are null, gets the whole table) ordered from newest to oldest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param p The persistent object of example.
	 * @param callback The list of persistent objects fetched.
	 */
	void getAllLatest(IPersistentObject p, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the list of persisted objects from the database given the list of objects of example (if all parameters are null, gets the whole table) ordered from newest to oldest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param l The list of persistent objects of example.
	 * @param callback The list of persistent objects that match the examples.
	 */
	void getAllLatest(List<IPersistentObject> l, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the latest persisted object from the database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the latest of the whole table).
	 * @param callback The latest persisted object that match the example.
	 */
	void getLatest(IPersistentObject p, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Gets the latest persisted object from the database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the latest of the whole table).
	 * @param callback The latest persisted object that match the examples.
	 */
	void getLatest(List<IPersistentObject> l, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Gets the limited list of the latest persisted objects from database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @param callback The list containing the latest persisted objects that match the example.
	 */
	void getLatest(IPersistentObject p, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the limited list of the latest persisted objects from database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @param callback The list containing the latest persisted objects that match the examples.
	 */
	void getLatest(List<IPersistentObject> l, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the list of persisted objects from the database given the object of example (if all parameters are null, gets the whole table) ordered from oldest to newest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param p The persistent object of example.
	 * @param callback The list of persistent objects that match the example.
	 */
	void getAllFirst(IPersistentObject p, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the list of persisted objects from the database given the list of objects of example (if all parameters are null, gets the whole table) ordered from oldest to newest.<br>
	 * Be careful, if all parameters are null this method can take some time to load from big tables.
	 * @param l The list of persistent objects of example.
	 * @param callback The list of persistent objects that match the examples.
	 */
	void getAllFirst(List<IPersistentObject> l, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the first persisted object from the database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the first of the whole table).
	 * @param callback The first persisted object that match the example.
	 */
	void getFirst(IPersistentObject p, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Gets the first persisted object from the database according to the versioning system given the list of objects of example.
	 * @param l The persistent object of example (if all parameters are null gets the first of the whole table).
	 * @param callback The first persisted object that match the examples.
	 */
	void getFirst(List<IPersistentObject> l, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Gets the limited list of the first persisted objects from database according to the versioning system given the object of example.
	 * @param p The persistent object of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @param callback The list containing the first persisted objects that match the example.
	 */
	void getFirst(IPersistentObject p, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets the limited list of the first persisted objects from database according to the versioning system given the list of objects of example.
	 * @param l The list of persistent objects of example (if all parameters are null gets the whole table).
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of rows to return.
	 * @param callback The list containing the first persisted objects that match the examples.
	 */
	void getFirst(List<IPersistentObject> l, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @param callback To be executed after get.
	 */
	void getList(IPersistentObject p, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param callback To be executed after get.
	 */
	void getList(List<IPersistentObject> l, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a limited list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @param callback To be executed after get.
	 */
	void getList(IPersistentObject p, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a limited list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @param callback To be executed after get.
	 */
	void getList(List<IPersistentObject> l, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Counts a list of persistent objects given the object of example.
	 * @param p The persistent object of example.
	 * @param callback The number of persisted objects that matches the example.
	 */
	void countList(IPersistentObject p, AsyncCallback<Long> callback);
	
	/**
	 * Counts a list of persistent objects given the list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param callback The number of persisted objects that matches the example.
	 */
	void countList(List<IPersistentObject> l, AsyncCallback<Long> callback);
	
	/**
	 * Gets a list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @param callback To be executed after get.
	 */
	void getLike(IPersistentObject p, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param callback To be executed after get.
	 */
	void getLike(List<IPersistentObject> l, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a limited list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @param callback To be executed after get.
	 */
	void getLike(IPersistentObject p, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Gets a limited list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param firstRow The first row to return.
	 * @param maxRows The maximum number of results to return.
	 * @param callback To be executed after get.
	 */
	void getLike(List<IPersistentObject> l, int firstRow, int maxRows, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Counts a list of persistent objects like the given object of example.
	 * @param p The persistent object of example.
	 * @param callback The number of persisted objects that matches like the example.
	 */
	void countLike(IPersistentObject p, AsyncCallback<Long> callback);
	
	/**
	 * Counts a list of persistent objects like the given list of objects of example.
	 * @param l The list of persistent objects of example.
	 * @param callback The number of persisted objects that matches like the example.
	 */
	void countLike(List<IPersistentObject> l, AsyncCallback<Long> callback);
	
	/**
	 * Forces an insert of a new persistent object into database.
	 * @param p The persistent object to be inserted.
	 * @param callback To be executed after insert.
	 */
	void insert(IPersistentObject p, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Saves (insert or update) a persistent object.
	 * @param p The persistent object to be saved.
	 * @param callback To be executed after save.
	 */
	void save(IPersistentObject p, AsyncCallback<IPersistentObject> callback);
	
	/**
	 * Saves (insert or update) an entire list of persistent objects.
	 * @param l The persistent object list with all objects to be saved.
	 * @param callback To be executed after save.
	 */
	void saveList(List<IPersistentObject> l, AsyncCallback<List<IPersistentObject>> callback);
	
	/**
	 * Forces an update of an already existing persistent object.
	 * @param p The persistent object to be updated.
	 * @param callback To be executed after update.
	 */
	void update(IPersistentObject p, AsyncCallback<IPersistentObject> callback);

}
