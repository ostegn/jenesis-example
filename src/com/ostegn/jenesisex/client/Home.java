package com.ostegn.jenesisex.client;


import org.gwtbootstrap3.client.ui.Anchor;
import org.gwtbootstrap3.client.ui.AnchorListItem;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.JenesisEnvironment;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.base.ThemeManager;
import com.ostegn.jenesisex.client.ui.AppInfo;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

public class Home extends JenesisEnvironment {
	
	/**
	 * This is the entry point method.
	 */
	@Override
	public void onEnvironmentLoad() {
		
		final AnchorListItem dashboard = new AnchorListItem("Dashboard");
		final AnchorListItem link = new AnchorListItem("Link");
		
		final ClickHandler onClick = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if ( (event.getSource() instanceof Anchor) ? ((Anchor) event.getSource()).getParent() instanceof AnchorListItem : false ) {
					AnchorListItem src = (AnchorListItem) ((Anchor) event.getSource()).getParent();
					mainMenu.setActive(src);
					final RootPanel homePanel = RootPanel.get("userData");
					homePanel.clear();
					if (src == dashboard) {
						homePanel.add(new Label("Experimental phase!"));
						homePanel.add(new Label("Object: " + getUser().toString()));
						homePanel.add(new Label("Username: " + getCredential().getUsername()));
						homePanel.add(new Label("Password (encoded): " + getCredential().getPassword()));
						homePanel.add(new Label("Permissions: " + getCredential().getJAuthorities().toString()));
						homePanel.add(new Label("E-mail: " + getUser().getEmail()));
						homePanel.add(new Label("Last login: " + getDateTimeFormat().format(getUser().getLastlogin())));
						homePanel.add(new Label("Login count: " + getUser().getLogincount().toString()));
						homePanel.add(new Label("Theme: " + ThemeManager.getCurrentTheme().getDescr()));
					}
					else if (src == link) {
						homePanel.add(new AppInfo("Version:"));
						final UserDetails ud = new UserDetails();
						ud.setIduser(getUser().getIduser());
						Services.getPersistentService().get(ud, new AsyncCallback<IPersistentObject>() {
							
							@Override
							public void onSuccess(IPersistentObject result) {
								UserDetails rst = (UserDetails) result;
								homePanel.add(new Label("Name: " + rst.getDescr()));
								homePanel.add(new Label("Sex: " + rst.getGender().toString()));
								homePanel.add(new Label("Birthday: " + getDateFormat().format(rst.getBirthday())));
							}
							
							@Override
							public void onFailure(Throwable caught) {
								// TODO max retries
								Services.getPersistentService().get(ud, this);
							}
						});
					}
				}
			}
		};

		dashboard.addClickHandler(onClick);
		link.addClickHandler(onClick);
		mainMenu.addItem(dashboard);
		mainMenu.addItem(link);
		
		onClick.onClick(new ClickEvent() {
			@Override
			public Object getSource() {
				return dashboard.getWidget(0);
			}
		});
		
	}
	
}
