package com.ostegn.jenesisex.client.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.FormElement;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * 
 * Web Pages catalog and link manipulation.<br>
 * Should always be used to redirect pages and generate URLs.
 * 
 * @author Thiago Ricciardi
 *
 */
public class WebPages {
	
	/** GWT pages extension */
	public static final String PAGE_EXT = "html";
	
	/** Pages catalog enumeration*/
	public enum Page {
		
		/** Main home when the user is signed in */
		HOME("home"),
		/** The main page where the unsigned users come in */
		MAIN("main"),
		
		;
		
		private final String id;
		
		Page(String id) {
			this.id = id;
		}
		
		/** Gets the ID of the page (also the page name without extension) */
		public String getId() {
			return id;
		}
		
		/** Gets the page relative URL (that is the page ID plus extension) */
		public String getRelativeURL() {
			return id + "." + PAGE_EXT;
		}
		
	}
	
	@SuppressWarnings("serial")
	private static final List<String> parametersToKeep = new ArrayList<String>() {
		{
			add("gwt.codesvr");
			add("locale");
			add(ThemeManager.THEME_PARAMETER);
		}
	};
	
	/**
	 * Add a map of Query strings to a URL, keeping the existing ones
	 * @param url the original URL.
	 * @param parameters The query string parameters map.
	 * @return an string representing the formed URL.
	 */
	public static String addQueryString(String url, Map<String, String> parameters) {
		String ret = url;
		if (parameters != null) {
			if (!parameters.isEmpty()) {
				if (ret.contains("?")) ret += "&";
				else ret += "?";
				for (String key : parameters.keySet()) {
					String value = parameters.get(key);
					if ((value != null) ? (value.length() > 0) : false) {
						ret += key + "=" + value + "&";
					}
				}
				ret = ret.substring(0, ret.length()-1);
			};
		}
		return ret;
	}
	
	/**
	 * Add a Query string to a URL, keeping the existing ones
	 * @param url the original URL.
	 * @param paramKey The key of the query string parameter.
	 * @param paramValue The value of the query string parameter.
	 * @return an string representing the formed URL.
	 */
	public static String addQueryString(String url, String paramKey, String paramValue) {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(paramKey, paramValue);
		return addQueryString(url, parameters);
	}
	
	/**
	 * Add a Hash value to a URL, replacing the old one (if existent)
	 * @param url the original URL.
	 * @param hash The hash value.
	 * @return an string representing the formed URL.
	 */
	public static String addHashString(String url, String hash) {
		String ret = url;
		if (ret.contains("#")) {
			ret = ret.substring(0, ret.indexOf("#"));
		}
		ret += "#" + hash;
		return ret;
	}
	
	private static String getQueryString(Map<String, String> parameters) {
		return addQueryString("", parameters);
	}
	
	private static String getQueryString(String paramKey, String paramValue) {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(paramKey, paramValue);
		return getQueryString(parameters);
	}
	
	private static String getHashString(String hash) {
		return addHashString("", hash);
	}
	
	/** Returns the default landing page */
	public static final Page getDefault() {
		return Page.HOME;
	}
	
	/**
	 * Gets an URL to the given page.
	 * @param page The page to generate URL from.
	 * @return an string representing the URL.
	 */
	public static final String getURL(Page page) {
		return page.getRelativeURL();
	}
	/**
	 * Gets an URL to the given page with the given parameters.
	 * @param page The page to generate URL from.
	 * @param parameters The query string parameters map.
	 * @return an string representing the URL.
	 */
	public static final String getURL(Page page, Map<String, String> parameters) {
		return page.getRelativeURL() + getQueryString(parameters);
	}
	/**
	 * Gets an URL to the given page with one parameter key/value pair.
	 * @param page The page to generate URL from.
	 * @param paramKey The key of the query string parameter.
	 * @param paramValue The value of the query string parameter.
	 * @return an string representing the URL.
	 */
	public static final String getURL(Page page, String paramKey, String paramValue) {
		return page.getRelativeURL() + getQueryString(paramKey, paramValue);
	}
	/**
	 * Gets an URL to the given page with a hash value.
	 * @param page The page to generate URL from.
	 * @param hash The hash value.
	 * @return an string representing the URL.
	 */
	public static final String getURL(Page page, String hash) {
		return page.getRelativeURL() + getHashString(hash);
	}
	/**
	 * Gets an URL to the given page with one parameter key/value pair and a hash value.
	 * @param page The page to generate URL from.
	 * @param paramKey The key of the query string parameter.
	 * @param paramValue The value of the query string parameter.
	 * @param hash The hash value.
	 * @return an string representing the URL.
	 */
	public static final String getURL(Page page, String paramKey, String paramValue, String hash) {
		return page.getRelativeURL() + getQueryString(paramKey, paramValue) + getHashString(hash);
	}
	/**
	 * Gets an URL to the given page with the given parameters and a hash value.
	 * @param page The page to generate URL from.
	 * @param parameters The query string parameters map.
	 * @param hash The hash value.
	 * @return an string representing the URL.
	 */
	public static final String getURL(Page page, Map<String, String> parameters, String hash) {
		return page.getRelativeURL() + getQueryString(parameters) + getHashString(hash);
	}
	
	/**
	 * Redirects the browser to the given page.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param page The page to go.
	 */
	public static final void goTo(Page page) {
		goTo(getURL(page));
	}
	/**
	 * Redirects the browser to the given page with the given parameters.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param page The page to go.
	 * @param parameters The query string parameters map.
	 */
	public static final void goTo(Page page, Map<String, String> parameters) {
		goTo(getURL(page, parameters));
	}
	/**
	 * Redirects the browser to the given page with one parameter key/value pair.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param page The page to go.
	 * @param paramKey The key of the query string parameter.
	 * @param paramValue The value of the query string parameter.
	 */
	public static final void goTo(Page page, String paramKey, String paramValue) {
		goTo(getURL(page, paramKey, paramValue));
	}
	/**
	 * Redirects the browser to the given page with a hash value.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param page The page to go.
	 * @param hash The hash value.
	 */
	public static final void goTo(Page page, String hash) {
		goTo(getURL(page, hash));
	}
	/**
	 * Redirects the browser to the given page with one parameter key/value pair and a hash value.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param page The page to go.
	 * @param paramKey The key of the query string parameter.
	 * @param paramValue The value of the query string parameter.
	 * @param hash The hash value.
	 */
	public static final void goTo(Page page, String paramKey, String paramValue, String hash) {
		goTo(getURL(page, paramKey, paramValue, hash));
	}
	/**
	 * Redirects the browser to the given page with the given parameters and a hash value.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param page The page to go.
	 * @param parameters The query string parameters map.
	 * @param hash The hash value.
	 */
	public static final void goTo(Page page, Map<String, String> parameters, String hash) {
		goTo(getURL(page, parameters, hash));
	}
	/**
	 * Redirects the browser to the given URL.
	 * <br>Automatically verifies parameters to keep and adapt the URL correctly.
	 * @param url The URL string to go.
	 */
	public static final void goTo(String url) {
		if (UriUtils.extractScheme(url) == null) url = GWT.getHostPageBaseURL() + url;
		for (String key : parametersToKeep) {
			String value = Location.getParameter(key);
			if ( (value != null) ? (value.length() > 0) : false ) {
				url = addQueryString(url, key, value);
			}
		}
		Location.assign(url);
	}
	
	/** Reloads the current page */
	public static final void reload() {
		Location.reload();
	}
	
	/** Signs out of the system */
	public static final void logout() {
		doPost(Config.getLogoutAction(), Config.getCSRFParameterName(), Config.getCSRFToken());
	}
	
	/**
	 * Verifies if the current URL matches with the configuration URL.<br>
	 * <b>Redirects to the correct URL if the current is wrong.</b>
	 * @return true if it is correct, false otherwise.
	 */
	public static Boolean verifyURL() {
		return verifyURL(true);
	}
	/**
	 * Verifies if the current URL matches with the configuration URL.
	 * @param autoRedirect Set to true if you want to redirect to the correct URL if the current is wrong, set false otherwise.
	 * @return true if it is correct, false otherwise.
	 */
	public static Boolean verifyURL(Boolean autoRedirect) {
		Boolean ret = (GWT.getHostPageBaseURL().startsWith(Config.getBaseURL()));
		if (!ret && autoRedirect) {
			WebPages.goTo(Config.getBaseURL() + "/" + Location.getHref().replace(GWT.getHostPageBaseURL(), ""));
		}
		return ret;
	}
	
	/**
	 * Redirects the browser to the given page with one parameter key/value pair using POST method.
	 * @param page The page to post to.
	 * @param paramKey The key of the post parameter.
	 * @param paramValue The value of the post parameter
	 */
	public static final void doPost(Page page, String paramKey, String paramValue) {
		doPost(getURL(page), paramKey, paramValue);
	}
	
	/**
	 * Redirects the browser to the given page with the given parameters using POST method.
	 * @param page The page to post to.
	 * @param parameters The posting parameters map.
	 */
	public static final void doPost(Page page, Map<String, String> parameters) {
		doPost(getURL(page), parameters);
	}
	
	/**
	 * Redirects the browser to the given page using POST method.
	 * @param page The page to post to.
	 */
	public static final void doPost(Page page) {
		doPost(getURL(page), null);
	}
	
	/**
	 * Redirects the browser to the given URL with one parameter key/value pair using POST method.
	 * @param url The URL string to post to.
	 * @param paramKey The key of the post parameter.
	 * @param paramValue The value of the post parameter.
	 */
	public static final void doPost(String url, String paramKey, String paramValue) {
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put(paramKey, paramValue);
		doPost(url, parameters);
	}
	
	/**
	 * Redirects the browser to the given URL with the given parameters using POST method.
	 * @param url The URL string to post to.
	 * @param parameters The posting parameters map.
	 */
	public static final void doPost(String url, Map<String, String> parameters) {
		final RootPanel root = RootPanel.get();
		final FormPanel form = new FormPanel();
		final FlowPanel main = new FlowPanel();
		root.add(form);
		form.add(main);
		if (parameters != null) {
			for (String key : parameters.keySet()) {
				main.add(new Hidden(key, parameters.get(key)));
			}
		}
		form.getElement().<FormElement>cast().setTarget("");
		form.setMethod(FormPanel.METHOD_POST);
		form.setAction(url);
		form.submit();
	}
	
	/**
	 * Redirects the browser to the given URL using POST method.
	 * @param url The URL string to post to.
	 */
	public static final void doPost(String url) {
		doPost(url, null);
	}

}
