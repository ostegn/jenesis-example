package com.ostegn.jenesisex.client.base;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RpcRequestBuilder;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.ostegn.jenesisex.client.services.IImageService;
import com.ostegn.jenesisex.client.services.IImageServiceAsync;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.client.services.IUserService;
import com.ostegn.jenesisex.client.services.IUserServiceAsync;
import com.ostegn.jenesisex.client.services.PersistentManager;


public final class Services {
	
	private static JenesisRpcRequestBuilder rpcRequestBuilder;
	
	private Services() {}
	
	private static IImageServiceAsync imageService;
	public static IImageServiceAsync getImageService() {
		if (imageService == null) {
			imageService = GWT.create(IImageService.class);
			((ServiceDefTarget) imageService).setRpcRequestBuilder(rpcRequestBuilder);
		}
		return imageService;
	}
	
	private static IPersistentServiceAsync persistentService;
	public static IPersistentServiceAsync getPersistentService() {
		if (persistentService == null) persistentService = PersistentManager.getInstance();
		return persistentService;
	}
	
	private static IUserServiceAsync userService;
	public static IUserServiceAsync getUserService() {
		if (userService == null) {
			userService = GWT.create(IUserService.class);
			((ServiceDefTarget) userService).setRpcRequestBuilder(rpcRequestBuilder);
		}
		return userService;
	}
	
	public static final class Paths {
		
		public static final String PATH = "../srv/";
		
		public static final String ImageService = "imageService";
		public static final String IImageService = PATH + ImageService;
		
		public static final String PersistentService = "persistentService";
		public static final String IPersistentService = PATH + PersistentService;
		
		public static final String UserService = "userService";
		public static final String IUserService = PATH + UserService;
		
	}
	
	public static RpcRequestBuilder getRpcRequestBuilder() {
		if (rpcRequestBuilder == null) rpcRequestBuilder = new JenesisRpcRequestBuilder();
		return rpcRequestBuilder;
	}

}
