package com.ostegn.jenesisex.client.base;

/**
 * 
 * Static images catalog.
 * 
 * @author Thiago Ricciardi
 *
 */
public enum StaticImage {
	
	/** Logo */
	LOGO("logo.png");
	
	/** Relative root directory of the images */
	public final static String ROOT = "img/";
	
	private String source;
	
	StaticImage(String source) {
		this.source = source;
	}
	
	/** Gets the source of the static image (the filename) */
	public String getSource() {
		return source;
	}
	
	/** Gets the relative path of this image */
	public String getRelativeSource() {
		return ROOT + getSource();
	}

	/** Gets the absolute path of this image */
	public String getAbsoluteSource() {
		return Config.getBaseURL() + getRelativeSource();
	}
	
}
