package com.ostegn.jenesisex.client.base;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import com.google.gwt.core.client.Callback;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.ScriptInjector;
import com.google.gwt.core.client.ScriptInjector.FromUrl;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.LocaleInfo;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.ostegn.jenesis.shared.base.AuthLevel;
import com.ostegn.jenesis.shared.base.ICredential;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.client.services.IUserServiceAsync;
import com.ostegn.jenesisex.client.ui.MainMenu;
import com.ostegn.jenesisex.client.ui.SignOut;
import com.ostegn.jenesisex.shared.persistent.User;

/**
 * 
 * Defines and implements the main Jenesis environment.<br>
 * All pages should extends this class or implement {@link EntryPoint}.
 * 
 * @author Thiago Ricciardi
 *
 */
public abstract class JenesisEnvironment implements EntryPoint {
	
	private int loadCount = 0;
	
	protected MainMenu mainMenu;
	
	/** Main GWT entry point */
	@Override
	public void onModuleLoad() {
		preload();
		loadConfig(new Callback<Void, Exception>() {
			@Override
			public void onFailure(Exception reason) {
				// TODO max retries
				loadConfig(this);
			}
			@Override
			public void onSuccess(Void result) {
				load();
			}
		});
	}
	
	private void load() {
		if (isAuthenticationRequired()) {
			getServerDate();
			loadCredential();
		}
		else {
			finished();
		}
	}
	
	/** To be executed after the environment is loaded and ready. */
	public abstract void onEnvironmentLoad();
	
	/** Override this function if the authentication is not required */
	public Boolean isAuthenticationRequired() {
		return true;
	}
	
	/**
	 * Gets the current locale name
	 * @return a string with the current locale name (en_US, pt_BR, etc...)
	 */
	public static String getLocaleName() {
		return LocaleInfo.getCurrentLocale().getLocaleName();
	}
	
	private static Long serverDateDiff = 0L;
	/**
	 * Sync a client date with server date
	 * @param clientDate the client date
	 * @return a new date representing the server date relative to the client date
	 */
	public static Date toServerDate(Date clientDate) {
		return new Date(clientDate.getTime() + serverDateDiff);
	}
	
	private void getServerDate() {
		begin();
		getPersistentService().getDate(new AsyncCallbackHandler<Date>(new AsyncCallback<Date>() {

			@Override
			public void onSuccess(Date result) {
				Date now = new Date();
				serverDateDiff = result.getTime() - now.getTime();
				end();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO max retries
				if (caught.getMessage().contains("<html")) Window.Location.reload();
				getServerDate();
				end();
			}
			
		}));
	}
	
	private static IPersistentServiceAsync persistentService;
	private static IPersistentServiceAsync getPersistentService() {
		if (persistentService == null) persistentService = Services.getPersistentService();
		return persistentService;
	}
	
	private static IUserServiceAsync userService;
	private static IUserServiceAsync getUserService() {
		if (userService == null) userService = Services.getUserService();
		return userService;
	}
	
	private static DateTimeFormat dateFormat;
	/** Returns the properties defined date format */
	public static DateTimeFormat getDateFormat() {
		if (dateFormat == null) dateFormat = DateTimeFormat.getFormat(Config.getDateFormatString());
		return dateFormat;
	}
	private static DateTimeFormat timeFormat;
	/** Returns the properties defined time format */
	public static DateTimeFormat getTimeFormat() {
		if (timeFormat == null) timeFormat = DateTimeFormat.getFormat(Config.getTimeFormatString());
		return timeFormat;
	}
	private static DateTimeFormat datetimeFormat;
	/** Returns the properties defined date/time format */
	public static DateTimeFormat getDateTimeFormat() {
		if (datetimeFormat == null) datetimeFormat = DateTimeFormat.getFormat(Config.getDateTimeFormatString());
		return datetimeFormat;
	}
	private static DateTimeFormat JSONdatetimeFormat;
	/** Returns the JSON defined date/time format */
	public static DateTimeFormat getJSONdatetimeFormat() {
		if (JSONdatetimeFormat == null) JSONdatetimeFormat = DateTimeFormat.getFormat(Config.getJSONDateTimeFormatString());
		return JSONdatetimeFormat;
	}
	
	private static ICredential credential;
	/** Returns the credential of the current access or <b>null</b> if not logged in. */
	public static ICredential getCredential() {
		return credential;
	}
	/** Returns the user of the current session. */
	public static User getUser() {
		return (User) credential;
	}
	/** Returns the list of all applicable levels of the current user */
	public static Collection<AuthLevel> getAuthLevel() {
		return (getUser() != null) ? AuthLevel.getAuthLevel(getUser().getAuthlevel()) : Collections.singletonList(AuthLevel.ANONYMOUS);
	}
	/** Checks if the current user has the indicated level
	 * @param authlevel The level to check
	 * @return <b>true</b> if the user has the authlevel, <b>false</b> otherwise
	 */
	public static Boolean hasAuthLevel(AuthLevel authlevel) {
		return (getUser() != null) ? authlevel.hasAuthLevel(getUser().getAuthlevel()) : (authlevel == AuthLevel.ANONYMOUS);
	}
	
	private void loadCredential() {
		begin();
		
		getUserService().getUser(new AsyncCallbackHandler<User>(new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				// TODO max retries
				loadCredential();
				end();
			}
			@Override
			public void onSuccess(User result) {
				credential = result;
				end();
			}
		}));
	}
	
	private void loadConfig(Callback<Void, Exception> callback) {
		FromUrl config = ScriptInjector.fromUrl("config.jsp").setWindow(ScriptInjector.TOP_WINDOW);
		config.setCallback(callback);
		config.inject();
	}
	
	private void begin() {
		loadCount++;
	}
	private void end() {
		loadCount--;
		if (!(loadCount > 0)) finished();
	}
	
	private void preload() {
		ThemeManager.setCurrentTheme(Location.getParameter(ThemeManager.THEME_PARAMETER));
		ThemeManager.addStylesheet("style.css");
	}
	
	private void finished() {
		
		// TODO Put things that need to be on every page here (or after onEnvironmentLoad).
		
		RootPanel mainMenuRoot = RootPanel.get("mainMenu");
		if (mainMenuRoot != null) {
			if (mainMenu == null) mainMenu = new MainMenu();
			mainMenuRoot.add(mainMenu);
		}
		
		//RootPanel searchBoxRoot = RootPanel.get("searchBox");
		//if (searchBoxRoot != null) searchBoxRoot.add(new SearchBox());
		
		RootPanel signoutRoot = RootPanel.get("signOut");
		if (signoutRoot != null) signoutRoot.add(new SignOut());
		
		/*if ( (getCredential() != null) ? (getCredential() instanceof User) : false ) {
			User u = (User) getCredential();
			if (u.getPasschange()) {
				final Modal db = new Modal();
				db.setClosable(false);
				db.setDataBackdrop(ModalBackdrop.STATIC);
				final PasswordManager pm = new PasswordManager(PasswordManager.Type.CHANGE);
				pm.addCompletedHandler(new CompletedHandler() {
					@Override
					public void onCompleted(CompletedEvent event) {
						db.hide();
					}
				});
				ModalBody body = new ModalBody();
				body.add(pm);
				db.add(body);
				db.show();
			}
		}*/
		
		onEnvironmentLoad();
		
	}
	
}
