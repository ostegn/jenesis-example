package com.ostegn.jenesisex.client.base;

/**
 * Css classes that can be used in development, linked to the main CSS.<br>
 * Please add new StyleNames here.
 * @author Thiago Ricciardi
 *
 */
public final class CssStyleName {
	
	/** Full sized, 100% on width and height */
	public static final String SIZE_FULL = "full";
	/** Automatically sized, "auto" style on width and height */
	public static final String SIZE_AUTO = "sizeAuto";
	
	/** Padded layout */
	public static final String LAYOUT_PADDED = "padded";
	/** When you do not want a component to be padded */
	public static final String LAYOUT_NOTPADDED = "notPadded";
	/** When you do not want the component to exceed the boundaries of width and/or height imposed.<br>This solution simply does not show the exceeded parts. */
	public static final String LAYOUT_NOOVERFLOW_HIDE = "noOverflowHide";
	/** When you do not want the component to exceed the boundaries of width and/or height imposed.<br>This solution creates a scrolling for the component. */
	public static final String LAYOUT_NOOVERFLOW_SCROLL = "noOverflowScroll";
	/** When you do not want the component to exceed the boundaries of width and/or height imposed.<br>This solution creates a scrolling for the component only if it has exceeded the boundaries. */
	public static final String LAYOUT_NOOVERFLOW_AUTO = "noOverflowAuto";
	/** To add borders */
	public static final String LAYOUT_BORDER = "border";
	/** When you want the component to be translucent */
	public static final String LAYOUT_TRANSLUCENT = "translucent";
	
	/** Used when a faded text is wanted */
	public static final String FONT_MUTED = "muted";
	
	/** Add this style to error Labels and texts descriptions */
	public static final String ERROR_TEXT = "text-danger";
	/** Add this style to Boxes with error */
	public static final String ERROR_BOX = "form-group has-error";
	
	/** Shows the hand cursor to inform the user that something is clickable.<br>Add this style to links and places where the user can click.<br>To get the exact match of a hiperlink (a href) use {@code LINK}. */
	public static final String CURSOR_HAND = "handCursor";
	/** Shows the busy cursor to inform the user that something is being processed.<br>Add this style to the entire page on long processes. */
	public static final String CURSOR_BUSY = "busyCursor";
	
}
