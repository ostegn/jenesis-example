package com.ostegn.jenesisex.client.base;

import java.io.Serializable;

import com.google.gwt.user.client.Window.Location;
import com.ostegn.jenesis.client.ui.base.StylesheetManager;
import com.ostegn.jenesis.shared.base.TagReplacer;
import com.ostegn.jenesisex.shared.base.IHasDescription;

public class ThemeManager extends StylesheetManager {

	public final static String THEME_PARAMETER = "theme";

	private final static String THEME_HREF = "theme/bootstrap.${theme}.min.css";
	private final static TagReplacer REPLACER = new TagReplacer();

	public enum Theme implements IHasDescription {

		DEFAULT("default", "Default"),
		CERULEAN("cerulean", "Cerulean"),
		COSMO("cosmo", "Cosmo"),
		CYBORG("cyborg", "Cyborg"),
		DARKLY("darkly", "Darkly"),
		FLATLY("flatly", "Flatly"),
		JOURNAL("journal", "Journal"),
		LUMEN("lumen", "Lumen"),
		PAPER("paper", "Paper"),
		READABLE("readable", "Readable"),
		SANDSTONE("sandstone", "Sandstone"),
		SIMPLEX("simplex", "Simplex"),
		SLATE("slate", "Slate"),
		SPACELAB("spacelab", "Spacelab"),
		SUPERHERO("superhero", "Superhero"),
		UNITED("united", "United"),
		YETI("yeti", "Yeti")
		;

		private final String id;
		private final String descr;

		Theme(String id, String descr) {
			this.id = id;
			this.descr = descr;
		}

		/** Gets the ID of the Theme */
		public String getId() {
			return id;
		}

		/** Gets the Theme Description */
		@Override
		public String getDescr() {
			return descr;
		}

		/** Gets the theme Style-sheet HREF */
		public String getStylesheetHref() {
			return (getId() != null) ? REPLACER.replace("theme", getId(), THEME_HREF) : null;
		}

		@Override
		public void doBeforeLoad() {}

		@Override
		public void doBeforeSave() {}

		@Override
		public void doAfterLoad() {}

		@Override
		public void doAfterSave() {}

		@Override
		public Serializable getPrimaryKey() {
			return getId();
		}

		@Override
		public void setDescr(String descr) {}

	}

	private static Theme currentTheme;

	static {
		setCurrentTheme(Location.getParameter(THEME_PARAMETER));
	}

	private static Theme getTheme(String id) {
		Theme ret = Theme.DEFAULT;
		if ( (id != null) ? id.length() > 0 : false) {
			for (Theme theme : Theme.values()) {
				if (id.equals(theme.getId())) {
					ret = theme;
					break;
				}
			}
		}
		return ret;
	}

	public static Theme setCurrentTheme(String id) {
		Theme ret = getTheme(id);
		setCurrentTheme(ret);
		return ret;
	}

	public static void setCurrentTheme(Theme theme) {
		currentTheme = (theme != null) ? theme : Theme.DEFAULT;
		setThemeStylesheet(currentTheme.getStylesheetHref());
	}

	public static Theme getCurrentTheme() {
		return currentTheme;
	}

}
