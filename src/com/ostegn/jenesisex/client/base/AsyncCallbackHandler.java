package com.ostegn.jenesisex.client.base;

import com.google.gwt.user.client.rpc.AsyncCallback;

public class AsyncCallbackHandler<T> implements AsyncCallback<T> {
	
	private AsyncCallback<T> callback;
	
	public AsyncCallbackHandler(AsyncCallback<T> callback) {
		this.callback = callback;
	}

	@Override
	public void onFailure(Throwable caught) {
		// TODO Auto-generated method stub
		callback.onFailure(caught);
	}

	@Override
	public void onSuccess(T result) {
		// TODO Auto-generated method stub
		callback.onSuccess(result);
	}

}
