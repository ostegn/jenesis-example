package com.ostegn.jenesisex.client.base;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.user.client.rpc.RpcRequestBuilder;

/**
 * 
 * Jenesis RPC Request Builder
 * <br>
 * Used to add custom headers to the RPC Services
 * 
 * @author Thiago Ricciardi
 *
 */
public class JenesisRpcRequestBuilder extends RpcRequestBuilder {
	
	@Override
	protected RequestBuilder doCreate(String serviceEntryPoint) {
		RequestBuilder ret = super.doCreate(serviceEntryPoint);
		ret.setHeader(Config.getCSRFHeaderName(), Config.getCSRFToken());
		return ret;
	}

}
