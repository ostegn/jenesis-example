package com.ostegn.jenesisex.client.base;

import com.ostegn.jenesis.client.base.EditableConverter;
import com.ostegn.jenesis.client.ui.base.ObjectListBox;
import com.ostegn.jenesisex.client.ui.base.DescriptionLabel;
import com.ostegn.jenesisex.shared.base.IHasDescription;


public final class EditableConverters {
	
	/** This converter is used to all description persistent objects that has a limited list of options like State and Country selection for instance. */
	public static final EditableConverter<DescriptionLabel, ObjectListBox<IHasDescription>, IHasDescription> DESCRIPTION_LIST_CONVERTER = new EditableConverter<DescriptionLabel, ObjectListBox<IHasDescription>, IHasDescription>() {
		@Override
		public IHasDescription getEditValue(ObjectListBox<IHasDescription> editWidget) {
			return editWidget.getSelectedObject();
		}
		@Override
		public void setEditValue(ObjectListBox<IHasDescription> editWidget, IHasDescription value) {
			editWidget.setSelectedObject(value);
		}
		@Override
		public IHasDescription getDisplayValue(DescriptionLabel displayWidget) {
			return displayWidget.getPersistentObject();
		}
		@Override
		public void setDisplayValue(DescriptionLabel displayWidget, IHasDescription value) {
			displayWidget.setPersistentObject(value);
		}
	};

}
