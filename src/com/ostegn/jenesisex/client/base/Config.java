package com.ostegn.jenesisex.client.base;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

/**
 * The config.js wrapper.
 * 
 * @author Thiago Ricciardi
 *
 */
@JsType(isNative=true, namespace=JsPackage.GLOBAL, name="jenesisConfig")
public class Config {

	/**
	 * Gets the site base url.
	 * @return An url.
	 */
	public static native String getBaseURL();
	
	/**
	 * Gets the home page name.
	 * @return An url.
	 */
	public static native String getHomePage();
	
	/**
	 * Gets the login page name.
	 * @return An url.
	 */
	public static native String getLoginPage();
	
	/**
	 * Gets the image base url.
	 * @return An url.
	 */
	public static native String getImageBaseURL();
	
	/**
	 * Gets the date format string according to {@link DateFormat}.
	 * @return The string representing the format.
	 * @see SimpleDateFormat
	 */
	public static native String getDateFormatString();
	
	/**
	 * Gets the time format string according to {@link DateFormat}.
	 * @return The string representing the format.
	 * @see SimpleDateFormat
	 */
	public static native String getTimeFormatString();
	
	/**
	 * Gets the date-time format string according to {@link DateFormat}.
	 * @return The string representing the format.
	 * @see SimpleDateFormat
	 */
	public static native String getDateTimeFormatString();
	
	/**
	 * Gets the JSON internal date-time format string according to {@link DateFormat}.
	 * @return The string representing the format.
	 * @see SimpleDateFormat
	 */
	public static native String getJSONDateTimeFormatString();
	
	/**
	 * Gets the Login Action relative url
	 * @return A relative url.
	 */
	public static native String getLoginAction();
	
	/**
	 * Gets the Logout Action relative url
	 * @return A relative url.
	 */
	public static native String getLogoutAction();
	
	/**
	 * Gets the Login Form's Username Field
	 * @return A field name.
	 */
	public static native String getUsernameField();
	
	/**
	 * Gets the Login Form's Password Field
	 * @return A field name.
	 */
	public static native String getPasswordField();
	
	/**
	 * Gets the Application Name
	 * @return The application name.
	 */
	public static native String getAppName();
	
	/**
	 * Gets the Application Version
	 * @return The application version.
	 */
	public static native String getAppVersion();
	
	/**
	 * Gets the Application Build String
	 * @return The application build string.
	 */
	public static native String getAppBuild();
	
	/**
	 * Gets the Application Revision String
	 * @return The application revision string.
	 */
	public static native String getAppRevision();
	
	/**
	 * Gets the CSRF Header Name
	 * @return The CSRF Header Name.
	 */
	public static native String getCSRFHeaderName();
	
	/**
	 * Gets the CSRF Parameter Name
	 * @return The CSRF Parameter Name.
	 */
	public static native String getCSRFParameterName();
	
	/**
	 * Gets the CSRF Token
	 * @return The CSRF Token.
	 */
	public static native String getCSRFToken();

}
