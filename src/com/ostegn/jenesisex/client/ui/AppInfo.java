package com.ostegn.jenesisex.client.ui;


import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Label;
import com.ostegn.jenesis.client.ui.base.UIModule;

import com.ostegn.jenesisex.client.base.Config;

/**
 * Shows some application information to make it easier to identify the current version and build.
 * @author Thiago Ricciardi
 *
 */
public class AppInfo extends UIModule implements MouseOverHandler, MouseOutHandler {
	
	private static final String APP_INFO = Config.getAppName() + " " + Config.getAppVersion();
	private static final String APP_BUILD = Config.getAppBuild();
	private static final String APP_REV = Config.getAppRevision();
	
	private String label = "";
	private String currentInfo = APP_INFO;
	
	private final Label appInfo = new Label();
	
	/** Create a new instance of AppInfo without a label */
	public AppInfo() {
		this(null);
	}
	
	/**
	 * Create a new instance of AppInfo with a label before the information
	 * @param label the label string
	 */
	public AppInfo(String label) {
		super();
		setLabel(label);
		appInfo.setTitle(APP_REV);
		appInfo.addMouseOverHandler(this);
		appInfo.addMouseOutHandler(this);
		setWidget(appInfo);
	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
		if (event.getSource() == appInfo) {
			showAppBuild();
			
		}
	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
		if (event.getSource() == appInfo) {
			showAppInfo();
		}
	}
	
	/**
	 * Set the label to display before the information
	 * @param label the label string
	 */
	public void setLabel(String label) {
		this.label = (label != null) ? label : "";
		updateLabel();
	}
	/**
	 * Get the label displayed before the information
	 * @return the label string
	 */
	public String getLabel() {
		return label;
	}
	
	private void showAppBuild() {
		currentInfo = APP_BUILD;
		updateLabel();
	}
	private void showAppInfo() {
		currentInfo = APP_INFO;
		updateLabel();
	}
	private void updateLabel() {
		appInfo.setText(label + " " + currentInfo);
	}

}
