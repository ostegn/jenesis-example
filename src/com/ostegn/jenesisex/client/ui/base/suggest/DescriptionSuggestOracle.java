package com.ostegn.jenesisex.client.ui.base.suggest;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.shared.base.IHasDescription;
import com.ostegn.jenesisex.shared.base.SuggestiblePersistentObject;

public class DescriptionSuggestOracle extends SuggestOracle {
	
	private static final IPersistentServiceAsync persistentService = Services.getPersistentService();
	
	private SuggestiblePersistentObject persistentObject;
	
	public DescriptionSuggestOracle(SuggestiblePersistentObject persistentObject) {
		setPersistentObject(persistentObject);
	}
	
	public SuggestiblePersistentObject getPersistentObject() {
		return persistentObject;
	}
	public void setPersistentObject(SuggestiblePersistentObject persistentObject) {
		this.persistentObject = persistentObject;
	}

	@Override
	public void requestSuggestions(final Request request, final Callback callback) {
		if (persistentObject instanceof IHasDescription) {
			((IHasDescription) persistentObject).setDescr(request.getQuery());
		}
		persistentService.getLike(persistentObject, 0, request.getLimit(), new AsyncCallbackHandler<List<IPersistentObject>>(new AsyncCallback<List<IPersistentObject>>() {
			
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				ArrayList<SuggestiblePersistentObject> l = new ArrayList<SuggestiblePersistentObject>();
				if (result != null) {
					for (IPersistentObject p : result) {
						l.add((SuggestiblePersistentObject) p);
					}
				}
				callback.onSuggestionsReady(request, new Response(l));
			}
			
			@Override
			public void onFailure(Throwable caught) {
				callback.onSuggestionsReady(request, new Response());
			}
			
		}));
	}

}
