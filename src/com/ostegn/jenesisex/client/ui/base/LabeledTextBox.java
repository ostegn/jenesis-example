package com.ostegn.jenesisex.client.ui.base;

import org.gwtbootstrap3.client.ui.TextBox;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;

public class LabeledTextBox extends TextBox {
	
	private Boolean isLabelVisible;
	private Boolean isEditing;
	
	private String label;
	private Boolean isPassword = false;
	
	public LabeledTextBox() {
		super();
		init(null, null);
	}
	public LabeledTextBox(String label) {
		super();
		init(label, null);
	}
	public LabeledTextBox(String label, Boolean isPassword) {
		init(label, isPassword);
	}
	
	private void init(String label, Boolean isPassword) {
		if (isPassword != null) this.isPassword = isPassword;
		setLabel(label);
		showLabel();
		isEditing = false;
		addFocusHandler(new FocusHandler() {
			@Override
			public void onFocus(FocusEvent event) {
				if (isLabelVisible) {
					hideLabel();
				}
				isEditing = true;
			}
		});
		addBlurHandler(new BlurHandler() {
			@Override
			public void onBlur(BlurEvent event) {
				isLabelVisible = !(getText().length() > 0);
				if (isLabelVisible) {
					showLabel();
				}
				isEditing = false;
			}
		});
	}
	
	private void showLabel() {
		if (isPassword) getElement().setAttribute("type", "text");
		addStyleDependentName("label");
		addStyleName("muted");
		super.setText(getLabel());
		isLabelVisible = true;
	}
	private void hideLabel() {
		if (isPassword) getElement().setAttribute("type", "password");
		removeStyleDependentName("label");
		removeStyleName("muted");
		super.setText("");
		isLabelVisible = false;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
		setTitle(label);
		getElement().setAttribute("placeholder", (label != null) ? label : "");
	}
	
	@Override
	public void setText(String text) {
		if (isLabelVisible) {
			if (text.length() > 0) {
				hideLabel();
				super.setText(text);
			}
		}
		else if (!(text.length() > 0) && !isEditing) {
			showLabel();
		}
		else {
			super.setText(text);
		}
	}
	@Override
	public String getText() {
		if (isLabelVisible) return "";
		else return super.getText();
	}
	
}