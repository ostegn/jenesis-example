package com.ostegn.jenesisex.client.ui.base;

import java.math.BigInteger;
import java.util.Map;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ButtonGroup;
import org.gwtbootstrap3.client.ui.InputGroup;
import org.gwtbootstrap3.client.ui.InputGroupAddon;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconType;

import com.google.gwt.dom.client.InputElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.XMLParser;
import com.ostegn.jenesis.client.event.CompletedEvent;
import com.ostegn.jenesis.client.event.CompletedHandler;
import com.ostegn.jenesis.client.event.HasCompletedHandler;
import com.ostegn.jenesis.shared.base.SimpleFunctions;
import com.ostegn.jenesisex.client.base.Config;
import com.ostegn.jenesisex.client.base.CssStyleName;
import com.ostegn.jenesisex.client.i18n.IConstants;
import com.ostegn.jenesisex.shared.base.IImageData.ImageSize;
import com.ostegn.jenesisex.shared.base.IImageData.ImageType;

public class ImageUpload extends FormPanel implements ClickHandler, ChangeHandler, SubmitCompleteHandler, HasCompletedHandler {
	
	private static final Map<String, String> loc = IConstants.instance.imageUpload();
	
	private HandlerManager handlerManager = new HandlerManager(this);
	
	private final VerticalPanel formPanel = new VerticalPanel();
	
	private final HorizontalPanel uploadPanel = new HorizontalPanel();
	private final VerticalPanel mainPanel = new VerticalPanel();
	private final VerticalPanel hiddenPanel = new VerticalPanel();
	private final FileUpload imageFile = new FileUpload();
	private final InputGroupAddon imageLabel = new InputGroupAddon();
	private final Button imagefileButton = new Button(loc.get("browse"));
	private final Label sendingLabel = new Label(loc.get("sendingMsg"));
	private final Label errorLabel = new Label("ERROR");
	private final Button submitButton = new Button(loc.get("send"));
	
	private String caption;
	
	private Image previewImage = new Image();
	
	public ImageUpload(ImageSize previewSize, BigInteger idimage) {
		init(previewSize, idimage);
	}
	
	public ImageUpload(ImageSize previewSize) {
		init(previewSize, null);
	}
	
	public ImageUpload() {
		init(null, null);
	}
	
	public void setIdimage(BigInteger idimage) {
		previewImage.setIdimage(idimage);
		previewImage.setVisible(previewImage.getSize() != null && idimage != null);
	}
	public BigInteger getIdimage() {
		return previewImage.getIdimage();
	}
	
	public void setCaption(String caption) {
		this.caption = caption;
	}
	public String getCaption() {
		return caption;
	}
	
	public void hideSubmitButton() {
		submitButton.setVisible(false);
	}

	private void init(ImageSize previewSize, BigInteger idimage) {
		
		previewImage.setSize(previewSize);
		previewImage.setIdimage(idimage);
		previewImage.setVisible((previewSize != null && idimage != null));
		
		imageFile.setName("imageFile");
		imageFile.addChangeHandler(this);
		imageFile.setVisible(false);
		
		imagefileButton.addClickHandler(this);
		
		submitButton.setType(ButtonType.PRIMARY);
		submitButton.setEnabled(false);
		submitButton.addClickHandler(this);
		
		sendingLabel.setVisible(false);
		
		uploadPanel.add(imageFile);
		InputGroup imageIGroup = new InputGroup();
		imageLabel.setIcon(IconType.FILE);
		imageIGroup.add(imageLabel);
		ButtonGroup imageBGroup = new ButtonGroup();
		imageBGroup.add(imagefileButton);
		imageIGroup.add(imageBGroup);
		uploadPanel.add(imageIGroup);
		SimplePanel gap = new SimplePanel();
		//gap.addStyleName(CssStyleName.LAYOUT_PADDED);
		uploadPanel.add(gap);
		uploadPanel.add(submitButton);
		uploadPanel.setCellVerticalAlignment(submitButton, HasVerticalAlignment.ALIGN_MIDDLE);
		uploadPanel.add(sendingLabel);
		
		errorLabel.setVisible(false);
		errorLabel.addStyleName(CssStyleName.ERROR_TEXT);
		
		mainPanel.add(previewImage);
		mainPanel.add(uploadPanel);
		mainPanel.add(errorLabel);
		
		formPanel.add(mainPanel);
		
		hiddenPanel.setVisible(false);
		formPanel.add(hiddenPanel);
		
		this.setWidget(formPanel);
		this.setAction(Config.getBaseURL() + "/images");
		this.setEncoding(ENCODING_MULTIPART);
		this.setMethod(METHOD_POST);

		this.addSubmitCompleteHandler(this);
		
	}
	
	public void clear() {
		previewImage.setIdimage(null);
		previewImage.setVisible((previewImage.getSize() != null && previewImage.getIdimage() != null));
		errorLabel.setVisible(false);
		imageLabel.setText("");
	}
	
	private void doUpload() {
		this.submit();
	}
	
	@Override
	public void submit() {
		if (submitButton.isEnabled()) {
			errorLabel.setVisible(false);
			imageLabel.setVisible(false);
			imagefileButton.setVisible(false);
			submitButton.setEnabled(false);
			submitButton.setVisible(false);
			sendingLabel.setVisible(true);
			
			hiddenPanel.clear();
			if (caption != null) hiddenPanel.add(new Hidden("caption", caption));
			
			super.submit();
		}
	}
	
	@Override
	public void onClick(ClickEvent event) {
		Object o = event.getSource();
		if (o == submitButton) {
			doUpload();
		}
		else if (o == imagefileButton) {
			imageFile.getElement().<InputElement>cast().click();
		}
	}
	
	@Override
	public void onChange(ChangeEvent event) {
		Object o = event.getSource();
		if (o == imageFile) {
			String ext = SimpleFunctions.getFileExtension(imageFile.getFilename());
			Boolean ok = false;
			for (ImageType type : ImageType.values()) {
				if (type.getExtension().equalsIgnoreCase(ext)) {
					ok = true;
					break;
				}
			}
			if (!ok) {
				errorLabel.setText("Imagem incompatível, por favor escolha outra.");
			}
			errorLabel.setVisible(!ok && (imageFile.getFilename().length() > 0));
			submitButton.setEnabled(ok);
			imageLabel.setText(SimpleFunctions.getFile(imageFile.getFilename()));
		}
	}
	
	@Override
	public void onSubmitComplete(SubmitCompleteEvent event) {
		sendingLabel.setVisible(false);
		imageLabel.setVisible(true);
		imagefileButton.setVisible(true);
		submitButton.setVisible(true);
		Object o = event.getSource();
		if (o == this) {
			Element authResult = null;
			try {
				authResult = (Element) XMLParser.parse(event.getResults()).getElementsByTagName("image-upload").item(0);
			}
			catch (Exception e) {}
			if (authResult != null) {
				String result = authResult.getAttribute("result");
				if (result.equalsIgnoreCase("FAILURE")) {
					String type = authResult.getAttribute("type");
					String message = authResult.getAttribute("message");
					if (type.equalsIgnoreCase("TOOBIG")) {
						errorLabel.setText(loc.get("imageTooBig"));
						errorLabel.setVisible(true);
					}
					else if (type.equalsIgnoreCase("NOTSUPPORTED")) {
						errorLabel.setText(loc.get("imageNotSupported"));
						errorLabel.setVisible(true);
					}
					else if (type.equalsIgnoreCase("NOPERMISSION")) {
						errorLabel.setText(loc.get("imageNoPermission"));
						errorLabel.setVisible(true);
					}
					else {
						Window.alert(message); //TODO Melhorar isso
					}
				}
				else if (result.equalsIgnoreCase("SUCCESS")) {
					BigInteger idimage = new BigInteger(authResult.getAttribute("idimage"));
					previewImage.setIdimage(idimage);
					previewImage.setVisible(previewImage.getSize() != null);
					fireEvent(new CompletedEvent(idimage));
				}
			}
			
		}
	}
	
	@Override
    public void fireEvent(GwtEvent<?> event) {
		super.fireEvent(event);
        handlerManager.fireEvent(event);
    }
	
	@Override
	public HandlerRegistration addCompletedHandler(CompletedHandler handler) {
		return handlerManager.addHandler(CompletedEvent.getType(), handler);
	}

}
