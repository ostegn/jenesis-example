package com.ostegn.jenesisex.client.ui.base;

import org.gwtbootstrap3.client.ui.SuggestBox;
import org.gwtbootstrap3.client.ui.base.ValueBoxBase;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.ostegn.jenesis.client.event.CompletedEvent;
import com.ostegn.jenesis.client.event.CompletedHandler;
import com.ostegn.jenesis.client.event.HasCompletedHandler;
import com.ostegn.jenesis.shared.base.HasPersistentObject;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.shared.base.IHasDescription;

public class DescriptionSuggestBox<T extends Suggestion & IHasDescription> extends SuggestBox implements HasPersistentObject, SelectionHandler<Suggestion>, BlurHandler, FocusHandler, HasCompletedHandler, HasChangeHandlers {
	
	private static final IPersistentServiceAsync persistentService = Services.getPersistentService();
	
	private T persistentObject;
	
	private String emptyValue = "";
	
	private HandlerManager handlerManager = new HandlerManager(this);
	
	{
		addSelectionHandler(this);
		getValueBox().addBlurHandler(this);
		getValueBox().addFocusHandler(this);
	}
	
	public DescriptionSuggestBox() {
		super();
	}
	public DescriptionSuggestBox(SuggestOracle oracle, ValueBoxBase<String> box, SuggestionDisplay suggestDisplay) {
		super(oracle, box, suggestDisplay);
	}
	public DescriptionSuggestBox(SuggestOracle oracle, ValueBoxBase<String> box) {
		super(oracle, box);
	}
	public DescriptionSuggestBox(SuggestOracle oracle) {
		super(oracle);
	}
	
	@Override
	public IPersistentObject getPersistentObject() {
		return persistentObject;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void setPersistentObject(final IPersistentObject persistentObject) {
		if (persistentObject != null) {
			if ((persistentObject instanceof Suggestion) &&  (persistentObject instanceof IHasDescription)) {
				if ( (((IHasDescription)persistentObject).getDescr() != null) ) {
					this.persistentObject = (T) persistentObject;
					setText(this.persistentObject.getReplacementString());
					fireEvent(new CompletedEvent(getPersistentObject()));
				}
				else {
					persistentService.get(persistentObject, new AsyncCallbackHandler<IPersistentObject>(new AsyncCallback<IPersistentObject>() {
						
						@Override
						public void onSuccess(IPersistentObject result) {
							T rst = (T) result;
							if ( (rst != null) ? (rst.getDescr() == null) : false ) rst.setDescr(emptyValue);
							setPersistentObject(rst);
						}
						
						@Override
						public void onFailure(Throwable caught) {
							// TODO Max retries
							setPersistentObject(persistentObject);
						}
					}));
				}
			}
		}
		else {
			clear();
		}
	}
	
	public T getSelectedObject() {
		return persistentObject;
	}
	public void setSelectedObject(T object) {
		setPersistentObject(object);
	}
	
	public void clear() {
		persistentObject = null;
		setText(emptyValue);
	}
	
	public String getEmptyValue() {
		return emptyValue;
	}
	public void setEmptyValue(String emptyValue) {
		this.emptyValue = emptyValue;
		if (persistentObject == null) {
			clear();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onSelection(SelectionEvent<Suggestion> event) {
		if (event.getSource() == this) {
			setPersistentObject((T) event.getSelectedItem());
		}
	}
	
	@Override
	public void onBlur(BlurEvent event) {
		if (event.getSource() == getValueBox()) {
			if (getText().isEmpty()) {
				clear();
				fireEvent(new CompletedEvent(getPersistentObject()));
			}
			if (persistentObject != null) {
				setText(persistentObject.getReplacementString());
			}
			else {
				clear();
			}
		}
	}
	
	@Override
	public void onFocus(FocusEvent event) {
		if (event.getSource() == getValueBox()) {
			if ((emptyValue != null) ? (emptyValue.compareTo(getText()) == 0) : ("".compareTo(getText()) == 0)) {
				setText("");
			}
			else {
				getValueBox().setSelectionRange(0, getText().length());
			}
		}
	}
	
	@Override
	public HandlerRegistration addCompletedHandler(CompletedHandler handler) {
		return handlerManager.addHandler(CompletedEvent.getType(), handler);
	}
	
	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return handlerManager.addHandler(ChangeEvent.getType(), handler);
	}
	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		handlerManager.fireEvent(event);
		super.fireEvent(event);
	}
	

}
