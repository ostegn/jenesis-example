package com.ostegn.jenesisex.client.ui.base;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.ostegn.jenesis.client.event.CompletedEvent;
import com.ostegn.jenesis.client.event.CompletedHandler;
import com.ostegn.jenesis.client.event.HasCompletedHandler;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.shared.base.IHasDescription;

public class DescriptionListBox<T extends IHasDescription> extends ObjectListBox<T> implements HasCompletedHandler {
	
	private static final IPersistentServiceAsync persistentService = Services.getPersistentService();
	
	private List<T> list;
	private T filter;
	private List<T> filterList;
	private String label;
	
	private final HandlerManager handlerManager = new HandlerManager(this);
	
	public DescriptionListBox() {}
	public DescriptionListBox(T filter) {
		init(filter, null, null);
	}
	public DescriptionListBox(List<T> filterList) {
		init(null, filterList, null);
	}
	public DescriptionListBox(String label) {
		init(null, null, label);
	}
	public DescriptionListBox(T filter, String label) {
		init(filter, null, label);
	}
	public DescriptionListBox(List<T> filterList, String label) {
		init(null, filterList, label);
	}
	
	@SuppressWarnings("unchecked")
	private void init(final T filter, final List<T> filterList, final String label) {
		
		this.filter = filter;
		this.filterList = filterList;
		this.label = label;
		
		final AsyncCallbackHandler<List<IPersistentObject>> callback = new AsyncCallbackHandler<List<IPersistentObject>>(new AsyncCallback<List<IPersistentObject>>() {
			
			@Override
			public void onSuccess(List<IPersistentObject> result) {
				List<T> rst = new ArrayList<T>();
				for (IPersistentObject r : result) {
					rst.add((T) r);
				}
				populateListBox(rst);
				fireEvent(new CompletedEvent(list));
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Max retries
				init(filter, filterList, label);
			}
			
		});
		
		if (filter != null) {
			persistentService.getList(filter, callback);
		}
		else if (filterList != null) {
			persistentService.getList(new ArrayList<IPersistentObject>(filterList), callback);
		}
		
	}
	
	private void populateListBox(List<T> ts) {
		clear();
		addItem((label != null) ? label : "");
		if (ts != null) {
			List<T> l = new ArrayList<T>(ts);
			Collections.sort(l, IHasDescription.COMPARATOR);
			for (T t : l) {
				addItem(t.getDescr(), t);
			}
		}
		list = ts;
	}
	
	public T getFilter () {
		return filter;
	}
	public void setFilter(T filter) {
		init(filter, null, label);
	}
	
	public List<T> getFilterList() {
		return filterList;
	}
	public void setFilterList(List<T> filterList) {
		init(null, filterList, label);
	}
	
	public List<T> getList() {
		return list;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
		populateListBox(list);
	}
	
	public void reload() {
		init(filter, filterList, label);
	}
	
	@Override
	public void fireEvent(GwtEvent<?> event) {
		handlerManager.fireEvent(event);
		super.fireEvent(event);
	}
	
	@Override
	public HandlerRegistration addCompletedHandler(CompletedHandler handler) {
		return handlerManager.addHandler(CompletedEvent.getType(), handler);
	}
		
}
