package com.ostegn.jenesisex.client.ui.base;

import java.io.Serializable;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.shared.base.IHasDescription;

public class DescriptionCell extends AbstractCell<IHasDescription> {

	private static final IPersistentServiceAsync persistentService = Services.getPersistentService();

	private Context myContext;
	private IHasDescription persistentObject;
	private SafeHtmlBuilder safeHtmlBuilder;

	private String emptyValue = "";

	@Override
	public void render(Context context, IHasDescription value, SafeHtmlBuilder sb) {
		init(context, value, sb);
	}

	private void init(final IHasDescription value) {
		init(myContext, value, safeHtmlBuilder);
	}
	private void init(final Context context, final IHasDescription value, final SafeHtmlBuilder sb) {

		myContext = context;
		persistentObject = value;
		safeHtmlBuilder = sb;

		if (persistentObject.getDescr() == null) {

			persistentService.get(persistentObject, new AsyncCallbackHandler<IPersistentObject>(new AsyncCallback<IPersistentObject>() {

				@SuppressWarnings("serial")
				@Override
				public void onSuccess(IPersistentObject result) {
					IHasDescription rst = (IHasDescription) result;
					if (rst == null) rst = new IHasDescription() {

						@Override
						public Serializable getPrimaryKey() {
							return null;
						}

						@Override
						public void doBeforeSave() {}

						@Override
						public void doBeforeLoad() {}

						@Override
						public void doAfterLoad() {}

						@Override
						public void doAfterSave() {}

						@Override
						public void setDescr(String descr) {}

						@Override
						public String getDescr() {
							return emptyValue;
						}

					};
					if (rst.getDescr() == null) rst.setDescr(emptyValue);
					persistentObject = rst;
					draw();
				}

				@Override
				public void onFailure(Throwable caught) {
					// TODO Max retries
					init(context, persistentObject, sb);
				}
			}));

		}
		else {
			draw();
		}

	}

	private void draw() {
		safeHtmlBuilder.appendEscaped(new Label(persistentObject.getDescr()).getElement().getInnerHTML());
	}

	public IHasDescription getPersistentObject() {
		return persistentObject;
	}
	public void setPersistentObject(IHasDescription persistentObject) {
		init(persistentObject);
	}

	public String getEmptyValue() {
		return emptyValue;
	}
	public void setEmptyValue(String emptyValue) {
		this.emptyValue = emptyValue;
		init(persistentObject);
	}

}
