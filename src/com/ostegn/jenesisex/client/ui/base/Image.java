package com.ostegn.jenesisex.client.ui.base;

import java.math.BigInteger;

import com.google.gwt.safehtml.shared.SafeUri;
import com.ostegn.jenesisex.client.base.Config;
import com.ostegn.jenesisex.client.base.StaticImage;
import com.ostegn.jenesisex.shared.base.IImageData.ImageSize;
import com.ostegn.jenesisex.shared.persistent.SimpleImage;


public class Image extends com.google.gwt.user.client.ui.Image {
	
	private BigInteger idimage;
	private ImageSize size = ImageSize.DEFAULT;
	private StaticImage staticImage;

	public Image(BigInteger idimage, ImageSize size) {
		super(genURL(idimage, size));
		this.idimage = idimage;
		if (size != null) this.size = size;
	}
	
	public Image(SimpleImage simpleImage) {
		super(genURL(simpleImage.getIdimage(), simpleImage.getSize()));
		this.idimage = simpleImage.getIdimage();
		this.size = simpleImage.getSize();
	}
	
	public Image(BigInteger idimage) {
		super(genURL(idimage, null));
		this.idimage = idimage;
	}
	
	public Image(ImageSize size) {
		super();
		this.size = size;
	}
	
	public Image(StaticImage staticImage) {
		super(genURL(staticImage));
		this.setStaticImage(staticImage);
	}
	
	public Image(String url) {
		super(url);
	}
	
	public Image() {
		super();
	}
	
	private static String genURL(BigInteger idimage, ImageSize size) {
		if (size == null) size = ImageSize.DEFAULT;
		return Config.getImageBaseURL() + "/" + size.getId() + "/" + ((idimage != null) ? idimage.toString() : "");
	}
	private static String genURL(StaticImage staticImage) {
		return staticImage.getRelativeSource();
	}
	
	public com.google.gwt.user.client.ui.Image toImage() {
		com.google.gwt.user.client.ui.Image ret = new com.google.gwt.user.client.ui.Image();
		if (idimage != null) ret.setUrl(genURL(idimage, size));
		else if (staticImage != null) ret.setUrl(genURL(staticImage));
		return ret;
	}
	
	public BigInteger getIdimage() {
		return idimage;
	}
	public void setIdimage(BigInteger idimage) {
		this.idimage = idimage;
		setUrl(genURL(idimage, size));
	}
	public ImageSize getSize() {
		return size;
	}
	public void setSize(ImageSize size) {
		this.size = size;
		if (idimage != null) setUrl(genURL(idimage, size));
	}
	public StaticImage getStaticImage() {
		return staticImage;
	}
	public void setStaticImage(StaticImage staticImage) {
		this.staticImage = staticImage;
		setUrl(genURL(staticImage));
	}
	
	public void setUrl(String url) {
		super.setUrl(url);
		sizeFIX();
	}
	public void setUrl(SafeUri url) {
		super.setUrl(url);
		sizeFIX();
	}
	
	private void sizeFIX() {
		getElement().removeAttribute("width");
		getElement().removeAttribute("height");
	}

}
