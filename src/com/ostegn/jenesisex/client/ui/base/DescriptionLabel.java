package com.ostegn.jenesisex.client.ui.base;

import java.io.Serializable;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.ostegn.jenesis.client.event.CompletedEvent;
import com.ostegn.jenesis.client.event.CompletedHandler;
import com.ostegn.jenesis.client.event.HasCompletedHandler;
import com.ostegn.jenesis.shared.base.HasPersistentObject;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.shared.base.IHasDescription;

public class DescriptionLabel extends Label implements HasCompletedHandler, HasPersistentObject {

	private static final IPersistentServiceAsync persistentService = Services.getPersistentService();

	private IHasDescription persistentObject;

	private String emptyValue = "";

	private HandlerManager handlerManager = new HandlerManager(this);

	public DescriptionLabel() {}
	public DescriptionLabel(IHasDescription persistentObject) {
		setPersistentObject(persistentObject);
	}

	private void init(final IHasDescription persistentObject) {

		this.persistentObject = persistentObject;

		if (persistentObject.getDescr() == null) {

			persistentService.get(persistentObject, new AsyncCallbackHandler<IPersistentObject>(new AsyncCallback<IPersistentObject>() {

				@SuppressWarnings("serial")
				@Override
				public void onSuccess(IPersistentObject result) {
					IHasDescription rst = (IHasDescription) result;
					if (rst == null) rst = new IHasDescription() {

						@Override
						public Serializable getPrimaryKey() {
							return null;
						}

						@Override
						public void doBeforeSave() {}

						@Override
						public void doBeforeLoad() {}

						@Override
						public void doAfterLoad() {}

						@Override
						public void doAfterSave() {}

						@Override
						public void setDescr(String descr) {}

						@Override
						public String getDescr() {
							return emptyValue;
						}

					};
					if (rst.getDescr() == null) rst.setDescr(emptyValue);
					setPersistentObject(rst);
					fireEvent(new CompletedEvent(getPersistentObject()));
				}

				@Override
				public void onFailure(Throwable caught) {
					// TODO Max retries
					init(persistentObject);
				}
			}));

		}
		else {
			setText(persistentObject.getDescr());
			fireEvent(new CompletedEvent(getPersistentObject()));
		}

	}

	@Override
	public IHasDescription getPersistentObject() {
		return persistentObject;
	}
	public void setPersistentObject(IHasDescription persistentObject) {
		init(persistentObject);
	}
	@Override
	public void setPersistentObject(IPersistentObject persistentObject) {
		if (persistentObject instanceof IHasDescription) {
			setPersistentObject((IHasDescription) persistentObject);
		}
	}

	public String getEmptyValue() {
		return emptyValue;
	}
	public void setEmptyValue(String emptyValue) {
		this.emptyValue = emptyValue;
		init(persistentObject);
	}

	@Override
    public void fireEvent(GwtEvent<?> event) {
		super.fireEvent(event);
        handlerManager.fireEvent(event);
    }

	@Override
	public HandlerRegistration addCompletedHandler(CompletedHandler handler) {
		return handlerManager.addHandler(CompletedEvent.getType(), handler);
	}

}
