package com.ostegn.jenesisex.client.ui;


import java.util.Map;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.FormGroup;
import org.gwtbootstrap3.client.ui.FormLabel;
import org.gwtbootstrap3.client.ui.InlineRadio;
import org.gwtbootstrap3.client.ui.InputGroup;
import org.gwtbootstrap3.client.ui.InputGroupAddon;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ValidationState;
import org.gwtbootstrap3.extras.datepicker.client.ui.DatePicker;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.ostegn.jenesis.client.ui.base.HeadingLabel;
import com.ostegn.jenesis.client.ui.base.UIModule;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Config;
import com.ostegn.jenesisex.client.base.CssStyleName;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.i18n.IConstants;
import com.ostegn.jenesisex.client.services.IUserServiceAsync;
import com.ostegn.jenesisex.client.ui.base.LabeledTextBox;
import com.ostegn.jenesisex.shared.UserValidation;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;
import com.ostegn.jenesisex.shared.persistent.UserDetails.Gender;

public class SignUpPanel extends UIModule implements ClickHandler, KeyPressHandler, BlurHandler {
	
	private static final Map<String, String> loc = IConstants.instance.signUpPanel();
	
	public static final int TYPE_SIMPLE = 0x01;
	public static final int TYPE_DETAIL = 0x02;
	public static final int TYPE_COMPLETE = 0x03;
	
	private int type = TYPE_COMPLETE;
	
	private final FormPanel mainForm = new FormPanel();
	
	private final VerticalPanel mainPanel = new VerticalPanel();
	private final FlexTable formFTable = new FlexTable();
	
	private final VerticalPanel topPanel = new VerticalPanel();
	private final Label topDescrLabel = new HeadingLabel(loc.get("signUp"), 4);
	
	private final TextBox namefTBox = new LabeledTextBox(loc.get("namef"));
	
	private final TextBox namelTBox = new LabeledTextBox(loc.get("namel"));
	
	private final DatePicker birthdayDBox = new DatePicker();
	
	private final Label genderLabel = new Label(loc.get("gender"));
	private final FlowPanel genderPanel = new FlowPanel();
	private final InlineRadio genderMRButton = new InlineRadio("gender", loc.get("genderMale"));
	private final InlineRadio genderFRButton = new InlineRadio("gender", loc.get("genderFemale"));
	
	private final TextBox emailTBox = new LabeledTextBox(loc.get("email"));
	private final TextBox emailcTBox = new LabeledTextBox(loc.get("confirmEmail"));
	
	private final TextBox usernameTBox = new LabeledTextBox(loc.get("username"));
	
	private final TextBox passwordPTBox = new LabeledTextBox(loc.get("password"), true);
	private final TextBox passwordcPTBox = new LabeledTextBox(loc.get("confirmPass"), true);
	
	//TODO Adicionar componentes do painel de detalhes
	
	private final HorizontalPanel submitPanel = new HorizontalPanel();
	private final Button submitButton = new Button(loc.get("signUp"));
	private final Label errorLabel = new Label();
	
	private final FlowPanel bottomPanel = new FlowPanel();
	
	private final VerticalPanel successPanel = new VerticalPanel();
	private final Label successLabel = new Label(loc.get("signUpSuccess"));
	
	private static final IUserServiceAsync newUserService = Services.getUserService();
	
	public SignUpPanel() {
		build();
	}
	
	public SignUpPanel(int type) {
		this.type = type;
		build();
	}
	
	private void build() {
		
		topPanel.add(topDescrLabel);
		//topPanel.setCellHorizontalAlignment(topDescrLabel, HasHorizontalAlignment.ALIGN_CENTER);
		
		if ( (type & TYPE_SIMPLE) == TYPE_SIMPLE) { // Create/Initialize simple part
			
			genderMRButton.setValue(true);
			
			genderPanel.add(genderLabel);
			genderPanel.add(genderMRButton);
			genderPanel.add(genderFRButton);
			
			formFTable.setWidget(0, 0, newFormGroup(namefTBox));
			formFTable.setWidget(0, 1, newFormGroup(namelTBox));
			formFTable.setWidget(1, 0, newFormGroup(birthdayDBox, IconType.CALENDAR));
			formFTable.getFlexCellFormatter().setColSpan(1, 0, 2);
			formFTable.setWidget(2, 0, newFormGroup(genderPanel));
			formFTable.getFlexCellFormatter().setColSpan(2, 0, 2);
			formFTable.setWidget(3, 0, newFormGroup(emailTBox, IconType.ENVELOPE));
			formFTable.setWidget(3, 1, newFormGroup(emailcTBox, IconType.ENVELOPE));
			formFTable.setWidget(4, 0, newFormGroup(usernameTBox, IconType.USER));
			formFTable.getFlexCellFormatter().setColSpan(4, 0, 2);
			formFTable.setWidget(5, 0, newFormGroup(passwordPTBox, IconType.LOCK));
			formFTable.setWidget(5, 1, newFormGroup(passwordcPTBox, IconType.LOCK));
			
			birthdayDBox.setPlaceholder(loc.get("birthdate"));
			birthdayDBox.setAutoClose(true);
			birthdayDBox.setGWTFormat(Config.getDateFormatString());
			birthdayDBox.reload();
			
		}
		if ( (type & TYPE_DETAIL) == TYPE_DETAIL) { // Create/Initialize detail part
			// TODO Configurar componentes do painel de detalhes
		}
		
		errorLabel.setVisible(false);
		errorLabel.addStyleName(CssStyleName.ERROR_TEXT);
		errorLabel.setWordWrap(true);
		submitPanel.add(errorLabel);
		submitButton.setType(ButtonType.PRIMARY);
		submitPanel.add(submitButton);
		submitPanel.addStyleName(CssStyleName.SIZE_FULL);
		submitPanel.setCellHorizontalAlignment(submitButton, HasHorizontalAlignment.ALIGN_RIGHT);
		
		mainPanel.add(topPanel);
		mainPanel.add(formFTable);
		mainPanel.add(submitPanel);
		mainPanel.add(bottomPanel);
		
		mainForm.add(mainPanel);
		setWidget(mainForm);
		
		successPanel.add(successLabel);
		successPanel.setCellHorizontalAlignment(successLabel, HasHorizontalAlignment.ALIGN_CENTER);
		successPanel.setCellVerticalAlignment(successLabel, HasVerticalAlignment.ALIGN_MIDDLE);
		successPanel.setVisible(false);
		
		//Handlers
		namefTBox.addKeyPressHandler(this);
		namelTBox.addKeyPressHandler(this);
		genderMRButton.addKeyPressHandler(this);
		genderFRButton.addKeyPressHandler(this);
		emailTBox.addKeyPressHandler(this);
		emailcTBox.addKeyPressHandler(this);
		usernameTBox.addKeyPressHandler(this);
		passwordPTBox.addKeyPressHandler(this);
		passwordcPTBox.addKeyPressHandler(this);
		
		submitButton.addClickHandler(this);
		
		emailTBox.addBlurHandler(this);
		usernameTBox.addBlurHandler(this);
		
	}

	@Override
	public void onClick(ClickEvent event) {
		Object src = event.getSource();
		if (src == submitButton) {
			signUp();
		}
		
	}
	
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object src = event.getSource();
		int keyCode = event.getNativeEvent().getKeyCode();
		if (keyCode == KeyCodes.KEY_ENTER) {
			if (src == namefTBox || src == namelTBox || src == genderMRButton || src == genderFRButton ||
				src == emailTBox || src == emailcTBox || src == passwordPTBox || src == passwordcPTBox ||
				src == usernameTBox) {
				signUp();
			}
		}
	}
	
	@Override
	public void onBlur(final BlurEvent event) {
		Object src = event.getSource();
		if (src == usernameTBox) {
			String username = usernameTBox.getText().trim();
			final FormGroup usernameFGroup = getFormGroup(usernameTBox);
			Widget w = usernameFGroup.getWidget(usernameFGroup.getWidgetCount()-1);
			if (w instanceof Label) usernameFGroup.remove(w);
			getFormGroup(usernameTBox).setValidationState(ValidationState.NONE);
			if (!(username.length() < UserValidation.MIN_USERNAME_LENGTH)) {
				newUserService.usernameExists(username, new AsyncCallbackHandler<Boolean>(new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						// TODO max retries
						onBlur(event);
					}
					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							FormLabel label = getFormLabel(usernameFGroup);
							label.setText(loc.get("errExistsUser"));
							usernameFGroup.setValidationState(ValidationState.ERROR);
						}
						else {
							getFormLabel(usernameFGroup).removeFromParent();
							usernameFGroup.setValidationState(ValidationState.SUCCESS);
						}
					}
				}));
			}
		}
		else if (src == emailTBox) {
			String email = emailTBox.getText().trim();
			final FormGroup emailFGroup = getFormGroup(emailTBox);
			Widget w = emailFGroup.getWidget(emailFGroup.getWidgetCount()-1);
			if (w instanceof Label) emailFGroup.remove(w);
			getFormGroup(emailTBox).setValidationState(ValidationState.NONE);
			if (UserValidation.validateEmail(email)) {
				newUserService.emailExists(email, new AsyncCallbackHandler<Boolean>(new AsyncCallback<Boolean>() {
					@Override
					public void onFailure(Throwable caught) {
						// TODO max retries
						onBlur(event);
					}
					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							FormLabel label = getFormLabel(emailFGroup);
							label.setText(loc.get("errExistsEmail"));
							emailFGroup.setValidationState(ValidationState.ERROR);
						}
						else {
							getFormLabel(emailFGroup).removeFromParent();
							emailFGroup.setValidationState(ValidationState.SUCCESS);
						}
					}
				}));
			}
		}
	}
	
	public void signUp() {
		
		removeError();
		
		if (!emailTBox.getText().trim().equals(emailcTBox.getText().trim())) {
			showError(loc.get("errEmailMatch"));
			getFormGroup(emailTBox).setValidationState(ValidationState.ERROR);
			getFormGroup(emailcTBox).setValidationState(ValidationState.ERROR);
			return;
		}
		if (!passwordPTBox.getText().trim().equals(passwordcPTBox.getText().trim())) {
			showError(loc.get("errPassMatch"));
			getFormGroup(passwordPTBox).setValidationState(ValidationState.ERROR);
			getFormGroup(passwordcPTBox).setValidationState(ValidationState.ERROR);
			return;
		}
		
		User user = new User();
		user.setEmail(emailTBox.getText().trim());
		user.setUsername(usernameTBox.getText().trim());
		user.setPassword(passwordPTBox.getText());
		UserDetails user_details = new UserDetails();
		user_details.setGender(genderMRButton.getValue() ? Gender.MALE : Gender.FEMALE);
		user_details.setNamef(namefTBox.getText().trim());
		user_details.setNamel(namelTBox.getText().trim());
		birthdayDBox.reload();
		user_details.setBirthday(birthdayDBox.getValue());
		
		String validation = UserValidation.validate(user, user_details);
		if (validation.length() > 0) {
			showError(validation, true);
			return;
		}
		
		newUserService.signUp(user, user_details, new AsyncCallbackHandler<User>(new AsyncCallback<User>() {
			
			@Override
			public void onSuccess(User result) {
				showError(null);
				successPanel.setVisible(true);
				successPanel.setWidth(Integer.toString(mainPanel.getOffsetWidth()) + "px");
				successPanel.setHeight(Integer.toString(mainPanel.getOffsetHeight()) + "px");
				mainPanel.setVisible(false);
				setWidget(successPanel);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if (caught instanceof JenesisException) {
					JenesisException e = (JenesisException) caught;
					if (e.getType().equalsIgnoreCase("VALIDATION")) {
						showError(e.getMessage(), true);
					}
					else if (e.getType().equalsIgnoreCase("EXISTS")) {
						showError(loc.get("errFields"));
					}
					else {
						showError(e.getMessage());
					}
				}
				else {
					showError(caught.getMessage());
				}
			}
		}));
		
	}
	
	public void showError(String error) {
		showError(error, false);
	}
	public void showError(String error, Boolean isEncoded) {
		if ((error != null) ? error.length() > 0 : false) {
			errorLabel.setVisible(true);
			if (isEncoded) {
				if (error.contains(UserValidation.NAMEF)) getFormGroup(namefTBox).setValidationState(ValidationState.ERROR);
				if (error.contains(UserValidation.NAMEL)) getFormGroup(namelTBox).setValidationState(ValidationState.ERROR);
				if (error.contains(UserValidation.BIRTHDAY)) getFormGroup(birthdayDBox).setValidationState(ValidationState.ERROR);
				if (error.contains(UserValidation.EMAIL)) getFormGroup(emailTBox).setValidationState(ValidationState.ERROR);
				if (error.contains(UserValidation.PASSWORD)) getFormGroup(passwordPTBox).setValidationState(ValidationState.ERROR);
				if (error.contains(UserValidation.USERNAME)) getFormGroup(usernameTBox).setValidationState(ValidationState.ERROR);
				errorLabel.setText(loc.get("errFields"));
				if (error.contains(UserValidation.BIRTHDAY_YOUNG)) {
					getFormGroup(passwordPTBox).setValidationState(ValidationState.ERROR);
					errorLabel.setText(loc.get("minAgeMsg").replace("{0}", UserValidation.MIN_AGE.toString()));
				}
			}
			else {
				errorLabel.setText(error);
			}
		}
		submitButton.setEnabled(true);
	}
	
	public void removeError() {
		getFormGroup(namefTBox).setValidationState(ValidationState.NONE);
		getFormGroup(namelTBox).setValidationState(ValidationState.NONE);
		getFormGroup(birthdayDBox).setValidationState(ValidationState.NONE);
		getFormGroup(emailTBox).setValidationState(ValidationState.NONE);
		getFormLabel(getFormGroup(emailTBox)).removeFromParent();
		getFormGroup(emailcTBox).setValidationState(ValidationState.NONE);
		getFormGroup(usernameTBox).setValidationState(ValidationState.NONE);
		getFormLabel(getFormGroup(usernameTBox)).removeFromParent();
		getFormGroup(passwordPTBox).setValidationState(ValidationState.NONE);
		getFormGroup(passwordcPTBox).setValidationState(ValidationState.NONE);
		
		submitButton.setEnabled(false);
		errorLabel.setVisible(false);
	}
	
	private FormGroup newFormGroup(Widget child) {
		FormGroup ret = new FormGroup();
		ret.add(child);
		return ret;
	}
	private FormGroup newFormGroup(Widget child, IconType icon) {
		FormGroup ret = new FormGroup();
		InputGroup ip = new InputGroup();
		InputGroupAddon ipa = new InputGroupAddon();
		ipa.setIcon(icon);
		ip.add(ipa);
		ip.add(child);
		ret.add(ip);
		return ret;
	}
	
	private FormGroup getFormGroup(Widget child) {
		Widget parent = child.getParent();
		while ( (parent != null) && (!(parent instanceof FormGroup)) ) {
			parent = parent.getParent();
		}
		return (FormGroup) parent;
	}
	private FormLabel getFormLabel(FormGroup parent) {
		FormLabel ret = null;
		for (int i = 0; i < parent.getWidgetCount(); i++) {
			Widget w = parent.getWidget(i);
			if (w instanceof FormLabel) {
				ret = (FormLabel) w;
				break;
			}
		}
		if (ret == null) {
			ret = new FormLabel();
			parent.add(ret);
		}
		return ret;
	}

}
