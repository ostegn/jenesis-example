package com.ostegn.jenesisex.client.ui;

import java.util.Map;

import org.gwtbootstrap3.client.ui.Button;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.ostegn.jenesisex.client.base.WebPages;
import com.ostegn.jenesisex.client.i18n.IConstants;

public class SignOut extends Button implements ClickHandler {
	
	private static final Map<String, String> loc = IConstants.instance.signOut();
	
	public SignOut() {
		super(loc.get("signoutBtn"));
		addClickHandler(this);
	}
	
	@Override
	public void onClick(ClickEvent event) {
		Object o = event.getSource();
		if (o == this) {
			signout();
		}
	}
	
	private void signout() {
		WebPages.logout();
	}
	
}
