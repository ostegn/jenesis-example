package com.ostegn.jenesisex.client.ui;

import java.util.List;
import java.util.Map;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.ButtonGroup;
import org.gwtbootstrap3.client.ui.InputGroup;
import org.gwtbootstrap3.client.ui.InputGroupAddon;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalBody;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconType;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.xml.client.Element;
import com.google.gwt.xml.client.XMLParser;
import com.ostegn.jenesis.client.ui.base.UIModule;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.Config;
import com.ostegn.jenesisex.client.base.CssStyleName;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.base.ThemeManager;
import com.ostegn.jenesisex.client.base.ThemeManager.Theme;
import com.ostegn.jenesisex.client.base.WebPages;
import com.ostegn.jenesisex.client.i18n.IConstants;
import com.ostegn.jenesisex.client.services.IUserServiceAsync;
import com.ostegn.jenesisex.client.ui.PasswordManager.Type;
import com.ostegn.jenesisex.client.ui.base.LabeledTextBox;
import com.ostegn.jenesisex.client.ui.base.ObjectListBox;

public class SignInPanel extends UIModule implements ClickHandler, KeyPressHandler, SubmitCompleteHandler {
	
	private static final Map<String, String> loc = IConstants.instance.signInPanel();
	
	private final FormPanel mainForm = new FormPanel();
	private final VerticalPanel mainPanel = new VerticalPanel();
	private final TextBox userTBox = new LabeledTextBox(loc.get("username"));
	private final TextBox passTBox = new LabeledTextBox(loc.get("password"), true);
	private final Button signUpButton = new Button(loc.get("signUp"));
	private final Button submitButton = new Button(loc.get("signIn"));
	
	private final CheckBox keepCBox = new CheckBox(loc.get("keepSigned"));
	private final Anchor forgotAnchor = new Anchor();
	private final Label badLoginLabel = new Label(loc.get("badCredentials")); // TODO Melhorar isso, fazer bunitinho
	
	private final Modal forgotModal = new Modal();
	
	private static final IUserServiceAsync userService = Services.getUserService();
	
	public SignInPanel() {
		init();
	}
	
	private void init() {
		
		userTBox.addKeyPressHandler(this);
		userTBox.setName(Config.getUsernameField());
		passTBox.addKeyPressHandler(this);
		passTBox.setName(Config.getPasswordField());
		
		signUpButton.addClickHandler(this);
		
		submitButton.setType(ButtonType.PRIMARY);
		submitButton.addClickHandler(this);
		
		forgotAnchor.setText(loc.get("forgot"));
		forgotAnchor.addClickHandler(this);
		badLoginLabel.addStyleName(CssStyleName.ERROR_TEXT);
		badLoginLabel.setVisible(false);
		
		keepCBox.setVisible(false);
		
		final VerticalPanel vp = new VerticalPanel();
		final InputGroup userIgroup = new InputGroup();
		final InputGroupAddon userIaddon = new InputGroupAddon();
		userIaddon.setIcon(IconType.USER);
		userIgroup.add(userIaddon);
		userIgroup.add(userTBox);
		vp.add(userIgroup);
		final InputGroup passIgroup = new InputGroup();
		final InputGroupAddon passIaddon = new InputGroupAddon();
		passIaddon.setIcon(IconType.LOCK);
		passIgroup.add(passIaddon);
		passIgroup.add(passTBox);
		vp.add(passIgroup);
		final ObjectListBox<Theme> themeSelector = new ObjectListBox<Theme>();
		for (Theme t : Theme.values()) {
			themeSelector.addItem(t.getDescr(), t.getId(), t);
		}
		themeSelector.setSelectedObject(ThemeManager.getCurrentTheme());
		themeSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				ThemeManager.setCurrentTheme(themeSelector.getSelectedObject());
			}
		});
		vp.add(themeSelector);
		vp.add(keepCBox);
		vp.add(badLoginLabel);
		vp.add(forgotAnchor);
		ButtonGroup hp = new ButtonGroup();
		hp.add(signUpButton);
		hp.add(submitButton);
		vp.add(hp);
		vp.setCellHorizontalAlignment(hp, HasHorizontalAlignment.ALIGN_RIGHT);
		vp.add(new Hidden(Config.getCSRFParameterName(), Config.getCSRFToken()));
		mainForm.add(vp);
		
		mainForm.setAction(Config.getLoginAction());
		mainForm.setMethod(FormPanel.METHOD_POST);
		mainForm.addSubmitCompleteHandler(this);
		
		mainPanel.setCellHorizontalAlignment(badLoginLabel, HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.add(mainForm);
		
		forgotModal.setClosable(true);
		forgotModal.hide();

		setWidget(mainPanel);
		
		checkURLParameters();
		
	}
	
	private void setForgotWidget(Widget w) {
		setForgotWidget(w, true);
	}
	private void setForgotWidget(Widget w, Boolean open) {
		ModalBody forgotBody = new ModalBody();
		forgotBody.add(w);
		forgotModal.add(forgotBody);
		HasWidgets parent = (HasWidgets) forgotBody.getParent();
		parent.clear();
		forgotModal.add(forgotBody);
		if (open) forgotModal.show();
	}
	
	private void checkURLParameters() {
		Map<String, List<String>> params = Location.getParameterMap();
		if (!params.isEmpty()) {
			final String hash = (params.get("h") != null) ? ( (params.get("h").size() > 0) ? params.get("h").get(0) : null ) : null;
			final String token = (params.get("t") != null) ? ( (params.get("t").size() > 0) ? params.get("t").get(0) : null ) : null;
			if (hash != null && token != null) {
				userService.checkPassToken(token, hash, new AsyncCallbackHandler<Boolean>(new AsyncCallback<Boolean>() {
					
					@Override
					public void onSuccess(Boolean result) {
						setForgotWidget(new PasswordManager(token, hash));
					}
					
					@Override
					public void onFailure(Throwable caught) {
						// TODO Auto-generated method stub
						
					}
				}));
			}
		}
	}
	
	private void doLogin() {
		if (userTBox.getText().length() > 0 && passTBox.getText().length() > 0) {
			badLoginLabel.setVisible(false);
			submitButton.setEnabled(false);
			mainForm.submit();
		}
		else {
			badLoginLabel.setVisible(true);
		}
	}

	@Override
	public void onClick(ClickEvent event) {
		Object o = event.getSource();
		if (o == submitButton) {
			doLogin();
		}
		else if (o == forgotAnchor) {
			setForgotWidget(new PasswordManager(Type.FORGOT));
		}
		else if (o == signUpButton) {
			setForgotWidget(new SignUpPanel());
			((Button) o).setFocus(false);
		}
	}
	
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object o = event.getSource();
		int keyCode = event.getNativeEvent().getKeyCode();
		if (keyCode == KeyCodes.KEY_ENTER) {
			if (o == userTBox || o == passTBox) {
				doLogin();
			}
		}
	}
	
	@Override
	public void onSubmitComplete(SubmitCompleteEvent event) {
		submitButton.setEnabled(true);
		Object o = event.getSource();
		if (o == mainForm) {
			Element authResult = null;
			try {
				authResult = (Element) XMLParser.parse(event.getResults()).getElementsByTagName("auth-response").item(0);
			}
			catch (Exception e) {}
			if (authResult != null) {
				String result = authResult.getAttribute("result");
				if (result.equalsIgnoreCase("FAILURE")) {
					String message = authResult.getAttribute("message");
					if (message.equalsIgnoreCase("Bad credentials")) {
						badLoginLabel.setVisible(true);
					}
					else {
						Window.alert(message); //TODO Melhorar isso
					}
				}
				else if (result.equalsIgnoreCase("SUCCESS")) {
					String newURL = authResult.getAttribute("redirect");
					if ( (newURL != null) ? newURL.isEmpty() : true) newURL = WebPages.getDefault().getRelativeURL();
					WebPages.goTo(WebPages.addQueryString(newURL, ThemeManager.THEME_PARAMETER, ThemeManager.getCurrentTheme().getId()));
				}
			}
			
		}
	}
	
}
