package com.ostegn.jenesisex.client.ui;


import org.gwtbootstrap3.client.ui.Navbar;
import org.gwtbootstrap3.client.ui.NavbarBrand;
import org.gwtbootstrap3.client.ui.NavbarCollapse;
import org.gwtbootstrap3.client.ui.NavbarCollapseButton;
import org.gwtbootstrap3.client.ui.NavbarForm;
import org.gwtbootstrap3.client.ui.NavbarHeader;
import org.gwtbootstrap3.client.ui.NavbarNav;
import org.gwtbootstrap3.client.ui.NavbarText;
import org.gwtbootstrap3.client.ui.base.AbstractAnchorListItem;
import org.gwtbootstrap3.client.ui.constants.NavbarType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.client.ui.html.Span;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.client.base.JenesisEnvironment;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.base.ThemeManager;
import com.ostegn.jenesisex.client.base.ThemeManager.Theme;
import com.ostegn.jenesisex.client.services.IPersistentServiceAsync;
import com.ostegn.jenesisex.client.ui.base.ObjectListBox;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

public class MainMenu extends Navbar {
	
	private final NavbarNav navbarNav;
	
	private AbstractAnchorListItem activeItem;
	
	private final IPersistentServiceAsync ps = Services.getPersistentService();
	
	public MainMenu() {
		
		final NavbarHeader header = new NavbarHeader();
		final NavbarBrand brand = new NavbarBrand();
		brand.setText("Jenesis");
		header.add(brand);
		final NavbarCollapseButton cb = new NavbarCollapseButton();
		cb.setDataTarget("#navbar-collapse");
		header.add(cb);
		add(header);
		
		final NavbarCollapse coll = new NavbarCollapse();
		coll.setId("navbar-collapse");
		
		navbarNav = new NavbarNav();
		coll.add(navbarNav);
		
		final NavbarForm form = new NavbarForm();
		form.setPull(Pull.RIGHT);
		form.add(new SignOut());
		coll.add(form);
		
		final NavbarText text = new NavbarText();
		text.setPull(Pull.RIGHT);
		final Span span = new Span();
		text.add(span);
		coll.add(text);
		final UserDetails ud = new UserDetails();
		ud.setIduser(JenesisEnvironment.getUser().getIduser());
		ps.get(ud, new AsyncCallback<IPersistentObject>() {
			@Override
			public void onSuccess(IPersistentObject result) {
				span.setText(((UserDetails)result).getNamef());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO max retries
				ps.get(ud, this);
			}
		});
		
		final NavbarForm theme = new NavbarForm();
		theme.setPull(Pull.RIGHT);
		final ObjectListBox<Theme> themeSelector = new ObjectListBox<Theme>();
		for (Theme t : Theme.values()) {
			themeSelector.addItem(t.getDescr(), t.getId(), t);
		}
		themeSelector.setSelectedObject(ThemeManager.getCurrentTheme());
		themeSelector.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				ThemeManager.setCurrentTheme(themeSelector.getSelectedObject());
			}
		});
		theme.add(themeSelector);
		coll.add(theme);
		
		add(coll);
		setType(NavbarType.INVERSE);
		
	}
	
	public void setActive(AbstractAnchorListItem item) {
		if (activeItem != null) activeItem.setActive(false);
		activeItem = item;
		if (activeItem != null) activeItem.setActive(true);
	}
	
	public AbstractAnchorListItem getActive() {
		return activeItem;
	}
	
	public void addItem(AbstractAnchorListItem item) {
		if (item != null) navbarNav.add(item);
	}
	
	public void removeItem (AbstractAnchorListItem item) {
		if (item != null) navbarNav.remove(item);
	}
	
	public void clear() {
		navbarNav.clear();
		activeItem = null;
	}

}
