package com.ostegn.jenesisex.client.ui;


import java.util.Map;

import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.FormGroup;
import org.gwtbootstrap3.client.ui.FormLabel;
import org.gwtbootstrap3.client.ui.InputGroup;
import org.gwtbootstrap3.client.ui.InputGroupAddon;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ValidationState;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Window.Location;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.ostegn.jenesis.client.event.CompletedEvent;
import com.ostegn.jenesis.client.event.CompletedHandler;
import com.ostegn.jenesis.client.event.HasCompletedHandler;
import com.ostegn.jenesis.client.ui.base.HeadingLabel;
import com.ostegn.jenesis.client.ui.base.UIModule;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.base.AsyncCallbackHandler;
import com.ostegn.jenesisex.client.base.CssStyleName;
import com.ostegn.jenesisex.client.base.Services;
import com.ostegn.jenesisex.client.i18n.IConstants;
import com.ostegn.jenesisex.client.services.IUserServiceAsync;
import com.ostegn.jenesisex.client.ui.base.LabeledTextBox;
import com.ostegn.jenesisex.shared.UserValidation;
import com.ostegn.jenesisex.shared.persistent.User;

public class PasswordManager extends UIModule implements ClickHandler, KeyPressHandler, HasCompletedHandler {
	
	private static final Map<String, String> loc = IConstants.instance.passwordManager();
	
	private HandlerManager handlerManager = new HandlerManager(this);
	
	private Type type;
	
	private TextBox emailTBox;
	private TextBox currentTBox;
	private TextBox newTBox;
	private TextBox new_confTBox;
	private Label errorLabel;
	private Button okButton;
	
	private String token;
	private String hash;
	
	private static final IUserServiceAsync userService = Services.getUserService();
	
	public enum Type {
		FORGOT, CHANGE, RESET
	}
	
	public String getToken() {
		return token;
	}
	public String getHash() {
		return hash;
	}
	public void setTokenHash(String token, String hash) {
		this.token = token;
		this.hash = hash;
		init(Type.RESET);
	}
	
	public PasswordManager() {
		init(null);
	}
	
	public PasswordManager(Type type) {
		init(type);
	}
	
	public PasswordManager(String token, String hash) {
		setTokenHash(token, hash);
	}
	
	private void init(Type type) {
		
		if (type == null) {
			verifyType();
			return;
		}
		
		this.type = type;
		
		VerticalPanel mainPanel = new VerticalPanel();
		
		Label infoLabel = new HeadingLabel(5);
		mainPanel.add(infoLabel);
		
		switch (type) {
			case FORGOT:
				infoLabel.setText(loc.get("forgotMsg"));
				break;
	
			case CHANGE:
				infoLabel.setText(loc.get("changeMsg"));
				break;
			
			case RESET:
				infoLabel.setText(loc.get("resetMsg"));
				break;
				
			default:	break;
		}
		
		Grid mainGrid = new Grid(3, 1);
		
		switch (type) {
			
			case FORGOT:
				mainGrid = new Grid(1, 1);
				
				emailTBox = new LabeledTextBox(loc.get("email"));
				emailTBox.addKeyPressHandler(this);
				FormGroup emailPanel = new FormGroup();
				InputGroup emailGroup = new InputGroup();
				InputGroupAddon emailIcon = new InputGroupAddon();
				emailIcon.setIcon(IconType.ENVELOPE);
				emailGroup.add(emailIcon);
				emailGroup.add(emailTBox);
				emailPanel.add(emailGroup);
				mainGrid.setWidget(0, 0, emailPanel);
				break;
			
			case CHANGE:
				currentTBox = new LabeledTextBox(loc.get("oldPass"), true);
				currentTBox.addKeyPressHandler(this);
				FormGroup currentPanel = new FormGroup();
				InputGroup currentGroup = new InputGroup();
				InputGroupAddon currentIcon = new InputGroupAddon();
				currentIcon.setIcon(IconType.LOCK);
				currentGroup.add(currentIcon);
				currentGroup.add(currentTBox);
				currentPanel.add(currentGroup);
				mainGrid.setWidget(0, 0, currentPanel);
			
			case RESET:
				newTBox = new LabeledTextBox(loc.get("newPass"), true);
				newTBox.addKeyPressHandler(this);
				FormGroup newPanel = new FormGroup();
				InputGroup newGroup = new InputGroup();
				InputGroupAddon newIcon = new InputGroupAddon();
				newIcon.setIcon(IconType.LOCK);
				newGroup.add(newIcon);
				newGroup.add(newTBox);
				newPanel.add(newGroup);
				mainGrid.setWidget(1, 0, newPanel);
				
				new_confTBox = new LabeledTextBox(loc.get("confirmPass"), true);
				new_confTBox.addKeyPressHandler(this);
				FormGroup new_confPanel = new FormGroup();
				InputGroup new_confGroup = new InputGroup();
				InputGroupAddon new_confIcon = new InputGroupAddon();
				new_confIcon.setIcon(IconType.LOCK);
				new_confGroup.add(new_confIcon);
				new_confGroup.add(new_confTBox);
				new_confPanel.add(new_confGroup);
				mainGrid.setWidget(2, 0, new_confPanel);
				break;
			
			default:	break;
				
		}
		
		mainPanel.add(mainGrid);
		
		errorLabel = new Label("ERROR");
		errorLabel.addStyleName(CssStyleName.ERROR_TEXT);
		errorLabel.setVisible(false);
		mainPanel.add(errorLabel);
		
		okButton = new Button(loc.get("submitButton"));
		okButton.setType(ButtonType.PRIMARY);
		okButton.addClickHandler(this);
		mainPanel.add(okButton);
		mainPanel.setCellHorizontalAlignment(okButton, HasHorizontalAlignment.ALIGN_RIGHT);
		
		this.setWidget(mainPanel);
		
	}

	private void verifyType() {
		final String hash = Location.getParameter("h");
		final String token = Location.getParameter("t");
		if (hash != null && token != null) {
			userService.checkPassToken(token, hash, new AsyncCallbackHandler<Boolean>(new AsyncCallback<Boolean>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(Boolean result) {
					if (result) setTokenHash(token, hash);
				}
				
			}));
		}
		else {
			userService.getUser(new AsyncCallbackHandler<User>(new AsyncCallback<User>() {

				@Override
				public void onFailure(Throwable caught) {
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onSuccess(User result) {
					if (result != null) {
						init(Type.CHANGE);
					}
					else {
						init(Type.FORGOT);
					}
				}
				
			}));
		}
	}
	
	@Override
	public void onClick(ClickEvent event) {
		Object o = event.getSource();
		if (o == okButton) okClick();
	}
	
	@Override
	public void onKeyPress(KeyPressEvent event) {
		Object o = event.getSource();
		int keyCode = event.getNativeEvent().getKeyCode();
		if (keyCode == KeyCodes.KEY_ENTER) {
			if (o == emailTBox || o == currentTBox || o == newTBox || o == new_confTBox) okClick();
		}
	}
	
	private void okClick() {
		if (!validate()) return;
		okButton.setEnabled(false);
		switch (type) {
			case FORGOT:	forgot();	break;
			case CHANGE:	change();	break;
			case RESET:		reset();	break;
			default:	break;
		}
	}
	
	private Boolean validate() {
		Boolean ret = true;
		
		if (currentTBox != null) removeError(currentTBox);
		if (emailTBox != null) removeError(emailTBox);
		if (newTBox != null) removeError(newTBox);
		if (new_confTBox != null) removeError(new_confTBox);
		errorLabel.setVisible(false);
		
		switch (type) {
			case FORGOT:
				if (!UserValidation.validateEmail(emailTBox.getText().trim())) {
					addError(emailTBox, loc.get("errEmailInv"));
					ret = false;
				}
				break;
			case CHANGE:
				if (!UserValidation.validatePassword(currentTBox.getText())) {
					addError(currentTBox, loc.get("errPassShort"));
					ret = false;
				}
			case RESET:
				if (!UserValidation.validatePassword(newTBox.getText())) {
					addError(newTBox, loc.get("errPassShort"));
					ret = false;
				}
				if (!newTBox.getText().equals(new_confTBox.getText())) {
					addError(newTBox, null);
					addError(new_confTBox, loc.get("errPassMatch"));
					ret = false;
				}
				break;
			default:	break;
		}
		
		return ret;
	}
	
	private void addError(Widget w, String label) {
		FormGroup p = (FormGroup) w.getParent().getParent();
		if ( (label != null) ? (label.length() > 0) : false ) {
			FormLabel error = new FormLabel();
			error.setText(label);
			p.add(error);
			p.setValidationState(ValidationState.ERROR);
		}
	}
	
	private void removeError(Widget w) {
		FormGroup p = (FormGroup) w.getParent().getParent();
		p.setValidationState(ValidationState.NONE);
		Widget error = p.getWidget(p.getWidgetCount()-1);
		if (error instanceof FormLabel) p.remove(error);
	}
	
	private void forgot() {
		if (type == Type.FORGOT) {
			userService.forgotPass(emailTBox.getText().trim(), new AsyncCallbackHandler<Void>(new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					onSuccessEnd(loc.get("mailsentMsg").replace("{0}", emailTBox.getText().trim()));
				}
				
				@Override
				public void onFailure(Throwable caught) {
					onFailureEnd(caught);
				}
			}));
		}
	}
	
	private void change() {
		if (type == Type.CHANGE) {
			userService.changePass(currentTBox.getText(), newTBox.getText(), new AsyncCallbackHandler<User>(new AsyncCallback<User>() {
				
				@Override
				public void onSuccess(User result) {
					onSuccessEnd(loc.get("changedMsg"));
				}
				
				@Override
				public void onFailure(Throwable caught) {
					onFailureEnd(caught);
				}

			}));
		}
	}
	
	private void reset() {
		if (type == Type.RESET) {
			userService.changePass(token, hash, newTBox.getText(), new AsyncCallbackHandler<Void>(new AsyncCallback<Void>() {
				
				@Override
				public void onSuccess(Void result) {
					onSuccessEnd(loc.get("changedMsg"));
				}
				
				@Override
				public void onFailure(Throwable caught) {
					onFailureEnd(caught);
				}
				
			}));
		}
	}
	
	private void onError(JenesisException error) {
		if ("PASSWORD".equalsIgnoreCase(error.getType())) {
			errorLabel.setText(loc.get("errPassBad"));
		}
		else if ("INVALID".equalsIgnoreCase(error.getType())) {
			errorLabel.setText(loc.get("errTokenInv"));
		}
		else if ("EXPIRED".equalsIgnoreCase(error.getType())) {
			errorLabel.setText(loc.get("errTokenExp"));
		}
		else if ("NOT_FOUND".equalsIgnoreCase(error.getType())) {
			errorLabel.setText(loc.get("errEmailNF"));
		}
		else {
			errorLabel.setText(error.getMessage());
		}
		errorLabel.setVisible(true);
		okButton.setEnabled(true);
	}
	
	private void onSuccessEnd(String displayText) {
		Label successLabel = new Label(displayText);
		successLabel.setWordWrap(true);
		VerticalPanel successPanel = new VerticalPanel();
		successPanel.add(successLabel);
		successPanel.addStyleName(CssStyleName.SIZE_FULL);
		successPanel.setCellHorizontalAlignment(successLabel, HasHorizontalAlignment.ALIGN_CENTER);
		successPanel.setCellVerticalAlignment(successLabel, HasVerticalAlignment.ALIGN_MIDDLE);
		this.setWidget(successPanel);
		fireEvent(new CompletedEvent());
	}
	
	private void onFailureEnd(Throwable caught) {
		if (caught instanceof JenesisException) {
			onError((JenesisException) caught);
		}
		else Window.alert(caught.getMessage());
	}
	
	@Override
    public void fireEvent(GwtEvent<?> event) {
		super.fireEvent(event);
        handlerManager.fireEvent(event);
    }
	
	@Override
	public HandlerRegistration addCompletedHandler(CompletedHandler handler) {
		return handlerManager.addHandler(CompletedEvent.getType(), handler);
	}

}
