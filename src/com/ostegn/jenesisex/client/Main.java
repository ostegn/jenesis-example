package com.ostegn.jenesisex.client;


import com.google.gwt.user.client.ui.RootPanel;
import com.ostegn.jenesisex.client.base.JenesisEnvironment;
import com.ostegn.jenesisex.client.ui.AppInfo;
import com.ostegn.jenesisex.client.ui.SignInPanel;

public class Main extends JenesisEnvironment {
	
	@Override
	public Boolean isAuthenticationRequired() {
		return false;
	};
	
	/**
	 * This is the entry point method.
	 */
	@Override
	public void onEnvironmentLoad() {
		
		final SignInPanel signInPanel = new SignInPanel();
		final AppInfo appInfo = new AppInfo();
		
		RootPanel signInRoot = RootPanel.get("signIn");
		signInRoot.add(signInPanel);
		
		RootPanel appInfoRoot = RootPanel.get("appInfo");
		appInfoRoot.add(appInfo);
		
	}
	
}
