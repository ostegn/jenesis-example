/* Generate Java Class */
SELECT concat(id_annotation, type_annotation, 'private ', type, ' ', name, ';') as "Java Class" FROM (
    SELECT
        CASE WHEN lower(column_key) = 'pri' THEN '@Id ' ELSE '' END as id_annotation,
        CASE
            WHEN lower(data_type) like '%int' THEN
            CASE
                WHEN lower(column_type) = 'tinyint(1)' THEN ''
                ELSE concat('@Column(columnDefinition="', lower(data_type), '") ')
            END
            WHEN lower(data_type) = 'char' THEN concat('@Column(columnDefinition="', lower(data_type), '", length=',  character_maximum_length, ') ')
            WHEN lower(column_name) = 'lastedit' THEN '@Version @Column(columnDefinition="datetime") '
            ELSE ''
        END as type_annotation,
        CASE
            WHEN lower(data_type) like '%char' THEN 'String'
            WHEN lower(data_type) like '%int' THEN
            CASE
                WHEN lower(data_type) = 'bigint' THEN 'BigInteger'
				WHEN lower(data_type) = 'integer' THEN 'Long'
                WHEN lower(column_type) = 'tinyint(1)' THEN 'Boolean'
                ELSE 'Integer'
            END
            WHEN lower(data_type) = 'datetime' THEN
            CASE
                WHEN lower(column_name) = 'lastedit' THEN 'Timestamp'
                ELSE 'Date'
            END
            ELSE concat(upper(substring(data_type,1,1)), lower(substring(data_type, 2)))
        END as type,
        lower(column_name) as name
    FROM information_schema.columns WHERE lower(table_schema) = lower('jenesis') AND lower(table_name) = lower('password_token') ORDER BY ordinal_position
) aux;