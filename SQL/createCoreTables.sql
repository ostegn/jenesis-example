/* BoF */

CREATE SCHEMA IF NOT EXISTS `jenesis`;

CREATE  TABLE IF NOT EXISTS `jenesis`.`user` (
  `iduser` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'User ID' ,
  `email` VARCHAR(254) NOT NULL COMMENT 'E-mail' ,
  `username` VARCHAR(30) NOT NULL COMMENT 'Username' ,
  `password` CHAR(64) NOT NULL COMMENT 'Hashed Password' ,
  `created` DATETIME NOT NULL COMMENT 'Date of creation' ,
  `lastedit` DATETIME NOT NULL COMMENT 'Date of the last change' ,
  `logincount` INT UNSIGNED NOT NULL DEFAULT 0 COMMENT 'How many times this user has logged in' ,
  `lastlogin` DATETIME NULL COMMENT 'The last login timestamp' ,
  `banneduntil` DATETIME NULL COMMENT 'This user is banned until what datetime' ,
  `authlevel` SMALLINT UNSIGNED NOT NULL DEFAULT 1 COMMENT 'The user authorization level (0 means anonymous, not to be used)' ,
  PRIMARY KEY (`iduser`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) ,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) )
ENGINE = InnoDB
COMMENT = 'User access table';

CREATE  TABLE IF NOT EXISTS `jenesis`.`user_details` (
  `iduser` BIGINT UNSIGNED NOT NULL COMMENT 'User ID' ,
  `namef` VARCHAR(50) NOT NULL COMMENT 'First Name' ,
  `namel` VARCHAR(50) NOT NULL COMMENT 'Last Name' ,
  `birthday` DATETIME NULL COMMENT 'Birthday' ,
  `gender` TINYINT NOT NULL COMMENT 'Gender - 0 male, 1 female - Hofstadter Female and Male sequences' ,
  `lastedit` DATETIME NOT NULL COMMENT 'Date of the last change' ,
  `idimage` DECIMAL(30) UNSIGNED NULL ,
  PRIMARY KEY (`iduser`) ,
  INDEX `fk_userdetails_user` (`iduser` ASC) ,
  CONSTRAINT `fk_userdetails_user`
    FOREIGN KEY (`iduser` )
    REFERENCES `jenesis`.`user` (`iduser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'User complementary table';

CREATE  TABLE IF NOT EXISTS `jenesis`.`password_token` (
  `iduser` BIGINT UNSIGNED NOT NULL COMMENT 'User ID' ,
  `token` CHAR(64) NOT NULL COMMENT 'A token to be used to reset the user password' ,
  `lastedit` DATETIME NOT NULL COMMENT 'Date of the last change\nUsed to expire the token' ,
  PRIMARY KEY (`iduser`) ,
  INDEX `fk_passwordtoken_user` (`iduser` ASC) ,
  CONSTRAINT `fk_passwordtoken_user`
    FOREIGN KEY (`iduser` )
    REFERENCES `jenesis`.`user` (`iduser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Table that controls the password recovery requests';

CREATE  TABLE IF NOT EXISTS `jenesis`.`user_log` (
  `iduser` BIGINT UNSIGNED NOT NULL COMMENT 'User ID' ,
  `iduser_log` BIGINT UNSIGNED NOT NULL COMMENT 'The Log ID' ,
  `timestamp` DATETIME NOT NULL COMMENT 'Timestamp of the logging' ,
  `data` VARCHAR(255) NOT NULL COMMENT 'Log data (or description)' ,
  PRIMARY KEY (`iduser`, `iduser_log`) ,
  INDEX `fk_userlog_user` (`iduser` ASC) ,
  CONSTRAINT `fk_userlog_user`
    FOREIGN KEY (`iduser` )
    REFERENCES `jenesis`.`user` (`iduser` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'User logging table';

/* EoF */