package com.ostegn.jenesisex.junit.server;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesisex.junit.server.base.ServerContextTest;
import com.ostegn.jenesisex.server.task.ReporterTask;

public class ServletTaskTest extends ServerContextTest {
	
	@Autowired private ReporterTask rt;
	
	@Test
	public void reporterTaskTest() {
		rt.getTask().run();
	}

}
