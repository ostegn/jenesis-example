package com.ostegn.jenesisex.junit.server;

import java.io.IOException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesis.junit.base.ITestCredential;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.services.IImageService;
import com.ostegn.jenesisex.junit.server.base.ServerContextTest;
import com.ostegn.jenesisex.junit.server.base.TestCredentials;

public class ImageServiceTest extends ServerContextTest {
	
	@Autowired private IImageService is;
	
	@Override
	public ITestCredential getTestCredential() {
		return TestCredentials.TestCredential.USER;
	}
	
	@Test
	public void testRebuildImageSizes() throws JenesisException, IOException {
		is.rebuildImageSizes();
	}

}
