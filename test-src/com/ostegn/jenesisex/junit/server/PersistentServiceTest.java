package com.ostegn.jenesisex.junit.server;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesis.junit.base.ITestCredential;
import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.services.IPersistentService;
import com.ostegn.jenesisex.junit.server.base.ServerContextTest;
import com.ostegn.jenesisex.junit.server.base.TestCredentials;
import com.ostegn.jenesisex.junit.server.base.TestCredentials.TestCredential;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;

public class PersistentServiceTest extends ServerContextTest {
	
	@Autowired private IPersistentService ps;
	
	@Override
	public ITestCredential getTestCredential() {
		return TestCredentials.TestCredential.USER;
	}
	
	@Test
	public void testUserAutofill() throws JenesisException {
		User u = ((TestCredential) getTestCredential()).getUser();
		UserDetails ud = new UserDetails();
		ud = (UserDetails) ps.get(ud);
		Assert.assertTrue("Problem autofilling User ID.", ud.getIduser().equals(u.getIduser()));
	}

}
