package com.ostegn.jenesisex.junit.server.base;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.ServerAddress;
import com.ostegn.jenesisex.server.base.WebConfig;

import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;


@Configuration
public class WebConfigTest extends WebConfig {
	
	protected Properties getHibernateProperties() {
		if (hibernateProperties == null) {
			hibernateProperties = super.getHibernateProperties();
			hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
			hibernateProperties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		}
		return hibernateProperties;
	}
	private Properties hibernateProperties;
	
	@Bean
	@Override
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl("jdbc:h2:mem:test;MODE=MYSQL;DB_CLOSE_DELAY=-1");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		dataSource.setDriverClassName("org.h2.Driver");
		return dataSource;
	}
	
	@Bean(destroyMethod="shutdown")
	public MongoServer mongoServer() {
		return new MongoServer(new MemoryBackend());
	}
	
	@Bean
	@Override
	public MongoDbFactory mongoDbFactory() {
		MongoDbFactory mongoDbFactory = null;
		MongoServer mongoServer = mongoServer();
		if (mongoServer != null) {
			try {
				mongoDbFactory = new SimpleMongoDbFactory(new MongoClient(new ServerAddress(mongoServer.bind())), "test");
			}
			catch (MongoException ex) {
				if (logger.isErrorEnabled()) {
					logger.error(String.format("%1$s thrown while building MongoDbFactory: %2$s", ex.getClass(), ex.getLocalizedMessage()), ex);
				}
			}
		}
		return mongoDbFactory;
	}
	
	@Bean(initMethod="startup", destroyMethod="shutdown")
	@Lazy(false)
	public TestCredentials testCredentials() {
		return new TestCredentials();
	}
	
}
