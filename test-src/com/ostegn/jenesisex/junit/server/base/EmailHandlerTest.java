package com.ostegn.jenesisex.junit.server.base;

import java.io.IOException;

import javax.mail.MessagingException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesisex.server.base.EmailHandler;

public class EmailHandlerTest extends ServerContextTest {
	
	@Autowired private EmailHandler eh;
	
	@Test
	public void testSend() throws IOException, MessagingException {
		eh.send("tester@jenesis.ostegn.com", "Hello World!", "Hi World, \n\nWe are testing the e-mail sender.\n\nYours,\nEmailHandlerTest");
	}

}
