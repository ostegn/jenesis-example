package com.ostegn.jenesisex.junit.server.base;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.test.context.ContextConfiguration;

import com.ostegn.jenesis.junit.base.ITestCredential;
import com.ostegn.jenesis.junit.base.ServerEnvironmentTest;
import com.ostegn.jenesis.junit.base.TestCredential;

@ContextConfiguration(classes={WebConfigTest.class})
public abstract class ServerContextTest extends ServerEnvironmentTest {

	@Override
	public Collection<? extends GrantedAuthority> getAnonymousRoles() {
		return Arrays.asList(new GrantedAuthority() {
			private static final long serialVersionUID = 1L;
			@Override
			public String getAuthority() {
				return "ROLE_ANONYMOUS";
			}
		});
	}
	
	@Override
	public ITestCredential getTestCredential() {
		return new TestCredential(); // Anonymous
	}

}
