package com.ostegn.jenesisex.junit.server.base;

import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ostegn.jenesis.junit.base.ITestCredential;
import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.shared.base.AuthLevel;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserDetails;
import com.ostegn.jenesisex.shared.persistent.UserDetails.Gender;

public class TestCredentials {
	
	protected transient Log logger = LogFactory.getLog(getClass());
	
	@Autowired protected PersistentOperations po;
	@Autowired protected PasswordEncoder passwordEncoder;
	
	public enum TestCredential implements ITestCredential {
		
		//SUPER_ADMIN("sa", "sa", "sa@jenesis.ostegn.com", AuthLevel.SUPER_ADMIN.getId()),
		ADMIN("admin", "admin", "admin@jenesis.ostegn.com", AuthLevel.ADMIN.getId()),
		SUPER_MANAGER("sm", "sm", "sm@jenesis.ostegn.com", AuthLevel.SUPER_MANAGER.getId()),
		MANAGER("man", "man", "man@jenesis.ostegn.com", AuthLevel.MANAGER.getId()),
		SUPER_EDITOR("se", "se", "se@jenesis.ostegn.com", AuthLevel.SUPER_EDITOR.getId()),
		EDITOR("editor", "editor", "editor@jenesis.ostegn.com", AuthLevel.EDITOR.getId()),
		SUPER_USER("su", "su", "su@jenesis.ostegn.com", AuthLevel.SUPER_USER.getId()),
		USER("user", "user", "user@jenesis.ostegn.com", AuthLevel.USER.getId())
		;
		
		private final User user;
		private final String password;
		
		TestCredential(String username, String password, String email, Integer authlevel) {
			this.password = password;
			user = new User();
			user.setUsername(username);
			user.setPassword(password);
			user.setEmail(email);
			user.setAuthlevel(authlevel);
		}
		
		public User getUser() {
			return user;
		}

		@Override
		public String getUsername() {
			return user.getUsername();
		}

		@Override
		public String getPassword() {
			return password;
		}
		
	}
	
	public void startup() {
		if (logger.isDebugEnabled()) logger.debug("Creating TestCredentials...");
		int i = 0;
		for (TestCredential tu : TestCredential.values()) {
			User u = tu.getUser();
			u.setPassword(passwordEncoder.encode(tu.getPassword()));
			u.setCreated(new Date());
			u.setLogincount(0L);
			u = po.save(u);
			tu.getUser().setIduser(u.getIduser());
			UserDetails ud = new UserDetails();
			ud.setIduser(u.getIduser());
			ud.setNamef(u.getUsername() + " FirstName");
			ud.setNamel(u.getUsername() + " LastName");
			ud.setGender( (i % 2 == 0) ? Gender.MALE : Gender.FEMALE );
			ud = po.save(ud);
			i++;
		}
	}
	
	public void shutdown() {
		if (logger.isDebugEnabled()) logger.debug("Exiting TestCredentials...");
	}

}
