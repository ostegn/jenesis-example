package com.ostegn.jenesisex.junit.server.base;

import java.io.IOException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesisex.server.base.Config;

public class ConfigTest extends ServerContextTest {
	
	@Autowired private Config config;
	
	@Test
	public void testInit() throws IOException {
		config.init();
	}
	
}
