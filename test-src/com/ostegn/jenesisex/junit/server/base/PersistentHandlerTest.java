package com.ostegn.jenesisex.junit.server.base;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesis.junit.base.ITestCredential;
import com.ostegn.jenesis.server.base.PersistentOperations;
import com.ostegn.jenesis.shared.base.IPersistentObject;
import com.ostegn.jenesisex.junit.server.base.TestCredentials.TestCredential;
import com.ostegn.jenesisex.shared.persistent.User;
import com.ostegn.jenesisex.shared.persistent.UserLog;

public class PersistentHandlerTest extends ServerContextTest {
	
	@Autowired private PersistentOperations po;
	
	@Override
	public ITestCredential getTestCredential() {
		return TestCredential.USER;
	}
	
	@Test
	public void testCountAll() {
		Class<? extends IPersistentObject> c = User.class;
		Long count = po.countAll(c);
		Assert.assertTrue("Problem counting all records", count == TestCredential.values().length);
	}
	
	@Test
	public void testGenerateColumn() {
		User u = ((TestCredential) getTestCredential()).getUser();
		for (int i = 0; i < 100; i++) {
			UserLog log = new UserLog();
			log.setIduser(u.getIduser());
			log.setTimestamp(new Date());
			log.setData(String.format("Test data %1$s", i+1));
			log = po.save(log);
			Assert.assertTrue(String.format("Problem generating column value %1$s", i), log.getIduser_log() != null);
		}
	}
	
}
