package com.ostegn.jenesisex.junit.server;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.ostegn.jenesis.shared.base.JenesisException;
import com.ostegn.jenesisex.client.services.IUserService;
import com.ostegn.jenesisex.junit.server.base.ServerContextTest;
import com.ostegn.jenesisex.junit.server.base.TestCredentials;

public class UserServiceTest extends ServerContextTest {
	
	@Autowired private IUserService us;
	
	@Test
	public void testforgotPass() throws JenesisException {
		
		String email = TestCredentials.TestCredential.USER.getUser().getEmail();
		us.forgotPass(email);
		
	}
	
}
