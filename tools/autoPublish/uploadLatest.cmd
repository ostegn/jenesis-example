@echo off
echo %DATE% %TIME%
echo.
echo Configuring environment...
setlocal ENABLEDELAYEDEXPANSION
set MY_HOME=%CD%
set JDK=C:\Program Files\Java\jdk1.7.0_07
set JAVA_HOME=%JDK%
set ECLIPSE_HOME=%HOMEDRIVE%%HOMEPATH%\develop\eclipse\indigo
set WORKSPACE=%CD%\..\workspace
set PROJ_NAME=Jenesis_Web
set SVN_HOME=C:\Program Files\SlikSvn\bin
set PUTTY_HOME=C:\Program Files (x86)\PuTTY
set SEVENZIP_HOME=C:\Program Files\7-Zip
set SERVER=ubuntu@jenesis.ostegn.com
set DEPLOY_PATH=/var/lib/tomcat7/webapps
set DEPLOY_FILE=ROOT.war
set TMP_FILE=%date:~-4%-%date:~-7,2%-%date:~-10,2%.war
set PATH=%PATH%;%JDK%\bin\
set PATH=%PATH%;%ECLIPSE_HOME%\
set PATH=%PATH%;%SVN_HOME%\
set PATH=%PATH%;%PUTTY_HOME%\
set PATH=%PATH%;%SEVENZIP_HOME%\
set PATH=%PATH%;%CD%\
set CLASSPATH=;
REM set CLASSPATH=%CLASSPATH%;%ECLIPSE_HOME%\plugins\*
for /F %%l in ('dir /a:D /s /b %eclipse_home%\plugins\com.google.gwt.eclipse.sdkbundle*') do set GWT_HOME=%%l
for /F %%l in ('dir /a:D /s /b %GWT_HOME%\gwt-*') do set GWT_HOME=%%l
set CLASSPATH=%CLASSPATH%;%GWT_HOME%\*
set PATH=%PATH%;%GWT_HOME%\
set PATHEXT=%PATHEXT%;.JAR
echo.
echo Environment Variables:
set
echo.
echo This script will checkout, compile, build war and update the server, if you do not want to continue close the window now...
echo.
pause
cd %WORKSPACE%
rd /s /q %PROJ_NAME%_tmp
echo.
echo Checking with SVN...
svn co http:///svn/jenesis/branches/public/ %PROJ_NAME%_tmp --username builder --password <password>
echo.
echo Replacing the old project with the new one...
rd /s /q %PROJ_NAME%
ren %PROJ_NAME%_tmp %PROJ_NAME%
cd %PROJ_NAME%
set CLASSPATH=%CLASSPATH%;%CD%\src;%CD%\war\WEB-INF\lib\*;%CD%\war\WEB-INF\classes\
echo.
echo Compiling Workspace...
echo ^<?xml version="1.0"?^> > refresh.xml
echo ^<project default="refresh"^> >> refresh.xml
echo ^<target name="refresh"^> >> refresh.xml
echo ^<eclipse.refreshLocal resource="%PROJ_NAME%" depth="infinite"/^> >> refresh.xml
echo ^</target^> >> refresh.xml
echo ^</project^> >> refresh.xml
eclipsec -nosplash -data %WORKSPACE% -application org.eclipse.ant.core.antRunner -f refresh.xml
REM del /q refresh.xml
eclipsec -nosplash -data %WORKSPACE% -application org.eclipse.jdt.apt.core.aptBuild
REM echo Compiling Java...
REM dir /s /b src\*.java > compilelist.txt
REM md war\WEB-INF\classes
REM javac -encoding UTF-8 -d war\WEB-INF\classes @compilelist.txt
REM java -jar %ECLIPSE_HOME%\plugins\org.eclipse.jdt.core_3.7.3.v20120119-1537.jar -classpath %CLASSPATH% -1.6 -encoding UTF-8 -d war/WEB-INF/classes @compilelist.txt
ren war\META-INF\context.unused.xml context.xml
cd war\WEB-INF\classes
ren log4j.properties log4j.backup.properties
ren log4j.public.properties log4j.properties
ren resources\jenesis.properties jenesis.backup.properties
ren resources\jenesis.public.properties jenesis.properties
echo.
echo Compiling GWT...
set GWT_CLASSES= 
for /r . %%f in (*.gwt.xml) do set GWT_CLASSES=!GWT_CLASSES!%%f 
call set GWT_CLASSES=%%GWT_CLASSES:%CD%\=%%
set GWT_CLASSES=%GWT_CLASSES:.gwt.xml=%
set GWT_CLASSES=%GWT_CLASSES:\=.%
set GWT_CLASSES=%GWT_CLASSES:jenesis.web.Testing=%
cd %WORKSPACE%\%PROJ_NAME%
java com.google.gwt.dev.Compiler -optimize 9 -localWorkers 2 %GWT_CLASSES%
echo.
echo Packing WAR...
cd war
del /q Testing.html
del /q %MY_HOME%\temp\%TMP_FILE%
7z a -mx9 -tzip %MY_HOME%\temp\%TMP_FILE% * -r
echo.
echo Uploading to server...
cd %MY_HOME%\temp
echo mkdir temp > sftp.bat
echo cd temp >> sftp.bat
echo del %TMP_FILE% >> sftp.bat
echo put %TMP_FILE% >> sftp.bat
psftp -b sftp.bat -be -agent %SERVER%
del /q sftp.bat
echo.
echo Publishing uploaded file on server...
echo cd %DEPLOY_PATH% > ssh.bat
REM echo sudo rm %DEPLOY_FILE% >> ssh.bat
echo sudo cp ~/temp/%TMP_FILE% %DEPLOY_FILE%.nodeploy >> ssh.bat
echo sudo mv %DEPLOY_FILE%.nodeploy %DEPLOY_FILE% >> ssh.bat
REM echo sudo service tomcat7 restart >> ssh.bat
plink -ssh -batch -agent -m ssh.bat %SERVER%
del /q ssh.bat
echo.
echo %DATE% %TIME%
pause
