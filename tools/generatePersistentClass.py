
mysqlhost = "172.16.0.4"
mysqldb = "geinseg"

package = "br.com.geinseg.web.shared.persistent"

# replace MatLab special characters and capitalize
def renameJavaClass(value):
	specialchars = [chr(x) for x in range(0x21,0x30)] + [chr(x) for x in range(0x3A,0x41)] + [chr(x) for x in range(0x5B,0x5F)] + [chr(0x60)] + [chr(x) for x in range(0x7B,0x7F)]
	ret = str(value)
	for sc in specialchars:
		ret = ret.replace(sc,"")
	ret = "".join([s[:1].upper() + s[1:].lower() for s in ret.split(" ")])
	ret = "".join([s[:1].upper() + s[1:] for s in ret.split("_")])
	ret = ret[:1].upper() + ret[1:]
	return ret

# generic text file writing (accept a list just like readText can output one)
def writeText(string, file, append=False):
	if isinstance(string, list):
		string = "\n".join(string)
	with open(file, "a" if append else "w") as textFile:
		textFile.write("{0}\n".format(string))

def loadTableDefinition(conn, database, table):
	sql = """SELECT
				column_name, is_nullable, data_type, character_maximum_length, numeric_precision, column_type, column_key
			FROM information_schema.columns
			WHERE lower(table_schema) = lower(%(database)s) AND lower(table_name) = lower(%(table)s) 
			ORDER BY ordinal_position"""
	c = conn.cursor(dictionary=True)
	c.execute(sql, {"database":database, "table":table})
	ret = c.fetchall()
	c.close()
	return ret

def isPrimaryKey(columnDef):
	return (columnDef["column_key"].lower() == "pri")

def isVersion(columnDef):
	return (columnDef["column_name"].lower() == "lastedit")

def isNullable(columnDef):
	return (columnDef["is_nullable"].lower() == "yes")

def isUnique(columnDef):
	return (columnDef["column_key"].lower() == "uni")

def createColumnAnnotation(columnDef):
	ret = ""
	data = []
	dataType = columnDef["data_type"].lower()
	columnType = columnDef["column_type"].lower()
	columnDefinition = dataType
	if "int" in dataType or dataType == "decimal":
		if columnType == "tinyint(1)":
			columnDefinition = "bit"
		elif "unsig" in columnType:
			columnDefinition = columnDefinition + " unsigned"
	elif dataType == "varchar":
		columnDefinition = ""
	elif isVersion(columnDef):
		columnDefinition = "datetime"
	if len(columnDefinition) > 0:
		data.append("columnDefinition=\"{0}\"".format(columnDefinition))
	if "char" in dataType:
		data.append("length={0}".format(columnDef["character_maximum_length"]))
	if dataType == "decimal":
		data.append("precision={0}".format(columnDef["numeric_precision"]))
	if not isNullable(columnDef):
		data.append("nullable=false")
	if isUnique(columnDef):
		data.append("unique=true")
	if len(data) > 0:
		ret = "@Column({0}) ".format(", ".join(data))
	return ret

def getVariableType(columnDef):
	dataType = columnDef["data_type"].lower()
	columnType = columnDef["column_type"].lower()
	ret = dataType[:1].upper() + dataType[1:].lower()
	if "char" in dataType:
		ret = "String"
	elif "int" in dataType:
		if dataType == "bigint":
			ret = "BigInteger"
		elif dataType == "integer" or dataType == "int":
			ret = "Long"
		elif columnType == "tinyint(1)":
			ret = "Boolean"
		else:
			ret = "Integer"
	elif dataType == "datetime":
		if isVersion(columnDef):
			ret = "Timestamp"
		else:
			ret = "Date"
	return ret

def createJavaVariable(columnDef):
	variableDef = {
		"type": getVariableType(columnDef),
		"name": columnDef["column_name"].lower()
		}
	return "private {type} {name};".format(**variableDef)
	
def createVariable(columnDef):
	annotationDef = {
		"id": "@Id " if isPrimaryKey(columnDef) else "",
		"version": "@Version " if isVersion(columnDef) else "",
		"column": createColumnAnnotation(columnDef)
		}
	
	finalDef = {
		"annotation": "{id}{version}{column}".format(**annotationDef),
		"variable": createJavaVariable(columnDef)
	}
	return "{annotation}{variable}".format(**finalDef)

def generateGetPrimaryKey(ids, pkClass):
	pkCount = len(ids)
	ret = []
	ret.append("	@Override")
	ret.append("	public Serializable getPrimaryKey() {")
	if pkCount > 1:
		ret.append("		{pkClass} ret = new {pkClass}();".format(pkClass=pkClass))
		for id in ids:
			setter = id[:1].upper() + id[1:]
			ret.append("		ret.set{setter}({id});".format(setter=setter,id=id))
		ret.append("		return ret;")
	elif pkCount > 0:
		ret.append("		return {0};".format(ids[0]))
	else:
		ret.append("		return null;")
	ret.append("	}")
	return ret

def generateGetter(columnDef):
	varname = columnDef["column_name"].lower()
	name = varname[:1].upper() + varname[1:]
	ret = []
	ret.append("	public {type} get{name}() {bracket}".format(type=getVariableType(columnDef),name=name,bracket="{"))
	ret.append("		return {0};".format(varname))
	ret.append("	}")
	return ret

def generateSetter(columnDef):
	varname = columnDef["column_name"].lower()
	name = varname[:1].upper() + varname[1:]
	ret = []
	ret.append("	public void set{name}({type} {varname}) {bracket}".format(type=getVariableType(columnDef),name=name,varname=varname,bracket="{"))
	ret.append("		this.{varname} = {varname};".format(varname=varname))
	ret.append("	}")
	return ret

def generateToString(className, tableDef):
	ret = []
	ret.append("	@Override")
	ret.append("	public String toString() {")
	ret.append("		return \"{0} [\" +".format(className))
	for columnDef in tableDef:
		field = columnDef["column_name"].lower();
		ret.append("			\"{field}=\" + {field} +".format(field=field))
	ret.append("		\"]\";")
	ret.append("	}")
	return ret

def generateHashCode(tableDef, prime = 31):
	ret = []
	ret.append("	@Override")
	ret.append("	public int hashCode() {")
	ret.append("		final int prime = {0};".format(prime))
	ret.append("		int result = 1;")
	for columnDef in tableDef:
		field = columnDef["column_name"].lower()
		ret.append("		result = prime * result + (({field} == null) ? 0 : {field}.hashCode());".format(field=field))
	ret.append("		return result;")
	ret.append("	}")
	return ret;

def generateEquals(className, tableDef):
	ret = []
	ret.append("	@Override")
	ret.append("	public boolean equals(Object obj) {")
	ret.append("		if (this == obj)")
	ret.append("			return true;")
	ret.append("		if (obj == null)")
	ret.append("			return false;")
	ret.append("		if (getClass() != obj.getClass())")
	ret.append("			return false;")
	ret.append("		{className} other = ({className}) obj;".format(className=className))
	for columnDef in tableDef:
		field = columnDef["column_name"].lower()
		ret.append("		if ({field} == null) {bracket}".format(field=field, bracket="{"))
		ret.append("			if (other.{field} != null)".format(field=field))
		ret.append("				return false;")
		ret.append("		{bracket} else if (!{field}.equals(other.{field}))".format(field=field, bracket="}"))
		ret.append("			return false;")
	ret.append("		return true;")
	ret.append("	}")
	return ret

def generatePKClass(className, idsDef):
	ret = []
	ret.append("@SuppressWarnings(\"serial\")")
	ret.append("class {className} implements ICompositePK {bracket}".format(className=className,bracket="{"))
	ret.append("")
	for idDef in idsDef:
		ret.append("	{0}".format(createJavaVariable(idDef)))
	ret.append("")
	for idDef in idsDef:
		ret.extend(generateGetter(idDef))
		ret.extend(generateSetter(idDef))
	ret.append("")
	ret.extend(generateHashCode(idsDef))
	ret.extend(generateEquals(className, idsDef))
	ret.extend(generateToString(className, idsDef))
	ret.append("")
	ret.append("}")
	return ret

#def countPK(tableDef):
#	has = [isPrimaryKey(columnDef) for columnDef in tableDef]
#	return has.count(True)
#
#def isCompositePK(tableDef):
#	return (countPK(tableDef) > 1)

def hasVersion(tableDef):
	has = [isVersion(columnDef) for columnDef in tableDef]
	return (has.count(True) != 0)

def createJavaClass(tableName, tableDef, package=None):
	idsDef = [columnDef for columnDef in tableDef if isPrimaryKey(columnDef)]
	ids = [idDef["column_name"].lower() for idDef in idsDef]
	pkCount = len(ids)
	className = renameJavaClass(tableName)
	tableName = tableName.lower()
	hasTable = className.lower() != tableName
	pkClass = className + "PK"
	version = hasVersion(tableDef)
	str = []
	if package is not None:
		str.append("package {0};".format(package))
		str.append("");
	str.append("import java.io.Serializable;")
	if version:
		str.append("import java.sql.Timestamp;")
	str.append("")
	str.append("import javax.persistence.Column;")
	str.append("import javax.persistence.Entity;")
	if pkCount > 0:
		str.append("import javax.persistence.Id;")
		if pkCount > 1:
			str.append("import javax.persistence.IdClass;")
	if hasTable:
		str.append("import javax.persistence.Table;")
	if version:
		str.append("import javax.persistence.Version;")
	str.append("")
	if pkCount > 1:
		str.append("import com.ostegn.jenesis.shared.base.ICompositePK;")
	str.append("import com.ostegn.jenesis.shared.base.PersistentObject;")
	str.append("")
	str.append("@Entity")
	str.append("@SuppressWarnings(\"serial\")")
	if hasTable:
		str.append("@Table(name=\"{0}\")".format(tableName))
	if pkCount > 1:
		str.append("@IdClass({0}.class)".format(pkClass))
	str.append("public class {className} extends PersistentObject {bracket}".format(className=className, bracket="{"))
	str.append("")
	for columnDef in tableDef:
		str.append("	{0}".format(createVariable(columnDef)))
	str.append("")
	str.extend(generateGetPrimaryKey(ids, pkClass))
	str.append("")
	for columnDef in tableDef:
		str.extend(generateGetter(columnDef))
		str.extend(generateSetter(columnDef))
	str.append("")
	str.extend(generateToString(className, tableDef))
	str.append("")
	str.append("}")
	if pkCount > 1:
		str.append("")
		str.extend(generatePKClass(pkClass, idsDef))
	return str

import mysql.connector
import getpass

mysqluser = input("MySQL user at server {0}: ".format(mysqlhost))
mysqlpass = getpass.getpass("MySQL password for {0}: ".format(mysqluser))

conn = mysql.connector.connect(
	host=mysqlhost,
	user=mysqluser,
	passwd=mysqlpass,
	database=mysqldb
)

c = conn.cursor(dictionary=True);

c.execute("SELECT lower(table_name) as table_name FROM information_schema.tables WHERE lower(table_schema) = lower(%(database)s)", {"database": mysqldb})

tables = c.fetchall()

for table in tables:
	tableName = table["table_name"]
	className = renameJavaClass(tableName)
	tableDef = loadTableDefinition(conn, mysqldb, tableName)
	writeText(createJavaClass(tableName, tableDef, package), "{0}.java".format(className))

