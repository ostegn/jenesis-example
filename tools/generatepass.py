import hashlib
import getpass
import sys
from sys import argv

pwd = " ".join(argv[1:])#.decode(sys.getfilesystemencoding()).encode(sys.getfilesystemencoding())

#pwd = "1"
#pwd2 = "2"
#pwd = raw_input("Senha: ")
#while (pwd != pwd2):
	#pwd = getpass("Senha: ")
	#pwd2 = getpass("Repetir: ")
	#if (pwd != pwd2):
	#	print("Senhas diferentes, por favor repita o processo")

#print(pwd)

if (len(pwd) > 0):

	# s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1]
	h = 0
	#l = len(pwd)
	#for i, c in enumerate(pwd, start=1):
	#	h += ord(c)*31**(l-i)
	for c in pwd:
		h = 31*h + ord(c);


	#print(h)
	#print(h&0xFF)
	md = hashlib.sha256()
	md.update("J3N3S1S")
	md.update(pwd.decode(sys.getfilesystemencoding()).encode('utf-8'))
	md.update(chr(h & 0xFF))

	#print("hash:"),
	print(md.hexdigest())
	exit(0)

else:
	print("Uso:"),
	print(argv[0]),
	print("<senha>")
	exit(1)