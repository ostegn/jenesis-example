@echo off
echo %DATE% %TIME%
echo.
echo Configuring environment...
setlocal ENABLEDELAYEDEXPANSION
set JDK=C:\Program Files\Java\jdk1.7.0_07
set JAVA_HOME=%JDK%
set ECLIPSE_HOME=%CD%\..\eclipse
set WORKSPACE=%CD%\workspace
set PROJ_NAME=Jenesis_Web
set DEPLOY_HOME=D:\Servers\tomcatApps\jenesis
set DEPLOY_FOLDER=ROOT
set PATH=%PATH%;%JDK%\bin
set PATH=%PATH%;%ECLIPSE_HOME%\
set PATH=%PATH%;%CD%\
set CLASSPATH=;
REM set CLASSPATH=%CLASSPATH%;%ECLIPSE_HOME%\plugins\*
for /F %%l in ('dir /a:D /s /b %eclipse_home%\plugins\com.google.gwt.eclipse.sdkbundle*') do set GWT_HOME=%%l
for /F %%l in ('dir /a:D /s /b %GWT_HOME%\gwt-*') do set GWT_HOME=%%l
set CLASSPATH=%CLASSPATH%;%GWT_HOME%\*
set PATH=%PATH%;%GWT_HOME%\
set PATHEXT=%PATHEXT%;.JAR
echo.
echo Environment Variables:
set
echo.
cd %WORKSPACE%
rd /s /q %PROJ_NAME%_tmp
echo.
echo Checking with SVN...
svn co http:///svn/jenesis/branches/develop/ %PROJ_NAME%_tmp --username builder --password <password>
echo.
echo Replacing the old project with the new one...
rd /s /q %PROJ_NAME%
ren %PROJ_NAME%_tmp %PROJ_NAME%
cd %PROJ_NAME%
set CLASSPATH=%CLASSPATH%;%CD%\src;%CD%\war\WEB-INF\lib\*;%CD%\war\WEB-INF\classes\
echo.
echo Compiling Workspace...
echo ^<?xml version="1.0"?^> > refresh.xml
echo ^<project default="refresh"^> >> refresh.xml
echo ^<target name="refresh"^> >> refresh.xml
echo ^<eclipse.refreshLocal resource="%PROJ_NAME%" depth="infinite"/^> >> refresh.xml
echo ^</target^> >> refresh.xml
echo ^</project^> >> refresh.xml
eclipse -nosplash -data %WORKSPACE% -application org.eclipse.ant.core.antRunner -f refresh.xml
REM del /q refresh.xml
eclipse -nosplash -data %WORKSPACE% -application org.eclipse.jdt.apt.core.aptBuild
REM echo Compiling Java...
REM dir /s /b src\*.java > compilelist.txt
REM md war\WEB-INF\classes
REM javac -encoding UTF-8 -d war\WEB-INF\classes @compilelist.txt
REM java -jar %ECLIPSE_HOME%\plugins\org.eclipse.jdt.core_3.7.3.v20120119-1537.jar -classpath %CLASSPATH% -1.6 -encoding UTF-8 -d war/WEB-INF/classes @compilelist.txt
ren war\META-INF\context.unused.xml context.xml
cd war\WEB-INF\classes
ren log4j.properties log4j.backup.properties
ren log4j.develop.properties log4j.properties
ren resources\jenesis.properties jenesis.backup.properties
ren resources\jenesis.develop.properties jenesis.properties
echo.
echo Compiling GWT...
set GWT_CLASSES= 
for /r . %%f in (*.gwt.xml) do set GWT_CLASSES=!GWT_CLASSES!%%f 
call set GWT_CLASSES=%%GWT_CLASSES:%CD%\=%%
set GWT_CLASSES=%GWT_CLASSES:.gwt.xml=%
set GWT_CLASSES=%GWT_CLASSES:\=.%
set GWT_CLASSES=%GWT_CLASSES:jenesis.web.Testing=%
cd %WORKSPACE%\%PROJ_NAME%
java -Xmx512m com.google.gwt.dev.Compiler -draftCompile -localWorkers 2 %GWT_CLASSES%
echo.
echo Publishing into server...
rd /s /q %DEPLOY_HOME%\%DEPLOY_FOLDER%
xcopy /e /i /y war\* %DEPLOY_HOME%\%DEPLOY_FOLDER%.nodeploy
move /y %DEPLOY_HOME%\%DEPLOY_FOLDER%.nodeploy %DEPLOY_HOME%\%DEPLOY_FOLDER%
echo.
echo %DATE% %TIME%
