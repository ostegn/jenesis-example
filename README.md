## jenesis-example

Jenesis Example Project

### Changing Project Name

##### Enter project folder
```shell
cd jenesis-example
```

##### Create new folders
```shell
mkdir -p src/your/new/root
mkdir -p test-src/your/new/root
```

##### Move project files and folders
```shell
git mv src/com/ostegn/jenesisex src/your/new/root/project
git mv test-src/com/ostegn/jenesisex test-src/your/new/root/project
```

##### Remove old folders
```shell
rmdir src/com/ostegn
rmdir src/com
rmdir test-src/com/ostegn
rmdif test-src/com
```

##### Rename com.ostegn.jenesisex namespace to your.new.root.project
```shell
grep -rl --exclude-dir=".git" "com\\.ostegn\\.jenesisex" | xargs sed -i "s/com\\.ostegn\\.jenesisex/your.new.root.project/g"
```

##### Rename com/ostegn/jenesisex path to your/new/root/project
```shell
grep -rl --exclude-dir=".git" "com/ostegn/jenesisex" | xargs sed -i "s/com\\/ostegn\\/jenesisex/your\\/new\\/root\\/project/g"
```

##### Change the project description
```shell
nano README.md
```

##### Change project name and other maven properties
```shell
nano pom.xml
```

##### Change log properties
```shell
nano src/log4j.properties
nano src/log4j.public.properties
nano src/log4j.develop.properties
```

##### Change project properties
```shell
nano src/resources/my.properties
```

##### Change sql schema
```shell
nano SQL/createCoreTables.sql
```
